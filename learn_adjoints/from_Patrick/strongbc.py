"""This program solves the Non-reacting Laminar Jet problem 
Shiwei Lan 2016
"""

from dolfin import *
# from dolfin_adjoint import *
import numpy as np

VELOCITY = 0
PRESSURE = 1

# 1. Define the Geometry
INLET = 1
OUTLET = 2
BOUNDING = 3

D = 1.
Lx = 10*D
Ly = 8*D

nx = 100
ny = 100

mesh = RectangleMesh(Point(0.0, -0.5*Ly), Point(Lx, 0.5*Ly), nx, ny)

# boundaries
class InletBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and abs(x[0]) < DOLFIN_EPS
    
class OutletBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and abs(x[0] - Lx) < DOLFIN_EPS
    
class BoundingBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and abs( abs(x[1]) - 0.5*Ly) < DOLFIN_EPS
    
boundary_parts = FacetFunction("size_t", mesh)
boundary_parts.set_all(0)

Gamma_inlet = InletBoundary()
Gamma_inlet.mark(boundary_parts, INLET)
Gamma_outlet = OutletBoundary()
Gamma_outlet.mark(boundary_parts, OUTLET)
Gamma_bounding = BoundingBoundary()
Gamma_bounding.mark(boundary_parts, BOUNDING)


# 2. Define the finite element spaces
Vh_velocity = VectorFunctionSpace(mesh, 'CG', 2)
Vh_pressure = FunctionSpace(mesh, 'CG', 1)

Vh = MixedFunctionSpace([Vh_velocity, Vh_pressure])

# 3. Define boundary conditions
# subclass of Expression with varying parameters
class parExpr(Expression):
    def __init__(self,theta,sigma=1.2,alpha=0.01,s=.8):
        self.theta=theta
        self.sigma=sigma
        self.alpha=alpha
        self.s=s
    # K-L expansion of theta ~ GP(0,C)
    def eval(self,value,x):
        value[0]=sum([self.theta[i]*self.sigma*pow(self.alpha+pow(pi*(i+0.5),2),-self.s/2)*cos(pi*(i+0.5)*x[1]) for i in range(len(self.theta))])+.2
    def value_shape(self):
        return ()

theta = np.ones(10) #np.random.randn(10)
u_inflow = parExpr(theta)
u_inflow = Expression("-(x[1] - 0.5*Ly)*(x[1] + 0.5*Ly)", Ly=Ly)
u_bounding = Constant(0.0)

# Dirichlet on the inlet (u\cdot n=theta) and bounding (u\cdot n=0)
bc_inflow = DirichletBC(Vh.sub(VELOCITY).sub(0), u_inflow, boundary_parts, INLET)
bc_bounding = DirichletBC(Vh.sub(VELOCITY).sub(1), u_bounding, boundary_parts, BOUNDING)

ess_bc = [bc_inflow,bc_bounding]

# 4. Define variational problem

# parameter values
nu = Constant(3e-1)
beta = Constant(0.5)

# trial and test functions
dstates = TrialFunction(Vh)
du, dp = split(dstates)
# tests = TestFunction(Vh)
# v, q = split(tests)
v, q = TestFunctions(Vh)

# functions
# states = Function(Vh)
# u, p = split(states)

# S = Constant(0.06) #Spread function
# den = Expression("A+B*x[0]", A=D, B=S)
# states_init = Expression( ("A/C*(0.5 + 0.5*tanh( 10*(-fabs(x[1]) + 0.5*C)/C ) )", "0.", "0."), A=D, C=den)

states_init = Constant((0.0,0.0,0.0))

states = interpolate(states_init,Vh)
u, p = split(states)
# states.interpolate(states_init)

# variational forms
def strain(u):
    return Constant(2.)*sym(grad(u))
def neg(u):
    return (u-abs(u))/2

n = FacetNormal(mesh)
ds = ds(subdomain_data=boundary_parts)

F = nu*inner(strain(u),strain(v))*dx - p*div(v)*dx \
    + beta*neg(inner(u,n))*inner(u,v)*ds(OUTLET) - div(u)*q*dx \
#     - lm*inner(v,n)*ds(INLET) -(inner(u,n)+u_inflow)*l*ds(INLET)

J_newton = nu*inner(strain(du),strain(v))*dx + inner(grad(du)*u+grad(u)*du,v)*dx - dp*div(v)*dx - div(du)*q*dx \
#             + beta*( neg(inner(du,n))*inner(u,v)+ neg(inner(u,n))*inner(du,v) )*ds(OUTLET)  \
#            - dlm*inner(v,n)*ds(INLET) -inner(du,n)*l*ds(INLET) # need to chop off non-inlet region

J_picard = nu*inner(strain(du),strain(v))*dx + inner(grad(du)*u,v)*dx - dp*div(v)*dx - div(du)*q*dx \
#             + beta*neg(inner(u,n))*inner(du,v)*ds(OUTLET) \
#            - dlm*inner(v,n)*ds(INLET) -inner(du,n)*l*ds(INLET) # need to chop off non-inlet region

# 5. Define initial guess and solve using Newton solver

# solve(F == 0, states, bcs=ess_bc, J=J_picard,
#          solver_parameters={"newton_solver" : {"error_on_nonconvergence": False, "relative_tolerance": 1e-1} })
# solve(F == 0, states, bcs=ess_bc, J=J_newton, 
#         solver_parameters={"newton_solver" : {"error_on_nonconvergence": True, "relative_tolerance": 1e-5} } )

# solve linear proboem to get initial value
problem = NonlinearVariationalProblem(F, states, ess_bc, J_picard)
solver = NonlinearVariationalSolver(problem)
solver.parameters['newton_solver']["relative_tolerance"] = 1e-3
solver.parameters['newton_solver']['error_on_nonconvergence'] = False
solver.solve()
  
# solve problem with full J using Newton solver
problem = NonlinearVariationalProblem(F, states, ess_bc, J_newton)
solver = NonlinearVariationalSolver(problem)
solver.parameters['newton_solver']["relative_tolerance"] = 1e-5
solver.parameters['newton_solver']['error_on_nonconvergence'] = True
solver.solve()

u, p = states.split()

# Create files for storing solution
ufile = File("results/strongbc/velocity.pvd")
pfile = File("results/strongbc/pressure.pvd")
# Save to file
ufile << u
pfile << p
