"""This program solves the Non-reacting Laminar Jet problem 
Shiwei Lan 2016
"""

from dolfin import *
import ufl
# from dolfin_adjoint import *
import numpy as np

VELOCITY = 0
PRESSURE = 1
LAGRANGE = 2

# 1. Define the Geometry
INLET = 1
OUTLET = 2
BOUNDING = 3

D = 1.
Lx = 10*D
Ly = 8*D

nx = 40
ny = 40

mesh = RectangleMesh(Point(0.0, -0.5*Ly), Point(Lx, 0.5*Ly), nx, ny)

# boundaries
class InletBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and abs(x[0]) < DOLFIN_EPS
    
class OutletBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and abs(x[0] - Lx) < DOLFIN_EPS
    
class BoundingBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and abs( abs(x[1]) - 0.5*Ly) < DOLFIN_EPS
    
boundary_parts = FacetFunction("size_t", mesh)
boundary_parts.set_all(0)

Gamma_inlet = InletBoundary()
Gamma_inlet.mark(boundary_parts, INLET)
Gamma_outlet = OutletBoundary()
Gamma_outlet.mark(boundary_parts, OUTLET)
Gamma_bounding = BoundingBoundary()
Gamma_bounding.mark(boundary_parts, BOUNDING)


# 2. Define the finite element spaces
Vh_velocity = VectorFunctionSpace(mesh, 'CG', 2)
Vh_pressure = FunctionSpace(mesh, 'CG', 1)
Vh_lagrange = FunctionSpace(mesh, 'CG', 2)
Vh_velocitycomponent = FunctionSpace(mesh, 'CG', 2)

Vh = MixedFunctionSpace([Vh_velocity, Vh_pressure, Vh_lagrange])

# 3. Define boundary conditions
# subclass of Expression with varying parameters
class parExpr(Expression):
    def __init__(self,theta,sigma=1.2,alpha=0.01,s=.8):
        self.theta=theta
        self.sigma=sigma
        self.alpha=alpha
        self.s=s
    # K-L expansion of theta ~ GP(0,C)
    def eval(self,value,x):
        value[0] = sum([self.theta[i]*self.sigma*pow(self.alpha+pow(pi*(i+0.5),2),-self.s/2)*cos(pi*(i+0.5)*x[1]) for i in range(len(self.theta))])+.2
    def value_shape(self):
        return ()

theta = np.ones(10) #np.random.randn(10)
u_inflow = parExpr(theta)
u_inflow = Expression("-(x[1] - 0.5*Ly)*(x[1] + 0.5*Ly)", Ly=Ly)
u_bounding = Constant(0.0)

# Dirichlet on the inlet (u\cdot n=theta) and bounding (u\cdot n=0)
bc_inflow = DirichletBC(Vh.sub(VELOCITY).sub(0), u_inflow, boundary_parts, INLET)
bc_bounding = DirichletBC(Vh.sub(VELOCITY).sub(1), u_bounding, boundary_parts, BOUNDING)

ess_bc = [bc_bounding]

# 4. Define variational problem

# parameter values
nu = Constant(3e-1)
beta = Constant(0.5)

v, q, Nlamda = TestFunctions(Vh)

# functions
# states = Function(Vh)
# u, p = split(states)

# S = Constant(0.06) #Spread function
# den = Expression("A+B*x[0]", A=D, B=S)
# states_init = Expression( ("A/C*(0.5 + 0.5*tanh( 10*(-fabs(x[1]) + 0.5*C)/C ) )", "0.", "0."), A=D, C=den)

states_init = Constant((0.0, 0.0, 0.0, 1.0))

states = interpolate(states_init,Vh)
u, p, lamda = split(states)
# states.interpolate(states_init)

# variational forms
def strain(u):
    return Constant(2.)*sym(grad(u))
def neg(u):
    return (u-abs(u))/2

n = FacetNormal(mesh)
ds = ds(subdomain_data=boundary_parts)
alpha_val = Constant(1.0e-20)

F = (
      nu*inner(strain(u),strain(v))*dx + inner(grad(u)*u,v)*dx - p*div(v)*dx
    + beta*neg(inner(u,n))*inner(u,v)*ds(OUTLET) - div(u)*q*dx
    + inner(lamda, dot(v, n))*ds(INLET)
    + inner(Nlamda, dot(u, n) + u_inflow)*ds(INLET)
    + alpha_val*inner(lamda, Nlamda)*dx
    )

# solve linear proboem to get initial value
problem = NonlinearVariationalProblem(F, states, ess_bc, J=derivative(F, states))
solver = NonlinearVariationalSolver(problem)
solver.parameters['newton_solver']["relative_tolerance"] = 1e-5
solver.parameters['newton_solver']['error_on_nonconvergence'] = False
solver.solve()

# Set up the functional
obs = Constant((1, 0))
J = inner(obs - u, obs - u)*dx

# Create adjoint boundary conditions (homogenised forward BCs)
def homogenize(bc):
    bc_copy = DirichletBC(bc)
    bc_copy.homogenize()
    return bc_copy
adjbcs = [homogenize(bc) for bc in ess_bc]

# Compute adjoint of forward operator
dFdstates = derivative(F, states)    # linearised forward operator
args = ufl.algorithms.extract_arguments(dFdstates) # arguments for bookkeeping
adFdstates = adjoint(dFdstates, reordered_arguments=args) # adjoint linearised forward operator

dJdu = derivative(J, states, TestFunction(Vh)) # derivative of functional with respect to solution

adjstate = Function(Vh)                        # adjoint state

# Solve adjoint PDE
solve(action(adFdstates, adjstate) - dJdu == 0, adjstate, adjbcs)

adju, adjp, adjlamda = adjstate.split()

# Create files for storing solution
ufile = File("results/adjlagrange/velocity.pvd")
pfile = File("results/adjlagrange/pressure.pvd")
lfile = File("results/adjlagrange/multiplier.pvd")
# Save to file
ufile << adju
pfile << adjp
lfile << adjlamda
