%%%% This is generic pCN (theta=.5) algorithm %%%%

function [q,nll,acpt] = pCN(q_cur,nll_cur,cholC,geom,h)

% dimension
D=length(q_cur);

% sample velocity
v = randn(D,1);
v = cholC'*v;

% current energy
E_cur = nll_cur;

% generate proposal according to Crank-Nicolson scheme
q = ((1-h/4).*q_cur + sqrt(h).*v)./(1+h/4);

% update geometry
nll=geom(q);

% new energy
E_prp = nll;

% Accept according to ratio
logRatio = -E_prp + E_cur;

if (isfinite(logRatio) && (log(rand) < min([0,logRatio])))
    acpt = 1;
else
    q = q_cur; nll = nll_cur;
    acpt = 0;
end


end