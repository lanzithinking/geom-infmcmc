%%%% This is generic infinite dimensional manifold HMC. %%%%

function [q,nll,g,FI,cholG,acpt] = infmHMC(q_cur,nll_cur,g_cur,FI_cur,cholG_cur,cholC,geom,h,L)

% initialization
q = q_cur; D = length(q);
rth = sqrt(h); % make the scale comparable to MALA

% sample velocity
v = randn(D,1);
v = cholG_cur\v;

% natural gradient
halfng = cholG_cur'\g_cur;
ng = cholG_cur\halfng;
hfpricng = cholC'\ng; % half prior conditioned natural gradient

% accumulate the power of force
pow = rth/2*(hfpricng'*(cholC'\v));

% calculate current energy
E_cur = nll_cur - h/8*(hfpricng'*hfpricng) + .5*(v'*FI_cur*v) - sum(log(diag(cholG_cur)));

randL = ceil(rand*L);

% Alternate full sth for position and velocity
for l = 1:randL
    % Make a half step for velocity
    v = v + rth/2*ng;
    
    % Make a full step for position
%     rot = (q+1i*v).*exp(-1i*rth);
%     q = real(rot); v = imag(rot);
    q_0 = q;
    q = ((1-h/4).*q_0 + rth.*v)./(1+h/4);
    v = ((1-h/4).*v - rth.*q_0)./(1+h/4);
    
    % update geometry
    [nll,g,FI,cholG]=geom(q);
    halfng = cholG'\g;
    ng = cholG\halfng;
    hfpricng = cholC'\ng; % half prior conditioned natural gradient

    % Make a half step for velocity
    v = v + rth/2*ng;
    
    % accumulate the power of force
    if l~=randL
      pow = pow + rth*(hfpricng'*(cholC'\v));
    end
end

% accumulate the power of force
pow = pow + rth/2*(hfpricng'*(cholC'\v));

% Evaluate energy at start and end of trajectory
E_prp = nll - h/8*(hfpricng'*hfpricng) + .5*(v'*FI*v) - sum(log(diag(cholG)));

% Accept or reject the state at end of trajectory, returning either
% the position at the end of the trajectory or the initial position
logRatio = - E_prp + E_cur - pow;

if (isfinite(logRatio) && (log(rand) < min([0,logRatio])))
    acpt = 1;
else
    q = q_cur; nll = nll_cur; g = g_cur; FI= FI_cur; cholG = cholG_cur;
    acpt = 0;
end

end