% This is HMC on Hilbert spaces by Beskos et~al (2011).
% This function generates one sample given previous state

% inputs:
%   q_cur: initial state of the parameter
%   nll_cur:=-loglik, current negative log-likelihood function of q
%   g_cur:=-dloglik, current negative gradient of log-likelihood
%   cholC: metric matrices related C
%   geom: function calculating required geometric terms
%   h: step size
%   L: number of leapfrogs
% outputs:
%   q: new state of the parameter
%   nll: new negative log-likelihoood
%   g: new negative gradient of log-likelihood
%   acpt: proposal acceptance indicator

function [q,nll,g,acpt] = infHMC(q_cur,nll_cur,g_cur,cholC,geom,h,L)

% initialization
q = q_cur; D = length(q);
rth = sqrt(h); % make the scale comparable to MALA

% sample velocity
v = randn(D,1);
v = cholC'*v;

% natural gradient
halfng = cholC*g_cur;
ng = cholC'*halfng;

% accumulate the power of force
pow = rth/2*(g_cur'*v);

% calculate current energy
E_cur = nll_cur - h/8*(halfng'*halfng);

randL = ceil(rand*L);

% Alternate full sth for position and velocity
for l = 1:randL
    % Make a half step for velocity
    v = v + rth/2*ng;
    
    % Make a full step for position
    rot = (q+1i*v).*exp(-1i*rth);
    q = real(rot); v = imag(rot);
    
    % update geometry
    [nll,g] = geom(q);
    halfng = cholC*g;
    ng = cholC'*halfng;

    % Make a half step for velocity
    v = v + rth/2*ng;
    
    % accumulate the power of force
    if l~=randL
      pow = pow + rth*(g'*v);
    end
end

% accumulate the power of force
pow = pow + rth/2*(g'*v);

% Evaluate energy at start and end of trajectory
E_prp = nll - h/8*(halfng'*halfng);

% Accept or reject the state at end of trajectory, returning either
% the position at the end of the trajectory or the initial position
logRatio = - E_prp + E_cur - pow;

if (isfinite(logRatio) && (log(rand) < min([0,logRatio])))
    acpt = 1;
else
    q = q_cur; nll = nll_cur; g = g_cur;
    acpt = 0;
end

end