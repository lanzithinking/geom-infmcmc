%%%% This is generic split infinite dimensional manifold HMC. %%%%

function [q,nll,g,FI_t,cholG_t,acpt] = splitinfmHMC(q_cur,ind_trct,nll_cur,g_cur,FI_t_cur,cholG_t_cur,cholC,geom,h,L)

% initialization
q = q_cur; D = length(q);
rth = sqrt(h); % make the scale comparable to MALA

% sample velocity
v = randn(D,1);
v(ind_trct) = cholG_t_cur\v(ind_trct);
v(~ind_trct) = cholC(~ind_trct,~ind_trct)'*v(~ind_trct);

% natural gradient
halfng = zeros(D,1); ng = halfng;
halfng(ind_trct) = cholG_t_cur'\g_cur(ind_trct); halfng(~ind_trct) = cholC(~ind_trct,~ind_trct)*g_cur(~ind_trct);
ng(ind_trct) = cholG_t_cur\halfng(ind_trct); ng(~ind_trct) = cholC(~ind_trct,~ind_trct)'*halfng(~ind_trct);
hfpricng = halfng; % half prior conditioned natural gradient
hfpricng(ind_trct) = cholC(ind_trct,ind_trct)'\ng(ind_trct); %hfpricng(~ind_trct) = cholC(~ind_trct,~ind_trct)*g_cur(~ind_trct);

% accumulate the power of force
pow = rth/2*(hfpricng(ind_trct)'*(cholC(ind_trct,ind_trct)'\v(ind_trct))+g_cur(~ind_trct)'*v(~ind_trct));

% calculate current energy
E_cur = nll_cur - h/8*(hfpricng'*hfpricng) + .5*(v(ind_trct)'*FI_t_cur*v(ind_trct)) - sum(log(diag(cholG_t_cur)));

randL = ceil(rand*L);

% Alternate full sth for position and velocity
for l = 1:randL
    % Make a half step for velocity
    v = v + rth/2*ng;
    
    % Make a full step for position
%     rot = (q+1i*v).*exp(-1i*rth);
%     q = real(rot); v = imag(rot);
    q_0 = q;
    q = ((1-h/4).*q_0 + rth.*v)./(1+h/4);
    v = ((1-h/4).*v - rth.*q_0)./(1+h/4);
    
    % update geometry
    [nll,g,FI_t,cholG_t]=geom(q,ind_trct);
    halfng(ind_trct) = cholG_t'\g(ind_trct); halfng(~ind_trct) = cholC(~ind_trct,~ind_trct)*g(~ind_trct);
    ng(ind_trct) = cholG_t\halfng(ind_trct); ng(~ind_trct) = cholC(~ind_trct,~ind_trct)'*halfng(~ind_trct);
    hfpricng = halfng; % half prior conditioned natural gradient
    hfpricng(ind_trct) = cholC(ind_trct,ind_trct)'\ng(ind_trct);

    % Make a half step for velocity
    v = v + rth/2*ng;
    
    % accumulate the power of force
    if l~=randL
      pow = pow + rth*(hfpricng(ind_trct)'*(cholC(ind_trct,ind_trct)'\v(ind_trct))+g(~ind_trct)'*v(~ind_trct));
    end
end

% accumulate the power of force
pow = pow + rth/2*(hfpricng(ind_trct)'*(cholC(ind_trct,ind_trct)'\v(ind_trct))+g(~ind_trct)'*v(~ind_trct));

% Evaluate energy at start and end of trajectory
E_prp = nll - h/8*(hfpricng'*hfpricng) + .5*(v(ind_trct)'*FI_t*v(ind_trct)) - sum(log(diag(cholG_t)));

% Accept or reject the state at end of trajectory, returning either
% the position at the end of the trajectory or the initial position
logRatio = - E_prp + E_cur - pow;

if (isfinite(logRatio) && (log(rand) < min([0,logRatio])))
    acpt = 1;
else
    q = q_cur; nll = nll_cur; g = g_cur; FI_t= FI_t_cur; cholG_t = cholG_t_cur;
    acpt = 0;
end

end