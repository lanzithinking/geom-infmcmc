%%%% This is generic split-infmMALA algorithm %%%%

function [q,nll,g,FI_t,cholG_t,acpt] = splitinfmMALA(q_cur,ind_trct,nll_cur,g_cur,FI_t_cur,cholG_t_cur,cholC_r,geom,h)

% dimension
D=length(q_cur);

% sample velocity
v = randn(D,1);
v(ind_trct) = cholG_t_cur\v(ind_trct);
v(~ind_trct) = cholC_r'*v(~ind_trct);

% natural gradient
halfng = zeros(D,1); ng = halfng;
halfng(ind_trct) = cholG_t_cur'\g_cur(ind_trct); halfng(~ind_trct) = cholC_r*g_cur(~ind_trct);
ng(ind_trct) = cholG_t_cur\halfng(ind_trct); ng(~ind_trct) = cholC_r'*halfng(~ind_trct);

% update velocity
v = v + sqrt(h)/2.*ng;

% current energy
E_cur = nll_cur - sqrt(h)/2*(g_cur'*v) + h/8*(halfng'*halfng) + .5*(v(ind_trct)'*FI_t_cur*v(ind_trct)) - sum(log(diag(cholG_t_cur)));

% generate proposal according to simplified manifold Langevin dynamics
q = ((1-h/4).*q_cur + sqrt(h).*v)./(1+h/4);

% update velocity
v = (-(1-h/4).*v + sqrt(h).*q_cur)./(1+h/4); % or (-(1+h/4).*v + sqrt(h).*q)./(1-h/4);

% update geometry
[nll,g,FI_t,cholG_t]=geom(q,ind_trct);

% natural gradient
halfng(ind_trct) = cholG_t'\g(ind_trct); halfng(~ind_trct) = cholC_r*g(~ind_trct);

% new energy
E_prp = nll - sqrt(h)/2*(g'*v) + h/8*(halfng'*halfng) + .5*(v(ind_trct)'*FI_t*v(ind_trct)) - sum(log(diag(cholG_t)));

% Accept according to ratio
logRatio = -E_prp + E_cur;

if (isfinite(logRatio) && (log(rand) < min([0,logRatio])))
    acpt = 1;
else
    q = q_cur; nll = nll_cur; g = g_cur; FI_t= FI_t_cur; cholG_t = cholG_t_cur;
    acpt = 0;
end


end