%%%% This is generic infMALA algorithm %%%%

function [q,nll,g,acpt] = infMALA(q_cur,nll_cur,g_cur,cholC,geom,h)

% dimension
D=length(q_cur);

% sample velocity
v = randn(D,1);
v = cholC'*v;

% natural gradient
halfng = cholC*g_cur;
ng = cholC'*halfng;

% update velocity
v = v + sqrt(h)/2.*ng;

% current energy
E_cur = nll_cur - sqrt(h)/2*(g_cur'*v) + h/8*(halfng'*halfng);

% generate proposal according to Langevin dynamics
q = ((1-h/4).*q_cur + sqrt(h).*v)./(1+h/4);

% update velocity
v = (-(1-h/4).*v + sqrt(h).*q_cur)./(1+h/4); % or (-(1+h/4).*v + sqrt(h).*q)./(1-h/4);

% update geometry
[nll,g]=geom(q);

% natural gradient
halfng = cholC*g;

% new energy
E_prp = nll - sqrt(h)/2*(g'*v) + h/8*(halfng'*halfng);

% Accept according to ratio
logRatio = -E_prp + E_cur;

if (isfinite(logRatio) && (log(rand) < min([0,logRatio])))
    acpt = 1;
else
    q = q_cur; nll = nll_cur; g = g_cur;
    acpt = 0;
end


end