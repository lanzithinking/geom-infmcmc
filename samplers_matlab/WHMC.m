%%%% This is generic wormhole HMC algorithm %%%%

function [q,u,du,acpY,jumped] = WHMC(q_cur,u_cur,du_cur,wmhole,xU,eps,L,Nfpiter)

% initialization in the augmented space
q = q_cur; D = length(q)-1;
u = u_cur; du=du_cur;

% wormhole parameters
modes=wmhole.modes; K=size(modes,1); % number of modes in wormhole
worldist=wmhole.worldist;
unidrecup=wmhole.unidrecup;
whwidth=wmhole.width;

% propose velocity
v = randn(D+1,1);

% Calculate current energy value
E_cur  = u + (v'*v)/2;

% adjust energy gap when mode jump happens
jumped = 0; adjust = 0; dtE = 0;

% randomize the number of Leapfrog steps
randL=ceil(rand*L);

% Make a half step for velocity
v = v - eps/2.*du_cur;
% Perform leapfrog steps
for l = 1:randL
    
    
    % Make a full step for position
    if jumped
        q = q + eps.*v;
    else
%         try
        % build wormhole network to try jump
        q_jump = q;
        [~,closest] = min(sum((repmat(q_jump(1:D)',[K,1])-modes).^2,2));
        endL = [modes(closest,:),sign(q_jump(end))*worldist]';
        endR = [modes,-sign(q_jump(end))*worldist.*ones(K,1)]';
        whdirection = squeeze(unidrecup(closest,:,:))';
        whdirection(D+1,:) = -sign(q_jump(end)).*whdirection(D+1,:); % wormhole upwards or dnwards
        for fpiter = 1:Nfpiter
            % calculate the mollifier
            X2endL = -repmat(q_jump-endL,1,K); X2endR = -(repmat(q_jump,1,K)-endR);
            projL = sum(X2endL.*whdirection); projR = sum(X2endR.*whdirection);
            vicinity = sum(X2endL.*X2endR) + abs(projL.*projR);
            mollifier = exp(-vicinity/D./whwidth(closest,:));
%             if any(isnan(mollifier))
%                 disp('wow!');
%             end
            % decide whether to jump
            if any(isnan(mollifier))||rand<1-sum(mollifier)
                f = v;
            else
%                 disp(['mollifier: ',num2str(mollifier)]);
                jump2 = randsample(K,1,true,mollifier);
%                 disp(['Jumping to mode ' num2str(jump2)]);
                f = 2/eps.*X2endR(:,jump2);
                jumped = 1; adjprob = min([1,sum(mollifier)]);
            end
            if fpiter==1
                f_fix = f;
            end
            % update q_jump via fixed point iteration
            q_jump = q + .5*eps.*(f_fix+f);
        end
        % test whether a mode jump truely happens and adjust energy gap if so
        [~,closest_jump] = min(sum((repmat(q_jump(1:D)',[K,1])-modes).^2,2));
        if closest_jump~=closest&&jumped
            dtE = xU(q_jump) - (xU(q)+(v'*v)/2);
            adjust = 1;
        end
        q = q_jump;
%         catch
%             adjust=0;
%         end
    end
    
    
    du = xU(q,1);
    % Make full step for velocity
    if(l~=randL)
        v = v - eps.*du;
    end
    
    % finish adjusting the energy gap when necessary
    if adjust
        dtE = dtE + (v'*v)/2;
        dtE = adjprob*dtE;
        adjust = 0;
    end
    
end
v = v - eps/2.*du;


try
    % Calculate proposed energy value
    u = xU(q);
    E_prp = u + (v'*v)/2;

    % Accept according to ratio
    logRatio = -E_prp + E_cur +dtE;

    if (isfinite(logRatio) && (log(rand) < min([0,logRatio])))
        acpY = 1;
    else
        q=q_cur; u=u_cur;
        acpY = 0;
    end
catch
    q=q_cur; u=u_cur;
    acpY = 0;
end
    
end
