%%%% This is generic infmMALA algorithm %%%%

function [q,nll,g,FI,cholG,acpt] = infmMALA(q_cur,nll_cur,g_cur,FI_cur,cholG_cur,geom,h)

% dimension
D=length(q_cur);

% sample velocity
v = randn(D,1);
v = cholG_cur\v;

% natural gradient
halfng = cholG_cur'\g_cur;
ng = cholG_cur\halfng;

% update velocity
v = v + sqrt(h)/2.*ng;

% current energy
E_cur = nll_cur - sqrt(h)/2*(g_cur'*v) + h/8*(halfng'*halfng) + .5*(v'*FI_cur*v) - sum(log(diag(cholG_cur)));

% generate proposal according to simplified manifold Langevin dynamics
q = ((1-h/4).*q_cur + sqrt(h).*v)./(1+h/4);

% update velocity
v = (-(1-h/4).*v + sqrt(h).*q_cur)./(1+h/4); % or (-(1+h/4).*v + sqrt(h).*q)./(1-h/4);

% update geometry
[nll,g,FI,cholG]=geom(q);

% natural gradient
halfng = cholG'\g;

% new energy
E_prp = nll - sqrt(h)/2*(g'*v) + h/8*(halfng'*halfng) + .5*(v'*FI*v) - sum(log(diag(cholG)));

% Accept according to ratio
logRatio = -E_prp + E_cur;

if (isfinite(logRatio) && (log(rand) < min([0,logRatio])))
    acpt = 1;
else
    q = q_cur; nll = nll_cur; g = g_cur; FI= FI_cur; cholG = cholG_cur;
    acpt = 0;
end


end