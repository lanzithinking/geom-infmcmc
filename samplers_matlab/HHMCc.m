% This is HMC on Hilbert spaces (compact version) by Beskos et~al (2011).
% This function generates one sample given previous state

% inputs:
%   q_cur: initial state of the parameter
%   U:=-log(density(q)), potential function of q (and its gradient)
%   ofC: metric matrices related C
%   h: step size
%   L: number of leapfrogs
% outputs:
%   q: new state of the parameter
%   acpt: proposal acceptance indicator

function [q,nll,g,acpt] = HHMCc(q_cur,nll_cur,g_cur,cholC,geom,h,L)

% initialization
q = q_cur; D = length(q);

% sample velocity
v = randn(D,1);
v = cholC'*v;

% natural gradient
halfng = cholC*g_cur;
ng = cholC'*halfng;

% accumulate the power of force
pow = h/2*(g_cur'*v);

% calculate current energy
E_cur = nll_cur - h^2/8*(halfng'*halfng);

randL = ceil(rand*L);

% Alternate full sth for position and velocity
for l = 1:randL
    % Make a half step for velocity
    v = v + h/2*ng;
    
    % Make a full step for position
    rot = (q+1i*v).*exp(-1i*h);
    q = real(rot); v = imag(rot);
    
    % update geometry
    if l~=randL
        [~,g] = geom(q,1);
    else
        [nll,g] = geom(q,[0,1]);
    end
    halfng = cholC*g;
    ng = cholC'*halfng;

    % Make a half step for velocity
    v = v + h/2*ng;
    
    % accumulate the power of force
    if l~=randL
      pow = pow + h*(g'*v);
    end
end

% accumulate the power of force
pow = pow + h/2*(g'*v);

% Evaluate energy at start and end of trajectory
E_prp = nll - h^2/8*(halfng'*halfng);

% Accept or reject the state at end of trajectory, returning either
% the position at the end of the trajectory or the initial position
logRatio = - E_prp + E_cur - pow;

if (isfinite(logRatio) && (log(rand) < min([0,logRatio])))
    acpt = 1;
else
    q = q_cur; nll = nll_cur; g = g_cur;
    acpt = 0;
end

end