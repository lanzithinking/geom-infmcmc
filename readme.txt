Infinite-dimensional manifold Markov chain Monte Carlo (MCMC) is a class of geometric MCMC
algorithms that are well defined on Hilbert spaces. They are robust under mesh refinement 
or increasing dimensions while traditional MCMC algorithms are characterized by 
deteriorating mixing times upon mesh-refinement, when the finite-dimensional approximations 
become more accurate. They also take advantage of geometric information and have been 
proven to be more efficient than infinite-dimensional non-geometric methods such as 
preconditioned Crank-Nicolson (pCN).

This repo contains Matlab/Python codes for examples in the following paper:

Alexandros Beskos, Mark Girolami, Shiwei Lan, Patrick E. Farrell, Andrew M. Stuart
Geometric MCMC for Infinite-Dimensional Inverse Problems
http://arxiv.org/abs/1606.06351

CopyRight: Shiwei Lan

Main codes are located in the folder 'experiments'.

Please cite the reference when using the codes, Thanks!

Shiwei Lan
9-18-2016