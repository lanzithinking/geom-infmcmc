% This function calculates the measurement of deviation of the empirical
% distribution of samples from a 1-d Gaussian

function dev=deviation2Gaussian(samp,opt,m,s)

if nargin<2
    opt=1; % criterion option
    % estimate sample mean and sample standard deviation
    m=mean(samp); s=std(samp);
elseif nargin<3
    m=mean(samp); s=std(samp);
end

[N,D]=size(samp);
if N==1
    samp=samp';
    N=D;D=1;
end

dev=zeros(1,D);

if length(m)==1
    m=repmat(m,D,1);
end
if length(s)==1
    s=repmat(s,D,1);
end

if opt==1
    % criterion 1: difference in CDF
    for d=1:D
        [F,X]=ecdf(samp(:,d));
        Phi=normcdf(X,m(d),s(d));
        dev(d)=max(abs(F-Phi));
%         dev(d)=norm(F-Phi)/sqrt(N);
    end
elseif opt==2
    % criterion 2: K-L divergence
    if N<1e4
        for d=1:D
            f=ksdensity(samp(:,d),samp(:,d));
            phi=normpdf(samp(:,d),m(d),s(d));
            dev(d)=mean(log(f)-log(phi));
        end
    else % use default setting for ksdensity for large samples to save time
        for d=1:D
            [f,xi]=ksdensity(samp(:,d));
            phi=normpdf(xi,m(d),s(d));
            dev(d)=mean(log(f)-log(phi));
        end
    end
else
    error('wrong option!');
end

end