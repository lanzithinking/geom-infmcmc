"""
Test the cpp code for expression
"""
from dolfin import *
import numpy as np
# from dolfin.cpp.mesh import UnitSquareMesh

mesh = UnitSquareMesh(20,20)
V = FunctionSpace(mesh,'CG',1)

KL_cpp = """
    class KL_expansion1 : public Expression
    {
    public:
      KL_expansion1() : Expression(), theta, sigma(1.2), alpha(0.1), s(0.6) { }
       
      void eval(Array<double>& values, const Array<double>& x) const
      {
        int l = sizeof(theta)/sizeof(theta[0]);
        for (int i=0; i<l; i++)
            value[0] += sigma*theta[i]*pow(pow(alpha+(M_PI*i),2),-s/2)*cos(M_PI*i*x[1]));
      }
    public:
      Array<double> theta; double sigma, alpha, s;
    };"""

theta = np.random.randn(10)
fun_expr = Expression(cppcode=KL_cpp, theta=theta)
fun = interpolate(fun_expr, V)
print(fun.vector())