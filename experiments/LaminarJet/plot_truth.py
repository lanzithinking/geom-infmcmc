"""
Plot true inflow velocity profile Laminar-Jet PDE
Shiwei Lan @ U of Warwick, 2016
"""


from dolfin import *
import numpy as np
import matplotlib.pyplot as plt
from itertools import cycle

from Laminar import Laminar

np.random.seed(2016)
# generate data
theta_dim=100
dim_seq = [10,20,30,50,100]

# theta=.1*np.ones(theta_dim)#np.random.randn(theta_dim)
theta=np.random.randn(theta_dim)
laminar=Laminar(unit=.1,nx=30,ny=30,nu=1.0e-2,beta=0.3,stokes=False)

y=np.linspace(-.5*laminar.Ly,.5*laminar.Ly,200+1)
# plot
plt.figure(figsize=(10,6))
# plt.gca().set_color_cycle(['red', 'green', 'blue', 'yellow'])
lines = ["-","--","-.",":"];linecycler = cycle(lines)
for i in range(len(dim_seq)):
    inflow=laminar.inflow_profile(theta=theta[:dim_seq[i]],sigma=.5,alpha=1,s=.8)
    u_inflow=interpolate(inflow,laminar.V_velocity0)
    u_in=np.array([u_inflow(Point(0,k)) for k in y])
    plt.plot(y,u_in,next(linecycler),linewidth=2)
# plt.axis([y.min(),y.max(),-1.0,2.5])
plt.axis('tight')
plt.xlabel('y',fontsize=15); plt.ylabel(r'u$_{\,\mathtt{inflow}}$',fontsize=15)
plt.title('True inflow velocity on inlet boundary')
plt.legend([s+' modes' for s in map(str,dim_seq)],ncol=2,loc='best')
# plt.savefig('./result/truth.png',bbox_inches='tight')
plt.show()