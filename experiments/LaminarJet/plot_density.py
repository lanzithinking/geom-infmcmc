"""
Plot posterior of KL coefficients in inverse problem of Laminar-Jet PDE
Shiwei Lan @ U of Warwick, 2016
"""

from dolfin import *
import numpy as np
import time
import matplotlib.pyplot as plt

from Laminar import Laminar

# from joblib import Parallel, delayed
# import multiprocessing

# parameters["num_threads"] = 2

np.random.seed(2016)
# generate data
theta_dim=100
var_obs=1e-4

# generate date
# theta=.1*np.ones(theta_dim)#np.random.randn(theta_dim)
theta=np.random.randn(theta_dim)
laminar=Laminar(unit=.1,nx=30,ny=30,nu=2.0e-2,beta=0.3,stokes=False)
inflow=laminar.inflow_profile(theta=theta,sigma=.5,alpha=1,s=.8)

# obtain observations
print('Obtaining observations...')
obs,idx,loc=laminar.get_obs(inflow)
num_obs=len(idx)
print('%d observations have been obtained!' % num_obs)
# add some noise
obs += np.sqrt(var_obs)*np.random.randn(num_obs)

# define data misfit class
print('\nDefining data-misfit...')
# smaller problem to reduce waiting time
# red_num_obs = np.random.randint(1,np.ceil(num_obs/2))
# obs=obs[:red_num_obs]; idx=idx[:red_num_obs]; loc=loc[:red_num_obs,]
obs=obs[::5]; idx=idx[::5]; loc=loc[::5,]
red_num_obs = len(idx)
print('Reduced to %d observations to save waiting time for results.' % red_num_obs)
misfit=laminar.data_misfit(obs,1./var_obs,idx,loc)

# preparing density plot
dim=[1,3]
print('\nPreparing posterior density plot in dimensions (%d, %d)...' % tuple(dim))
# log density function
# def logdensity(x,dim=dim):
#     theta_i = theta
#     N = x.size/len(dim)
#     ll = np.full(N,np.nan)
#     for i,p in zip(range(N),x):
#         theta_i[np.array(dim)-1] = p; inflow.theta = theta_i
#         try:
#             nll,_,_,_=laminar.get_geom(inflow,misfit)
#             ll[i] = -nll
#         except RuntimeError:
#             print('Bad point (%.4f,%.4f) encountered: divergent solution!' % tuple(p))
#             pass
#     return ll

def logpdf(x,dim=dim,theta=theta,inflow=inflow,PDE=laminar,obj=misfit):
    theta[np.array(dim)-1] = x; inflow.theta = theta
    try:
        nll,_,_,_=PDE.get_geom(inflow,obj)
        logpost = -nll #+ theta.dot(theta)/2
#         print('successful solving!')
    except RuntimeError:
        print('Bad point (%.4f,%.4f) encountered: divergent solution!' % tuple(x))
        logpost = np.nan
        pass
    return logpost



# from multiprocessing import Pool
# h =.2
# x = np.arange(-4.0, 4.0+h, h);
n = 15
x = np.linspace(-4.0, 4.0, num=n);
y = x.copy()
X, Y = np.meshgrid(x, y)
XY = np.array([X.flatten(),Y.flatten()]).T
print(XY.shape)

start = time.time()
Z = map(logpdf,XY)
# num_cores = multiprocessing.cpu_count()
# Z = Parallel(n_jobs=2,backend="threading")(delayed(logpdf)(r) for r in XY)
# Z = Parallel(n_jobs=2,backend="threading")(map(delayed(logpdf), XY))
end = time.time()
print('Time used is %.4f' % (end-start))

Z=np.reshape(Z,X.shape)

# save
import pickle
f=open('./result/posterior.pckl','wb')
pickle.dump([X,Y,Z],f)
f.close()

# plot
plt.figure()
CS = plt.contour(X, Y, Z)
plt.clabel(CS, inline=1, fontsize=10)
plt.xlabel(r'$\theta_{%d}$' % dim[0]); plt.ylabel(r'$\theta_{%d}$' % dim[1])
plt.title('log-Posterior Contour')
plt.savefig('./result/post_contour.png',bbox_inches='tight')
plt.show()
