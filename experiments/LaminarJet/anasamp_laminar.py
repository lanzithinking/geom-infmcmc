"""
Analyze posterior samples of Laminar-Jet problem by geo-inf Monte Carlo methods
Shiwei Lan @ U of Warwick, 2016
"""

import os
import numpy as np

from analyze_samples import ana_samp


def main():

    algs=('pCN','infMALA','infHMC','infmMALA','infmHMC','splitinfmMALA','splitinfmHMC')
#     dim=100
#     algs=[s+'_dim'+str(dim) for s in algs]

    print('Analyzing posterior samples of Laminar-Jet...\n')

    _=ana_samp(algs=algs,dir_name='analysis',PLOT=True,save_fig=True).analyze()

if __name__ == '__main__':
    main()
