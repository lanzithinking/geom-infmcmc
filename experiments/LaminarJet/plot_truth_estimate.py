"""
Plot the true and estimated inflow velocity profile in Laminar-Jet PDE
Shiwei Lan @ U of Warwick, 2016
"""

import os,pickle
from dolfin import *
import numpy as np
import matplotlib.pyplot as plt
from itertools import cycle

from Laminar import Laminar

np.random.seed(2016)
# generate data
theta_dim=100
# define the PDE problem
# theta=.1*np.ones(theta_dim)#np.random.randn(theta_dim)
theta=np.random.randn(theta_dim)
laminar=Laminar(unit=.1,nx=30,ny=30,nu=3.0e-2,beta=0.3,stokes=False)
inflow=laminar.inflow_profile(theta=theta,sigma=.5,alpha=1,s=.8)
# def inflow_fun(theta,sigma=.5,alpha=1,s=.8):
#     l = len(theta)
#     seq0l = np.arange(l,dtype=np.float)
#     fun = lambda x: sigma*theta.dot(pow(alpha+(pi*seq0l)**2,-s/2)*np.cos(pi*seq0l*x))
#     return fun

# plot
fig,axes = plt.subplots(nrows=1,ncols=2,figsize=(12,5))

# plot truth
plt.axes(axes[0])
dim_seq = [10,20,30,100]
y=np.linspace(-.5*laminar.Ly,.5*laminar.Ly,200+1)
# plt.gca().set_color_cycle(['red', 'green', 'blue', 'yellow'])
lines = ["--","-.",":","-"];linecycler = cycle(lines)
for i in range(len(dim_seq)):
    inflow.theta=theta[:dim_seq[i]]; inflow.l=dim_seq[i]
    u_inflow=interpolate(inflow,laminar.V_velocity0)
    u_truth=np.array([u_inflow(Point(0,k)) for k in y])
    # u_inflow_fun=inflow_fun(theta[:dim_seq[i]])
    # u_truth1=np.array([u_inflow_fun(k) for k in y])
    # print(np.linalg.norm(u_truth-u_truth1))
    plt.plot(y,u_truth,next(linecycler),linewidth=2)
# plt.axis([y.min(),y.max(),-1.0,2.5])
plt.axis('tight')
plt.xlabel('y',fontsize=12); plt.ylabel(r'u$_{\,\mathtt{inflow}}$',fontsize=12)
plt.title('True inflow velocity on inlet boundary',fontsize=12)
plt.legend([ss+' modes' for ss in map(str,dim_seq)],fontsize=10,loc='upper right')

################################################################################

# preparation for estimates
algs=('pCN','infMALA','infHMC','infmMALA','infmHMC','splitinfmMALA','splitinfmHMC')
alg_names=('pCN','$\infty$-MALA','$\infty$-HMC','$\infty$-mMALA','$\infty$-mHMC','split$\infty$-mMALA','split$\infty$-mHMC')
num_algs=len(algs)
found = np.zeros(num_algs,dtype=np.bool)

folder = './analysis'
fnames=[f for f in os.listdir(folder) if f.endswith('.pckl')]

# plot posterior mean
plt.axes(axes[1])
plt.plot(y,u_truth,color='c',linestyle='-',linewidth=3)
for a in range(num_algs):
    for f_i in fnames:
        if '_'+algs[a]+'_' in f_i:
            f=open(os.path.join(folder,f_i),'rb')
            _,_,_,samp,_,_,_,_=pickle.load(f)
            f.close()
            found[a]=True
    if found[a]:
        theta_mean = np.mean(samp,axis=0)
        inflow.theta=theta_mean
        u_inflow=interpolate(inflow,laminar.V_velocity0)
        u_inflow_est=np.array([u_inflow(Point(0,k)) for k in y])
        plt.plot(y,u_inflow_est,next(linecycler),linewidth=2)
        if algs[a]=='infmHMC':
            f_name=os.path.join('./analysis/','u_inflow_cong.pckl')
            if os.path.isfile(f_name):
                f=open(f_name,'rb')
                u_inflow_cong=pickle.load(f)
                f.close()
            else:
                u_inflow_cong = np.zeros((samp.shape[0],len(y)))
                for i,theta in zip(range(samp.shape[0]),samp):
                    inflow.theta=theta
                    u_inflow=interpolate(inflow,laminar.V_velocity0)
                    u_inflow_cong[i,]=np.array([u_inflow(Point(0,k)) for k in y])
                # save u_inflow_cong
                f=open(f_name,'wb')
                pickle.dump(u_inflow_cong,f)
                f.close()
            u_inflow_m = np.mean(u_inflow_cong,axis=0)
            # print(np.linalg.norm(u_inflow_m-u_inflow_est))
            u_inflow_std = np.std(u_inflow_cong,axis=0)
            cb_l=u_inflow_m-1.96*u_inflow_std
            cb_u=u_inflow_m+1.96*u_inflow_std
            plt.fill_between(y,cb_l,cb_u,facecolor='.8')
plt.axis('tight')
plt.xlabel('y',fontsize=12); #plt.ylabel(r'u$_{\,\mathtt{inflow}}$',fontsize=12)
plt.title('Posterior estimates of inflow velocity',fontsize=12)
lgd_list=np.insert(np.array(alg_names)[found],0,'Truth')
plt.legend(lgd_list,fontsize=10,ncol=2,loc='best')

fig.tight_layout()
plt.savefig('./analysis/truth_est.png')

plt.show()
