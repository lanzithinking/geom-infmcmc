"""
Test solutions of Laminar-Jet PDE
Shiwei Lan @ U of Warwick, 2016
"""


from dolfin import *
import numpy as np
import time
# import matplotlib.pyplot as plt

from Laminar import Laminar

# parameters["num_threads"] = 2

np.random.seed(2016)
# generate data
dim=10
var_obs=1e-2

# generate observations
# theta=.1*np.ones(10)#np.random.randn(dim)
theta=np.random.randn(dim)
laminar=Laminar(unit=.1,nx=4,ny=4,nu=2.0e-2,beta=0.3,stokes=False)
inflow=laminar.inflow_profile(theta=theta,sigma=.5,alpha=1,s=.8)

# solve forward equation
# u_fwd,p_fwd,l_fwd=laminar.soln_fwd(theta)

# obtain observations
print('Obtaining observations...')
obs,idx,loc=laminar.get_obs(inflow)
# print(idx)
# print(obs)
# print(loc)
num_obs=len(idx)
print('%d observations have been obtained!' % num_obs)
# add some noise
obs += np.sqrt(var_obs)*np.random.randn(num_obs)
  
# define data misfit class
print('\nDefining data-misfit...')
# smaller problem to reduce waiting time
# red_num_obs = np.random.randint(1,np.ceil(num_obs/2))
# obs=obs[:red_num_obs]; idx=idx[:red_num_obs]; loc=loc[:red_num_obs,]
# obs=obs[::4]; idx=idx[::4]; loc=loc[::4,]
# red_num_obs = len(idx)
# print('Reduced to %d observations to save waiting time for results.' % red_num_obs)
misfit=laminar.data_misfit(obs,1./var_obs,idx,loc)

# obtain the geometric quantities
print('\n\nObtaining full geometric quantities with Adjoint method...')
start = time.time()
nll,dnll,Fv,FI = laminar.get_geom(inflow,misfit,[0,1,1.5,2])
if dnll is not None:
    print('gradient:')
    print(dnll)
v = np.random.randn(inflow.l)
if Fv is not None:
    Ma = Fv(v)
    print('metric action on a random vector:')
    print(Ma)
if FI is not None:
    print('metric:')
    print(FI)
end = time.time()
print('Time used is %.4f' % (end-start))

# obtain the truncated geometric quantities
opt=2
# idx=range(5)
idx=np.random.choice(dim,size=5,replace=False)
print('\nNow truncating on '+{0:'value, gradient, and metric (no sense)',
                            1:'gradient and metric',
                            2:'metric',
                            3:'none'}[opt]+'...')

# delete dunknowndtheta_mat and dunknowndtheta_sps for reinitialization
del laminar.dunknowndtheta_mat,laminar.dunknowndtheta_sps
print('\n\nObtaining truncated geometric quantities with Adjoint method...')
start = time.time()
nll_t,dnll_t,Fv_t,FI_t = laminar.get_geom(inflow,misfit,[0,1,1.5,2],opt=opt,idx=idx)
if dnll_t is not None:
    print('gradient:')
    print(dnll_t)
if opt>2:
    v_t = v
else:
    v_t = v[idx]
if Fv_t is not None:
    Ma_t = Fv_t(v_t)
    print('metric action on a random vector:')
    print(Ma_t)
if FI_t is not None:
    print('metric:')
    print(FI_t)
end = time.time()
print('Time used is %.4f' % (end-start))


# test with truncation
if opt<=2:
    print('The difference in metric is: %.10f' % np.linalg.norm(FI[np.ix_(idx,idx)]-FI_t)) # FI[idx,idx] is not the correct way to extract submatrix in numpy!
    print('The difference in metric action is: %.10f... expected... think about it:)' % np.linalg.norm(Ma[idx]-Ma_t))
if opt<=1:
    print('The difference in gradient is: %.10f' % np.linalg.norm(dnll[idx]-dnll_t))
if opt==0:
    print('The difference in misfit value is: %.10f' % np.linalg.norm(nll-nll_t))
