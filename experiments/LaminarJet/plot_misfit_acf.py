"""
Plot autocorrelation of data-misfits in Laminar-Jet inverse problem
Shiwei Lan @ U of Warwick, 2016
"""

import os,pickle
from dolfin import *
import numpy as np
import matplotlib.pyplot as plt
# import pandas as pd
from itertools import cycle

from Laminar import Laminar

def autocorr(x):
    """This one is closest to what plt.acorr does.
    http://stackoverflow.com/q/14297012/190597
    http://en.wikipedia.org/wiki/Autocorrelation#Estimation
    """
    n = len(x)
    variance = x.var()
    x = x - x.mean()
    r = np.correlate(x, x, mode='full')[-n:]
    assert np.allclose(r, np.array(
        [(x[:n - k] * x[-(n - k):]).sum() for k in range(n)]))
    result = r / (variance * (np.arange(n, 0, -1)))
    return result

algs=('pCN','infMALA','infHMC','infmMALA','infmHMC','splitinfmMALA','splitinfmHMC')
alg_names=('pCN','$\infty$-MALA','$\infty$-HMC','$\infty$-mMALA','$\infty$-mHMC','split$\infty$-mMALA','split$\infty$-mHMC')
num_algs=len(algs)
found = np.zeros(num_algs,dtype=np.bool)

folder = './analysis'
fnames=[f for f in os.listdir(folder) if f.endswith('.pckl')]

# plot data-misfit
fig,axes = plt.subplots(num=0,ncols=2,figsize=(12,6))
lines = ["-","--","-.",":"]
linecycler0 = cycle(lines); linecycler1 = cycle(lines);

for a in range(num_algs):
    for f_i in fnames:
        if '_'+algs[a]+'_' in f_i:
            f=open(os.path.join(folder,f_i),'rb')
            _,_,_,_,loglik,_,_,_=pickle.load(f)
            _,_,_,_,_,_,_,_,_,_,_,_,_,_,args=pickle.load(f)
            f.close()
            found[a]=True
    if found[a]:
        # modify misifits to discern their traceplots
        misfit=-loglik[args.num_burnin:]-a*10
        axes[0].plot(misfit,next(linecycler0),linewidth=1.25)
        # pd.tools.plotting.autocorrelation_plot(loglik[1000:], ax=axes[1],linestyle=next(linecycler))
        acorr_misfit=autocorr(misfit)
        axes[1].plot(range(1000),acorr_misfit[:1000],next(linecycler1),linewidth=1.25)

plt.axes(axes[0])
plt.axis('tight')
plt.xlabel('iteration',fontsize=14); plt.ylabel('data-misfit (offset)',fontsize=14)
plt.legend(np.array(alg_names)[found],fontsize=11.5,loc=3,ncol=num_algs,bbox_to_anchor=(0,1.02,1.,0.102))
plt.axes(axes[1])
# plt.axis([0,100,-1,1])
plt.axis('tight')
plt.xlabel('lag',fontsize=14); plt.ylabel('auto-correlation',fontsize=14)
fig.tight_layout(rect=[0,0,1,.9])
plt.savefig('./analysis/misfit_acf.png')

plt.show()
