"""
Main function to run Laminar-Jet model to generate posterior samples
Shiwei Lan @ U of Warwick, 2016
"""

import os,argparse,pickle
from dolfin import *
import numpy as np
# PDE
from Laminar import Laminar

# MCMC
from geoinfMC import geoinfMC
from geom_laminar import geom

np.set_printoptions(precision=3, suppress=True)
np.random.seed(2016)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('algNO', nargs='?', type=int, default=0)
    parser.add_argument('dim', nargs='?', type=int, default=100)
#     parser.add_argument('num_obs', nargs='?', type=int, default=100)
    parser.add_argument('num_samp', nargs='?', type=int, default=10000)
    parser.add_argument('num_burnin', nargs='?', type=int, default=1000)
    parser.add_argument('step_sizes', nargs='?', type=float, default=[.0001,.00015,.00015,3.0,3.0,2.0,1.45])
    parser.add_argument('step_nums', nargs='?', type=int, default=[1,1,4,1,4,1,4])
    parser.add_argument('algs', nargs='?', type=str, default=('pCN','infMALA','infHMC','infmMALA','infmHMC','splitinfmMALA','splitinfmHMC'))
    parser.add_argument('trct_opt', nargs='?', type=int, default=3)
    parser.add_argument('trct_idx', nargs='?', type=int, default=[])
    args = parser.parse_args()


    # define the PDE problem
    theta_truth=np.random.randn(args.dim)
    unit=.1;nx=30;ny=30;nu=3.0e-2;beta=0.3;stokes=False;cont=False
    laminar=Laminar(unit=unit,nx=nx,ny=ny,nu=nu,beta=beta,stokes=stokes,cont=cont)
    sigma=.5;alpha=1;s=.8
    inflow=laminar.inflow_profile(theta=theta_truth,sigma=sigma,alpha=alpha,s=s)

    # obtain observations
    print('Obtaining observations...')
    obs,idx,loc=laminar.get_obs(inflow)
    num_obs=len(idx)
    print('%d observations have been obtained!' % num_obs)
    # add some noise
    var_obs=1e-4 # observation noise level
    obs += np.sqrt(var_obs)*np.random.randn(num_obs)

    # define data misfit class
    print('Defining data-misfit...')
    # smaller problem to reduce waiting time
#     red_num_obs = np.random.randint(1,np.ceil(num_obs/2))
#     obs=obs[:red_num_obs]; idx=idx[:red_num_obs]; loc=loc[:red_num_obs,]
#     print('Reduced to %d observations to save waiting time for results.' % red_num_obs)
    red_idx = range(num_obs); red_idx = red_idx[::5]
    print('Reduced to %d observations to save waiting time for results.' % len(red_idx))
    misfit=laminar.data_misfit(obs[red_idx],1./var_obs,idx[red_idx],loc[red_idx,])


    # run MCMC to generate samples
    print("Preparing %s sampler with step size %g for %d step(s)..."
          % (args.algs[args.algNO],args.step_sizes[args.algNO],args.step_nums[args.algNO]))

    if 'split' in args.algs[args.algNO]:
        args.trct_opt = 2
        args.trct_idx = range(30)
        print('and truncating on '+{0:'value, gradient, and metric (no sense)',
                                    1:'gradient and metric',
                                    2:'metric',
                                    3:'none'}[args.trct_opt]+'...')

    geomfun=lambda theta,geom_opt: geom(theta,inflow,laminar,misfit,geom_opt,args.trct_opt,args.trct_idx)
    inf_MC=geoinfMC(np.zeros(args.dim),np.eye(args.dim),geomfun,args.step_sizes[args.algNO],args.step_nums[args.algNO],args.algs[args.algNO],args.trct_idx)
    mc_fun=inf_MC.sample
    mc_args=(args.num_samp,args.num_burnin)
    mc_fun(*mc_args)

    # append PDE information including the count of solving
    filename_=os.path.join(inf_MC.savepath,inf_MC.filename+'.pckl')
    filename=os.path.join(inf_MC.savepath,'Laminar_'+inf_MC.filename+'.pckl') # change filename
    os.rename(filename_, filename)
    f=open(filename,'ab')
    soln_count=laminar.soln_count.copy()
    pickle.dump([unit,nx,ny,nu,beta,stokes,cont,theta_truth,sigma,alpha,s,var_obs,red_idx,soln_count,args],f)
    f.close()
#     # verify with load
#     f=open(filename,'rb')
#     mc_samp=pickle.load(f)
#     pde_info=pickle.load(f)
#     f.close
#     print(pde_cnt)

if __name__ == '__main__':
    main()
