"""
A wrapper of Laminar-Jet model get_geom to feed Geometric quantities including nll,g,FI,cholG in MCMC samplers
Shiwei Lan @ U of Warwick, 2016
"""

import numpy as np
from Laminar import Laminar

def geom(theta,inflow,PDE,obj,geom_opt=[0],trct_opt=3,trct_idx=[]):
    nll=None; g=None; FI=None; cholG=None;

    # obtain gradient of data-misfit
    inflow.theta=theta
    nll,dnll,_,FI=PDE.get_geom(inflow,obj,geom_opt,trct_opt,trct_idx)

    # dimension
    D=len(theta);
    if not any(trct_idx):
        trct_idx = range(D)

    if any(s>0 for s in geom_opt):
        g=np.zeros(D)
        if 2 in geom_opt:
            g[trct_idx]=FI.dot(theta[trct_idx])
            cholG=np.linalg.cholesky(FI+np.eye(FI.shape[0]))
        if trct_opt==1: # dnll of size num_trct
            g[trct_idx]-=dnll;
        else: # dnll of size D
            g-=dnll

    return nll,g,FI,cholG
#     return {0:nll,1:g,2:(FI,cholG)}
