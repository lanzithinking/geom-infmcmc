from multiprocessing import Pool

def f(x):
    return x*x

if __name__ == '__main__':
    p = Pool(4)
    res = p.map(f, range(20))
    print(res)