"""
Plot observations and some solutions of Laminar-Jet PDE
Shiwei Lan @ U of Warwick, 2016
"""

from dolfin import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mp

from Laminar import Laminar

np.random.seed(2016)
# generate data
theta_dim=100
# define the PDE problem
# theta=.1*np.ones(theta_dim)#np.random.randn(theta_dim)
theta=np.random.randn(theta_dim)
laminar=Laminar(unit=.1,nx=30,ny=30,nu=3.0e-2,beta=0.3,stokes=False)
sigma=.5;alpha=1;s=.8
inflow=laminar.inflow_profile(theta=theta,sigma=sigma,alpha=alpha,s=s)
# obtain observations
obs,idx,loc=laminar.get_obs(inflow)
loc=loc[::5,]

# plot
fig,axes = plt.subplots(nrows=1,ncols=2,sharex=True,figsize=(14,5))

# plot observations
plt.axes(axes[0])
parameters["plotting_backend"]="matplotlib"
plot(laminar.mesh)
plt.plot(loc[:,0],loc[:,1],'bo',markersize=12)
plt.axvline(x=0,color='r',linewidth=5)
plt.axis('tight')
plt.xlabel('x',fontsize=12); plt.ylabel('y',fontsize=12)
plt.title('Observations on outlet boundary',fontsize=12)

# plot solutions
plt.axes(axes[1])
u_fwd,p_fwd,_=laminar.states_fwd.split(True)
sub_fig=plot(p_fwd)
# plt.colorbar(sub_fig)
plot(u_fwd,scale=1.5)
plt.axis('tight')
plt.xlabel('x',fontsize=12); plt.ylabel('y',fontsize=12)
plt.title('Solutions of velocity and pressure',fontsize=12)

cax,kw = mp.colorbar.make_axes([ax for ax in axes.flat])
plt.colorbar(sub_fig, cax=cax, **kw)

# fig.tight_layout()
plt.savefig('./result/obs_solns.png',bbox_inches='tight')

plt.show()
