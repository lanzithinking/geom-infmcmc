"""
Test solutions of Laminar-Jet PDE
Shiwei Lan @ U of Warwick, 2016
"""


from dolfin import *
import numpy as np
import time
import matplotlib.pyplot as plt

from Laminar import Laminar

# parameters["num_threads"] = 2

# np.random.seed(2016)
# generate data
dim=100
var_obs=1e-2

# generate observations
# theta=.1*np.ones(10)#np.random.randn(dim)
theta=np.random.randn(dim)
laminar=Laminar(unit=.1,nx=40,ny=40,nu=2.0e-2,beta=0.3,stokes=False)
inflow=laminar.inflow_profile(theta=theta,sigma=.5,alpha=1,s=.8)

# solve forward equation
# u_fwd,p_fwd,l_fwd=laminar.soln_fwd(theta)

# obtain observations
print('Obtaining observations...')
obs,idx,loc=laminar.get_obs(inflow)
# print(idx)
# print(obs)
# print(loc)
num_obs=len(idx)
print('%d observations have been obtained!' % num_obs)
# add some noise
obs += np.sqrt(var_obs)*np.random.randn(num_obs)
  
# define data misfit class
print('\nDefining data-misfit...')
# smaller problem to reduce waiting time
# red_num_obs = np.random.randint(1,np.ceil(num_obs/2))
# obs=obs[:red_num_obs]; idx=idx[:red_num_obs]; loc=loc[:red_num_obs,]
# obs=obs[::4]; idx=idx[::4]; loc=loc[::4,]
# red_num_obs = len(idx)
# print('Reduced to %d observations to save waiting time for results.' % red_num_obs)
misfit=laminar.data_misfit(obs,1./var_obs,idx,loc)

# obtain the geometric quantities
print('\n\nObtaining geometric quantities with Adjoint method...')
start = time.time()
ord=3
nll,dnll,Fv,FI = laminar.get_geom(inflow,misfit,[0,1,1.5,2])
if dnll is not None:
    print('gradient:')
    print(dnll)
v = np.random.randn(inflow.l)
if Fv is not None:
    Ma = Fv(v)
    print('metric action on a random vector:')
    print(Ma)
if FI is not None:
    print('metric:')
    print(FI)
# laminar.set_forms(inflow,opt=range(ord))
# _,_,_=laminar.soln_fwd()
# _,_,_=laminar.soln_adj(misfit)
# M = laminar.get_met(misfit)
# plot
# laminar.plot()
end = time.time()
print('Time used is %.4f' % (end-start))

print('The difference is: %.10f' % np.linalg.norm(Ma-FI.dot(v)))

# save solutions to file
# laminar.save()
# plot solutions
# laminar.plot(SAVE=False)

# plot solutions
# parameters["plotting_backend"]="matplotlib"
# u_fwd,p_fwd,_=laminar.states_fwd.split(True)
# plt.figure(0)
# fig=plot(p_fwd)
# plt.colorbar(fig)
# plot(u_fwd)
# plt.show()

