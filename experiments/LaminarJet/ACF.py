"""Functions to calculate Autocorrelation functions (ACF).

References:
http://stackoverflow.com/questions/643699/how-can-i-use-numpy-correlate-to-do-autocorrelation
http://stackoverflow.com/questions/14297012/estimate-autocorrelation-using-python
http://jespertoftkristensen.com/JTK/Blog/Entries/2013/12/29_Efficient_computation_of_autocorrelations%3B_demonstration_in_MatLab_and_Python.html
http://stackoverflow.com/questions/27541290/bug-of-autocorrelation-plot-in-matplotlib-s-plt-acorr
"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


def acorr(x, stats=True):
    """Return autocorrelation of a time series x."""
    if stats:
        x -= x.mean()  # in the statistics sense; or in signal processing sense
    autocorr = np.correlate(x, x, mode='full')
    autocorr = autocorr[x.size:]
    autocorr /= autocorr.max()
    return autocorr


def autocorr(x):
    """This one is closest to what plt.acorr does.
    http://stackoverflow.com/q/14297012/190597
    http://en.wikipedia.org/wiki/Autocorrelation#Estimation
    """
    n = len(x)
    variance = x.var()
    x = x - x.mean()
    r = np.correlate(x, x, mode='full')[-n:]
    assert np.allclose(r, np.array(
        [(x[:n - k] * x[-(n - k):]).sum() for k in range(n)]))
    result = r / (variance * (np.arange(n, 0, -1)))
    return result


def acf0(x, length=20):
    """Return autocorrelation of a time series x."""
    length = min([length, len(x) - 1])
    return np.array([1] + [np.corrcoef(x[:-i], x[i:])[0, 1]
                           for i in range(1, length)])


def acf_fft(x, length=20):
    """Return autocorrelation of a time series x using fft."""
    N = len(x)
    xvi = np.fft.fft(x, n=2 * N)
    acf = np.real(np.fft.ifft(xvi * np.conjugate(xvi))[:N])
    d = N - np.arange(N)
    acf = acf / d
    return acf[:min([length, N])]


def plot_acorr(x, ax=None):
    """plot autocorrelation of a time series x."""
    if ax is None:
        ax = plt.gca()
    x = x - x.mean()
    autocorr = np.correlate(x, x, mode='full')
    autocorr = autocorr[x.size:]
    autocorr /= autocorr.max()

    return ax.stem(autocorr)

if __name__ == '__main__':
    x = np.array([1, 2, 3, 1, 2, 2, 2, 4, 5], dtype=float)
    print(acorr(x))
    print(estimated_autocorrelation(x))
    print(acf0(x))
    print(acf_fft(x))
    fig, axes = plt.subplots(nrows=1, ncols=3, figsize=(12, 5))
    axes[0].acorr(x, maxlags=len(x) - 1)
    pd.tools.plotting.autocorrelation_plot(x, ax=axes[1])
    # plt.axis([0, x.size-1, -1, 1])
    plt.axes(axes[2])
    plot_acorr(x)
    fig.tight_layout()
    plt.show()
