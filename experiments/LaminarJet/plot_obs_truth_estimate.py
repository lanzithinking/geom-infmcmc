"""
Plot observations, true and estimated inflow velocity profile Laminar-Jet PDE
Shiwei Lan @ U of Warwick, 2016
"""

import os,pickle
from dolfin import *
import numpy as np
import matplotlib.pyplot as plt
from itertools import cycle

from Laminar import Laminar

np.random.seed(2016)
# generate data
theta_dim=100
# define the PDE problem
# theta=.1*np.ones(theta_dim)#np.random.randn(theta_dim)
theta=np.random.randn(theta_dim)
laminar=Laminar(unit=.1,nx=40,ny=40,nu=2.0e-2,beta=0.3,stokes=False)
sigma=.5;alpha=1;s=.8
inflow=laminar.inflow_profile(theta=theta,sigma=sigma,alpha=alpha,s=s)
# obtain locations of observations
bmesh = BoundaryMesh(laminar.mesh, "exterior", True)
bmesh_coordinates = bmesh.coordinates()
ind_on_outlet = [laminar.bdy_outlet.inside(Point(p),True) for p in bmesh_coordinates]
loc = bmesh_coordinates[np.where(ind_on_outlet)[0],]
# smaller problem to reduce waiting time
loc = loc[::4,]

# plot
fig,axes = plt.subplots(nrows=1,ncols=3,figsize=(12,5))

# plot observations
plt.axes(axes[0])
parameters["plotting_backend"]="matplotlib"
plot(laminar.mesh)
plt.plot(loc[:,0],loc[:,1],'bo',markersize=12)
plt.axvline(x=0,color='r',linewidth=5)
plt.axis('tight')
plt.xlabel('x',fontsize=12); plt.ylabel('y',fontsize=12)
plt.title('Observations on outlet boundary',fontsize=12)

# plot truth
plt.axes(axes[1])
dim_seq = [10,20,100]
y=np.linspace(-.5*laminar.Ly,.5*laminar.Ly,200+1)
# plt.gca().set_color_cycle(['red', 'green', 'blue', 'yellow'])
lines = ["--","-.","-",":"];linecycler = cycle(lines)
for i in range(len(dim_seq)):
    inflow=laminar.inflow_profile(theta=theta[:dim_seq[i]],sigma=sigma,alpha=alpha,s=s)
    u_inflow=interpolate(inflow,laminar.V_velocity0)
    u_truth=np.array([u_inflow(Point(0,k)) for k in y])
    plt.plot(y,u_truth,next(linecycler),linewidth=2)
# plt.axis([y.min(),y.max(),-1.0,2.5])
plt.axis('tight')
plt.xlabel('y',fontsize=12); plt.ylabel(r'u$_{\,\mathtt{inflow}}$',fontsize=12)
plt.title('True inflow velocity on inlet boundary',fontsize=12)
plt.legend([ss+' modes' for ss in map(str,dim_seq)],fontsize=10,loc='upper right')

# plot posterior mean
plt.axes(axes[2])
folder = './analysis_0'
for f in os.listdir(folder):
    if f.endswith('.pckl') and '_splitinfmMALA_' in f:
        f_=open(os.path.join(folder,f),'rb')
        _,_,_,samp,_,_,_,_=pickle.load(f_)
        f_.close()
        theta_est = np.mean(samp,axis=0)
inflow=laminar.inflow_profile(theta=theta_est,sigma=sigma,alpha=alpha,s=s)
u_inflow=interpolate(inflow,laminar.V_velocity0)
u_est=np.array([u_inflow(Point(0,k)) for k in y])
plt.plot(y,u_truth,color='r',linestyle='-',linewidth=2)
plt.plot(y,u_est,color='b',linestyle='--',linewidth=2)
plt.axis('tight')
plt.xlabel('y',fontsize=12); #plt.ylabel(r'u$_{\,\mathtt{inflow}}$',fontsize=12)
plt.title('Posterior mean of inflow velocity',fontsize=12)
plt.legend(['Truth','Estimate'],fontsize=10,loc='upper right')

fig.tight_layout()
plt.savefig('./result/obs_truth_est.png')

plt.show()