"""
Test solutions of Laminar-Jet PDE
Shiwei Lan @ U of Warwick, 2016
"""


from dolfin import *
import numpy as np
import time
import matplotlib.pyplot as plt

from Laminar import Laminar

# parameters["num_threads"] = 2

np.random.seed(2016)
# generate data
dim=100
var_obs=1e-2

# generate observations
# theta=.1*np.ones(10)#np.random.randn(dim)
theta=np.random.randn(dim)
laminar=Laminar(unit=.1,nx=100,ny=100,nu=4.0e-2,beta=0.5,stokes=True)
inflow=laminar.inflow_profile(theta=theta,sigma=1,alpha=1)

# ------------ weak boundary ---------------------#
laminar.set_forms(inflow)
_,_,_=laminar.soln_fwd()
u_fwd,p_fwd,_=laminar.states_fwd.split(True) # remember to turn off init_guess for simplicity
parameters["plotting_backend"]="matplotlib"
plt.figure(0)
plot(p_fwd)
plot(u_fwd)

# ------------ strong boundary ---------------------#
VELOCITY = 0; PRESSURE = 1;
INLET = 1; OUTLET = 2; BOUNDING = 3

unit=.1; Lx = 10*unit; Ly = 8*unit; nx=100; ny=100;
mesh = RectangleMesh(Point(0.0, -0.5*Ly), Point(Lx, 0.5*Ly), nx, ny)

bdy_inlet = AutoSubDomain(lambda x, on_boundary: near(x[0], 0.0) and on_boundary)
bdy_outlet = AutoSubDomain(lambda x, on_boundary: near(x[0], Lx) and on_boundary)
bdy_bounding = AutoSubDomain(lambda x, on_boundary: near(abs(x[1]), 0.5*Ly) and on_boundary)

boundaries = FacetFunction("size_t", mesh, 0)
ds = ds(subdomain_data=boundaries)

bdy_inlet.mark(boundaries, INLET)
bdy_outlet.mark(boundaries, OUTLET)
bdy_bounding.mark(boundaries, BOUNDING)

V_velocity = VectorFunctionSpace(mesh, 'CG', 2)
# V_velocity0 = FunctionSpace(mesh, 'CG', 2)
V_pressure = FunctionSpace(mesh, 'CG', 1)
V = MixedFunctionSpace([V_velocity, V_pressure])

u_inflow_expr = laminar.inflow_profile(theta=theta,sigma=1,alpha=1)
# u_inflow = interpolate(u_inflow_expr, V_velocity0)
bc_inlet = DirichletBC(V.sub(VELOCITY).sub(0), u_inflow_expr, boundaries, INLET)
bc_bounding = DirichletBC(V.sub(VELOCITY).sub(1),Constant(0.0), boundaries, BOUNDING)
ess_bc = [bc_bounding,bc_inlet]

states_init = Constant((0.0, 0.0, 0.0))
states_fwd=interpolate(states_init,V)
# states_fwd=Function(V)
u, p = split(states_fwd)
v, q = TestFunctions(V)
strain = lambda u: sym(grad(u))
neg = lambda u: (u-abs(u))/2
n = FacetNormal(mesh)
F = (
    laminar.nu*inner(strain(u),strain(v))*dx + Constant(1.0-laminar.stokes)*inner(grad(u)*u,v)*dx - p*div(v)*dx
    + Constant(1.0-laminar.stokes)*laminar.beta*neg(inner(u,n))*inner(u,v)*ds(OUTLET) - div(u)*q*dx
    )
# states_fwd.interpolate(states_init)

problem = NonlinearVariationalProblem(F, states_fwd, ess_bc, J=derivative(F, states_fwd))
solver = NonlinearVariationalSolver(problem)
solver.parameters['newton_solver']["relative_tolerance"] = 1e-5
solver.parameters['newton_solver']['error_on_nonconvergence'] = True
solver.solve()
u1_fwd, p1_fwd = states_fwd.split(True)
plt.figure(1)
plot(p1_fwd)
plot(u1_fwd)
plt.show()

diff_p = p_fwd.vector()-p1_fwd.vector()
diff_u = u_fwd.vector()-u1_fwd.vector()
print('Difference in pressure between weak and strong conditions: %.10f (inf-norm) and %.10f (2-norm)' % (np.linalg.norm(diff_p,np.inf),np.linalg.norm(diff_p)))
print('Difference in velocity between weak and strong conditions: %.10f (inf-norm) and %.10f (2-norm)' % (np.linalg.norm(diff_u,np.inf),np.linalg.norm(diff_u)))