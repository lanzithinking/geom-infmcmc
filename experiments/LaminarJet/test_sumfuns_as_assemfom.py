"""
Test how to write sum of functions as an assembled form
"""
from dolfin import *
import numpy as np

# np.random.seed(2016)

mesh = UnitSquareMesh(20,20)
V = FunctionSpace(mesh,'CG',1)

# pts = np.random.rand(4,2)
# pts = mesh.coordinates()
n = V.dim();  d = 2
# dof_coordinates = V.dofmap().tabulate_all_coordinates(mesh)
dof_coordinates = V.tabulate_dof_coordinates()
dof_coordinates.resize((n, d))
pts = dof_coordinates

class KL_expansion(Expression): # generic class of Karhunen-Loeve expansion # TODO: write cpp code
    def __init__(self,theta,sigma=1.2,alpha=0.1,s=0.6):
        self.theta=theta
        self.sigma=sigma
        self.alpha=alpha
        self.s=s
        self.l=len(self.theta)
    # K-L expansion of theta ~ GP(0,C)
    def eval(self,value,x):
        seq0l = np.linspace(0,self.l-1,self.l)
        value[0] = self.sigma*self.theta.dot(pow(self.alpha+(pi*seq0l)**2,-self.s/2)*np.cos(pi*seq0l*x[1]))

theta = np.random.randn(10)
fun_expr = KL_expansion(theta)
fun = interpolate(fun_expr, V)

fun_vals = sum([fun(list(p)) for p in pts])
print(fun_vals)

# calibrating with cell volume
fun_form = fun/CellVolume(mesh)*dx # doesn't work for space other than DG0
fun_assemb = assemble(fun_form)
print('\nCalibrating with cell volume only works in DG0 space:')
print(fun_assemb)
print(abs(fun_vals-fun_assemb))

# PointSource
delta_meas = [PointSource(V,Point(p),fun(list(p))) for p in pts]
v=TestFunction(V)
fun_form1 = Constant(0.0)*v*dx
fun_form1_assemb = assemble(fun_form1)
[delta.apply(fun_form1_assemb) for delta in delta_meas]
fun_vals1 = sum(fun_form1_assemb.array())
print('\nPoint Source is nice but has to be applied on vector and the associated function space must be scalar:')
print(fun_vals1)
print(abs(fun_vals-fun_vals1))

# # other failing thoughts about PointSource
# w=Function(V)
# [delta.apply(w.vector()) for delta in delta_meas]
# fun_form2 = w*dx
# fun_assemb2 = assemble(fun_form2)
# print(fun_assemb2)
# print(abs(fun_vals-fun_assemb2))
# 
# fun_vals2 = sum([w(list(p)) for p in pts])
# print(fun_vals2)
# print(abs(fun_vals-fun_vals2))

###### point integral ######
def near_pts(x):
    return any([near(x[0],p[0]) and near(x[1],p[1]) for p in pts])

pts_domain = VertexFunction("size_t", mesh, 0)
pts_nbhd = AutoSubDomain(near_pts)
pts_nbhd.mark(pts_domain, 1)
dPP = dP(subdomain_data=pts_domain)

fun_form3 = fun*dPP(1)
fun_assemb3 = assemble(fun_form3)
print('\nFinally point measure does the right thing!')
print(fun_assemb3)
print(abs(fun_vals-fun_assemb3))
