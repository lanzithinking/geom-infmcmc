"""
Test dof coordinates
"""
from dolfin import *

mesh = RectangleMesh(Point(0.0, 0.0), Point(2.0, 1.5), 10, 10)

V = VectorFunctionSpace(mesh, 'CG', 2)

dofmap = V.dofmap()
dofs = dofmap.dofs()

dof_coordinates = V.tabulate_dof_coordinates()
print(type(dof_coordinates))
print(dof_coordinates.shape)