"""
Plot log-likelihood of Laminar-Jet inverse problem
Shiwei Lan @ U of Warwick, 2016
"""

import os,pickle
from dolfin import *
import numpy as np
import matplotlib.pyplot as plt
from itertools import cycle

from Laminar import Laminar

algs=('pCN','infMALA','infHMC','infmMALA','infmHMC','splitinfmMALA','splitinfmHMC')
num_algs=len(algs)
found = np.zeros(num_algs,dtype=np.bool)

folder = './analysis_0'
fnames=[f for f in os.listdir(folder) if f.endswith('.pckl')]

idx4plot=range(200)

# plot log-likelihood
fig = plt.figure(figsize=(8,5))
lines = ["-","--","-.",":"];linecycler = cycle(lines)

for a in range(num_algs):
    for f_i in fnames:
        if '_'+algs[a]+'_' in f_i:
            f=open(os.path.join(folder,f_i),'rb')
            _,_,_,_,loglik,_,_,_=pickle.load(f)
            f.close()
            found[a]=True
    if found[a]:
        plt.plot(idx4plot,loglik[idx4plot],next(linecycler),linewidth=2)

plt.axis('tight')
plt.xlabel('iteration',fontsize=12); plt.ylabel('data-misfit',fontsize=12)
plt.legend(np.array(algs)[found],ncol=2,fontsize=10,loc='lower right')

fig.tight_layout()
plt.savefig('./result/convergence.png')

plt.show()