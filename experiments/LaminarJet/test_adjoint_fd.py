"""
Test solutions of Laminar-Jet PDE
Shiwei Lan @ U of Warwick, 2016
"""


from dolfin import *
import numpy as np
import time
# import matplotlib.pyplot as plt

from Laminar import Laminar

# parameters["num_threads"] = 2

np.random.seed(2016)
# generate data
dim=10
var_obs=1e-2

# generate observations
# theta=.1*np.ones(10)#np.random.randn(dim)
theta=np.random.randn(dim)
laminar=Laminar(unit=.1,nx=50,ny=50,nu=4.0e-2,beta=0.4,stokes=False)
inflow=laminar.inflow_profile(theta=theta,sigma=1,alpha=1)

# solve forward equation
# u_fwd,p_fwd,l_fwd=laminar.soln_fwd(theta)

# obtain observations
print('Obtaining observations...')
obs,idx,loc=laminar.get_obs(inflow)
# print(idx)
# print(obs)
# print(loc)
num_obs=len(idx)
print('%d observations have been obtained!' % num_obs)
# add some noise
obs += np.sqrt(var_obs)*np.random.randn(num_obs)
  
# define data misfit class
print('\nDefining data-misfit...')
# smaller problem to reduce waiting time
# red_num_obs = np.random.randint(1,np.ceil(num_obs/2))
# obs=obs[:red_num_obs]; idx=idx[:red_num_obs]; loc=loc[:red_num_obs,]
obs=obs[::10]; idx=idx[::10]; loc=loc[::10,]
red_num_obs = len(idx)
print('Reduced to %d observations to save waiting time for results.' % red_num_obs)
misfit=laminar.data_misfit(obs,1./var_obs,idx,loc)

# ------------ early test ---------------------#
# laminar.set_forms(theta)
# u0,_,_=laminar.soln_fwd()
# u1,_,_=laminar.states_fwd.split(True)
# u2=u1.vector()
#
# print('Data-misfit: % .10f' % misfit.eval(u0))
# print('Data-misfit: % .10f' % misfit.eval(u1))
# print('Data-misfit: % .10f' % misfit.eval(u2))
#        
# # test misfit as functional for adjoint
# J_form = misfit.form(u0)
# J_assemb = assemble(J_form)
# print('Assembled data-misfit form: % .10f' % J_assemb)
# J_func = misfit.func(u0)
# J_value = sum([J_func(list(p)) for p in loc])
# print('Evaluated data-misfit function: % .10f' % J_value)
#
# # solve adjoint equation
# # u_adj,p_adj,l_adj=laminar.soln_adj(misfit)
# 
# # obtain gradient of data-misfit
# g = laminar.get_grad(misfit)
# print(g)
# 
# # solve 2nd forward equation
# u_actedon = np.random.randn(len(theta))
# # u_fwd2,p_fwd2,l_fwd2=laminar.soln_fwd2(u_actedon)
# 
# # solve 2nd adjoint equation
# # u_adj,p_adj2,l_adj2=laminar.soln_adj2()
# 
# # obtain metric action
# Ma = laminar.get_metact(u_actedon)
# print (Ma)


# ------------ adjoint method ---------------------#

# obtain the geometric quantities
print('\n\nObtaining geometric quantities with Adjoint method...')
start = time.time()
nll,dnll,Fv,FI = laminar.get_geom(inflow,misfit,[0,1,1.5,2])
if dnll is not None:
    print('gradient:')
    print(dnll)
v = np.random.randn(inflow.l)
if Fv is not None:
    Ma = Fv(v)
    print('metric action on a random vector:')
    print(Ma)
if FI is not None:
    print('metric:')
    print(FI)
# plot
# laminar.plot()
end = time.time()
print('Time used is %.4f' % (end-start))

# save solutions to file
# laminar.save()
# plot solutions
# laminar.plot_vtk()
# laminar.plot_mat()
# laminar.plot()

# ------------ finite difference ---------------------#

# check with finite difference
print('\n\nTesting against Finite Difference method...')
start = time.time()
h = 1e-4
theta1 = theta.copy(True);
 
## gradient
print('\nFirst gradient:')
dnll_fd = np.zeros_like(dnll)
for i in range(len(theta)):
    theta1[i]+=h; inflow.theta=theta1
    nll_p,_,_,_ = laminar.get_geom(inflow,misfit)
    theta1[i]-=2*h; inflow.theta=theta1
    nll_m,_,_,_ = laminar.get_geom(inflow,misfit)
    dnll_fd[i] = (nll_p-nll_m)/(2*h)
    theta1[i]+=h;
print('gradient:')
print(dnll_fd)
diff_grad = dnll_fd-dnll
print('Difference in gradient between adjoint and finite difference: %.10f (inf-norm) and %.10f (2-norm)' % (np.linalg.norm(diff_grad,np.inf),np.linalg.norm(diff_grad)))
 
## metric-action
print('\nThen Metric-action:')
Ma_fd = np.zeros_like(Ma)
# obtain sensitivities
for n in range(len(idx)):
    misfit_n=laminar.data_misfit(obs[n],1./var_obs,idx[n],loc[None,n,])
    dudtheta=np.zeros_like(theta)
    for i in range(len(theta)):
        theta1[i]+=h; inflow.theta=theta1
        laminar.set_forms(inflow)
        u_p,_,_ = laminar.soln_fwd()
        u_p_vec = misfit_n.extr_sol_vec(u_p)
        theta1[i]-=2*h; inflow.theta=theta1
        laminar.set_forms(inflow)
        u_m,_,_ = laminar.soln_fwd()
        u_m_vec = misfit_n.extr_sol_vec(u_m)
        dudtheta[i]=(u_p_vec-u_m_vec)/(2*h)
        theta1[i]+=h;
    Ma_fd += dudtheta*(dudtheta.dot(v))
Ma_fd *= misfit.prec
print('metric action on a random vector:')
print(Ma_fd)
diff_Ma = Ma_fd-Ma
print('Difference in metric-action between adjoint and finite difference: %.10f (inf-norm) and %.10f (2-norm)' % (np.linalg.norm(diff_Ma,np.inf),np.linalg.norm(diff_Ma)))
end = time.time()
print('Time used is %.4f' % (end-start))

# ------------ strong boundary ---------------------#

# laminar.set_forms(theta)
# _,_,_=laminar.soln_fwd()
# u_fwd,p_fwd,_=laminar.states_fwd.split(True) # remember to turn off init_guess for simplicity
# parameters["plotting_backend"]="matplotlib"
# plt.figure(0)
# plot(p_fwd)
# plot(u_fwd)
# 
# # pose strong boundary condition
# V_pressure = FunctionSpace(laminar.mesh, 'CG', 1)
# V = MixedFunctionSpace([laminar.V_velocity, V_pressure])
# 
# u_inflow_expr = laminar.KL_expansion(theta)
# u_inflow = interpolate(u_inflow_expr, laminar.V_velocity0)
# bc_inlet = DirichletBC(V.sub(laminar.VELOCITY).sub(0), u_inflow, laminar.boundaries, laminar.INLET)
# ess_bc = [laminar.ess_bc[0],bc_inlet]
# 
# states_init = Constant((0.0, 0.0, 0.0))
# states_fwd=interpolate(states_init,V)
# u, p = split(states_fwd)
# v, q = TestFunctions(V)
# strain = lambda u: sym(grad(u))
# neg = lambda u: (u-abs(u))/2
# n = FacetNormal(laminar.mesh)
# F = (
#     laminar.nu*inner(strain(u),strain(v))*dx + Constant(1.0-laminar.stokes)*inner(grad(u)*u,v)*dx - p*div(v)*dx
#     + Constant(1.0-laminar.stokes)*laminar.beta*neg(inner(u,n))*inner(u,v)*laminar.ds(laminar.OUTLET) - div(u)*q*dx
#     )
# 
# problem = NonlinearVariationalProblem(F, states_fwd, ess_bc, J=derivative(F, states_fwd))
# solver = NonlinearVariationalSolver(problem)
# solver.parameters['newton_solver']["relative_tolerance"] = 1e-5
# solver.parameters['newton_solver']['error_on_nonconvergence'] = True
# solver.solve()
# u1_fwd, p1_fwd = states_fwd.split(True)
# plt.figure(1)
# plot(p1_fwd)
# plot(u1_fwd)
# plt.show()
# 
# diff_p = p_fwd.vector()-p1_fwd.vector()
# diff_u = u_fwd.vector()-u1_fwd.vector()
# print('Difference in pressure between weak and strong conditions: %.10f (inf-norm) and %.10f (2-norm)' % (np.linalg.norm(diff_p,np.inf),np.linalg.norm(diff_p)))
# print('Difference in velocity between weak and strong conditions: %.10f (inf-norm) and %.10f (2-norm)' % (np.linalg.norm(diff_u,np.inf),np.linalg.norm(diff_u)))