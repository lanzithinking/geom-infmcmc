%%% This is to plot how posteriors of samples deviate from Gaussian. %%%

clear;
addpath('~/Documents/MATLAB/tight_subplot/');
addpath('../../')
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

% algorithms
% alg={'pCN','infMALA','HHMC','infMMALA','splitinfMMALA'};
alg={'splitinfMMALA'};
alg_name={'pCN','$$\infty$$-MALA','H-HMC','$$\infty$$-MMALA','split$$\infty$$-MMALA'};
Nalg=length(alg);

% dimension setting
ncmp = 10; D = ncmp^2;
dev=zeros(D,2);
dev_name={'difference in CDF','K-L divergence'};

% estimate the deviation measure
% load data
files = dir('./summary');
nfiles = length(files) - 2;

for i=1:Nalg
    for j=1:nfiles
        if ~isempty(strfind(files(j+2).name,['_',alg{i},'_D',num2str(D),'_']))
                    load(strcat('./summary/', files(j+2).name));
        end
    end
    % criterion 1: difference in CDF
    tic;
    dev(:,1)=deviation2Gaussian(samp);
    toc
    % criterion 2: K-L divergence
    tic;
    dev(:,2)=deviation2Gaussian(samp,2);
    toc
end

 % set plot
fig1=figure(1); clf;
% set(fig1,'windowstyle','docked');
set(fig1,'pos',[0 800 800 400]);
% fig=figure('visible','off');
ha=tight_subplot(1,2,[.16 0.1],[.12,.08],[.07,.05]);

% plot
for i=1:2
    subplot(ha(i));
    devinmat=reshape(dev(:,i),ncmp,ncmp);
    imagesc(devinmat);
    title(dev_name{i},'fontsize',18,'interpreter','latex');
    xlabel('d_1','fontsize',15);ylabel('d_2','fontsize',15,'rot',0);
    ylabh=get(gca,'YLabel');set(ylabh,'Position',get(ylabh,'Position') - [.5 0 0]);
    set(gca,'Ydir','normal');
    set(gca,'FontSize',15);
    colorbar;
end

% save to file
% print(fig,'-dpng','./figure/deviation2Gaussian');