%%%% This is MCMC sampling by split-infmHMC %%%%
clear;
addpath('../','../../sampler/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

% mesh setting
h=0.2;
% setting for solving PDE
s=1.2; ncmp = 10; D = ncmp^2;
Bi=.1; opt='Fourier';
try
    % load data
    load(['ThermalFin_h',num2str(h),'_',opt,'.mat']);
catch
    % prepare PDE setup for inference
    PDE4inf=PDEsetup(h,Bi,D,opt,s);
    % observations on dOmega\Gamma
    e_idx=unique(PDE4inf.e(1:2,:));
    x=PDE4inf.p(:,e_idx)';
    x((x(:,1)>-.5&x(:,1)<.5)&(x(:,2)==0),:)=[];
    % show positions on the geometry plot
    % pdeplot(PDE4inf.p,PDE4inf.e,PDE4inf.t);hold on;plot(x(:,1),x(:,2),'ko','markersize',7);hold off;
    % solve PDE on a finer mesh for observations
    PDE4obs=PDErefine(PDE4inf);
    u=PDEsol4obs(PDE4obs);
    % observation operator
    O=sparse(pdist2(x,PDE4obs.p','chebychev','smallest',1)<1e-7)';
    u=u(O);
    % model parameters
    sigmay=.01*max(u); sigma2y=sigmay^2; mtheta=0; sigma2theta=1;
    y=u+sigmay.*randn(length(u),1);
    % update observation operator for inferece
    O=sparse(pdist2(x,PDE4inf.p','chebychev','smallest',1)<1e-7)';
    % set up objective
    obj=misfit(x,y,sigma2y,O);
    % save data
    save(['ThermalFin_h',num2str(h),'_',opt,'.mat'],'seed','h','PDE4inf','PDE4obs','obj','mtheta','sigma2theta');
end

% sampling setting
stepsz = 1; Nleap = 4;

% allocation to save
Nsamp = 6000; NBurnIn = 1000;
samp = zeros(Nsamp-NBurnIn,D);
loglik = zeros(Nsamp,1);
accp = 0; % online acceptance
acpt = 0; % final acceptance rate
global pde_sol_cnt
pde_sol_cnt=zeros(4,1);

% truncation number
rtrct=5; ntrct=rtrct^2;
% index of truncation: indicator of being kept
ind_trct=logical(sparse(ncmp,ncmp)); ind_trct(1:rtrct,1:rtrct)=1;

% Initialize
theta = randn(D,1);
[nll,g,FI_f,cholG_f] = GEOM4inf(theta,PDE4inf,ind_trct,obj,mtheta,sigma2theta,[0,1,2],[0,0,1]);

disp(' ');
disp('Running split inf-mHMC...');
for Iter = 1:Nsamp
    
    % display every 100 iterations
    if mod(Iter,100) == 0
        disp([num2str(Iter) ' iterations completed.']);
        disp(['Current acceptance: ',num2str(accp/100)]);
        accp=0;
    end
    
    % sample with splitinfmHMC
    [theta,nll,g,FI_f,cholG_f,acpt_idx] = splitinfmHMC(theta,ind_trct,nll,g,FI_f,cholG_f,sqrt(sigma2theta).*speye(D),...
                                                       @(theta,ind_trct)GEOM4inf(theta,PDE4inf,ind_trct,obj,mtheta,sigma2theta,[0,1,2],[0,0,1]),stepsz,Nleap);
    accp = accp + acpt_idx;
    
    % Start timer after burn-in
    if Iter == NBurnIn
        disp('Burn-in complete, now drawing samples.'); tic;
    end
    
    loglik(Iter) = -nll;
    % Save samples if required
    if Iter > NBurnIn
        samp(Iter-NBurnIn,:) = theta';
        acpt = acpt + acpt_idx;
    end
    
end

% save results
time=toc;
acpt=acpt/(Nsamp-NBurnIn);
curTime = regexprep(num2str(fix(clock)),'    ','_');
curfile=['ThermalFin_splitinfmHMC_D',num2str(D),'_ntrct',num2str(ntrct),'__', curTime];
save(['result/',curfile,'.mat'],'h','PDE4inf','ntrct','stepsz','Nleap','samp','loglik','acpt','time','pde_sol_cnt');
disp(' ');
disp(['Accpetance Rate of splitinfmHMC: ',num2str(acpt)]);
disp(' ');
addpath('./result/');
CalculateStatistics(curfile,'./result/');
% some plots
fig1=figure(1); set(fig1,'pos',[0 800 900 300]);
idx=floor(linspace(1,size(samp,1),min([1e4,size(samp,1)])));
dim=[1,2,floor(D/2),D];
subplot(1,3,1);
plot(samp(idx,dim));
subplot(1,3,2);
plotmatrix(samp(idx,dim));
subplot(1,3,3);
plot(loglik);