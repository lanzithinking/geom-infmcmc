%%%% right hand side f in adjoint Elliptic PDE wrt misfit function %%%%

function [ft,fn]=f_adj(p,t,u,time,obj,tol)

if nargin<6
    tol=1e-7;
end
ft=[];fn=[];

if ~isempty(obj.O)
    O=obj.O(:);
else
    O=sparse(pdist2(obj.x,p','chebychev','smallest',1)<tol)';
end

Y=sparse(size(u,1),size(u,2)); Y(logical(O))=obj.y;

fn=-(Y-u).*O./obj.sigma2y; % f at nodes
if nargout==2
    return;
end
ft=pdeintrp(p,t,fn); % f at triangle midpoints

end