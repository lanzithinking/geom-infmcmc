%%%% Definition of PDE problem %%%%
%%%% specify the solution domain, mesh and boudnary conditions %%%%
% This is thermal conductivity problem %
classdef PDE
    % data of obj
    properties
        seedno=year(date);
        h=0.2; % maximum edge size
        Bi=0.1; % Boit number
        g;
        p;e;t;
        Npde=1;
        b;
        PLOT=0;
        ref_times=1; % the number of times to refine mesh
        parD=0; % default dimension of parameters
        opt='Fourier'; % option for K-L expansion
        s;sigma2;l;
        eigv;eigf; % eigen-pairs
    end
    % evaluation
    methods
        % constructor
        function obj=PDE(h,Bi,D,opt,varargin)
            switch nargin
                case 0
                    
                case 1
                    obj.h=h;
                case 2
                    obj.h=h; obj.Bi=Bi;
                case 3
                    obj.h=h; obj.Bi=Bi; obj.parD=D;
                case 4
                    obj.h=h; obj.Bi=Bi; obj.parD=D; obj.opt=opt;
                otherwise
                    error('Wrong number of inputs!');
            end
                    
            
            % Random Numbers...
            seed = RandStream('mt19937ar','Seed',obj.seedno);
            RandStream.setGlobalStream(seed); % force the same random seed
            
            % define solution domain
            load('ThermalFinDOMAIN.mat','g');
            obj.g=g;
            if obj.PLOT
                figure;
                pdegplot(obj.g, 'edgeLabels', 'on');
                title 'Geometry With Edge Labels Displayed';
            end
            
            % creat triangular mesh
            [obj.p,obj.e,obj.t] = initmesh(obj.g, 'Hmax', obj.h);
            if obj.PLOT
                figure;
                pdeplot(obj.p,obj.e,obj.t);
                axis equal
                title 'Domain With Mesh'
                xlabel 'X-coordinate'
                ylabel 'Y-coordinate'
            end
            
            % Create a pde entity for a PDE of u
            obj.b = createpde(obj.Npde);
            % Create a geometry entity
            geometryFromEdges(obj.b,obj.g);
            % specify constant boundary conditions
            applyBoundaryCondition(obj.b,'Edge',1,'g',1);
            applyBoundaryCondition(obj.b,'Edge',2:size(obj.g,2),'q',obj.Bi);
            
            if obj.parD~=0
                % prepare for the PDE coefficients
                Nt = size(obj.t,2);
                % Triangle point indices
                it1 = obj.t(1,:);
                it2 = obj.t(2,:);
                it3 = obj.t(3,:);
                % Find centroids of triangles
                centroid = (obj.p(:,it1)+obj.p(:,it2)+obj.p(:,it3))./3;
                
                % eigen-pairs of covariance kernel
                switch obj.opt
                    case 'Fourier'
                        % 2d fourier basis
                        if isempty(varargin)
                            varargin{1}=1.1; % parameter to control smoothness of prior
                        end
                        ncmp = sqrt(obj.parD); s=varargin{1}; obj.s=s;
                        eigv = pi^(-s).*bsxfun(@(x,y)(x.^2+y.^2).^(-s/2),(1:ncmp)',1:ncmp);
                        cosix = cos(pi.*centroid(1,:)'*(1:ncmp)); cosiy = cos(pi.*centroid(2,:)'*(1:ncmp));
                        eigf = bsxfun(@times,cosix,reshape(cosiy,Nt,1,ncmp));
                    case 'PCA'
                        if isempty(varargin)
                            varargin{1}=1; % variance of GP in KL
                            varargin{2}=.1; % correlation length of GP
                        end
                        sigma2=varargin{1};l=varargin{2}; obj.sigma2=sigma2;obj.l=l;
                        C = sigma2.*exp(-pdist2(centroid',centroid').^2/(2*l^2));
                        % number of components in PCA
                        K=min([obj.parD,Nt]);
                        [evc,ev] = eigs(C,K,'LA'); % there are some approximations behind it, I guess, check C*evc./evc! but much faster
                        eigv = sqrt(diag(ev)); eigf = evc; % ok under fixed random seed
                    otherwise
                        error('Wrong option!');
                end
                
                % PDE coefficient decomposition
                obj.eigv=eigv; obj.eigf=eigf;
            end
        end
        % refine PDE object
        function obj=refine(obj,ref_times)
            if nargin==2
                obj.ref_times=ref_times;
            end
            
            % Random Numbers...
            seed = RandStream('mt19937ar','Seed',obj.seedno);
            RandStream.setGlobalStream(seed); % force the same random seed
            
            % refine mesh
            for i=1:obj.ref_times
                [obj.p,obj.e,obj.t] = refinemesh(obj.g,obj.p,obj.e,obj.t);
            end
            if obj.PLOT
                figure;
                pdeplot(obj.p,obj.e,obj.t);
                axis equal
                title 'Domain With Refined Mesh'
                xlabel 'X-coordinate'
                ylabel 'Y-coordinate'
            end
            
            % update boundary conditions
            applyBoundaryCondition(obj.b,'Edge',1,'g',1);
            applyBoundaryCondition(obj.b,'Edge',2:size(obj.g,2),'q',obj.Bi);
            
            if obj.parD~=0
                % prepare for the PDE coefficients
                Nt = size(obj.t,2);
                % Triangle point indices
                it1 = obj.t(1,:);
                it2 = obj.t(2,:);
                it3 = obj.t(3,:);
                % Find centroids of triangles
                centroid = (obj.p(:,it1)+obj.p(:,it2)+obj.p(:,it3))./3;
                
                % eigen-pairs of covariance kernel
                switch obj.opt
                    case 'Fourier'
                        % 2d fourier basis
                        ncmp = sqrt(obj.parD);
                        obj.eigv = pi^(-obj.s).*bsxfun(@(x,y)(x.^2+y.^2).^(-obj.s/2),(1:ncmp)',1:ncmp);
                        cosix = cos(pi.*centroid(1,:)'*(1:ncmp)); cosiy = cos(pi.*centroid(2,:)'*(1:ncmp));
                        obj.eigf = bsxfun(@times,cosix,reshape(cosiy,Nt,1,ncmp));
                    case 'PCA'
                        C = obj.sigma2.*exp(-pdist2(centroid',centroid').^2/(2*obj.l^2));
                        % number of components in PCA
                        K=min([obj.parD,Nt]);
                        [evc,ev] = eigs(C,K,'LA'); % there are some approximations behind it, I guess, check C*evc./evc! but much faster
                        obj.eigv = sqrt(diag(ev)); obj.eigf = evc; % ok under fixed random seed
                    otherwise
                        error('Wrong option!');
                end
            end
        end
    end
end