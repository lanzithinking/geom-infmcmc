function[samp,loglik,acpt,time]=sampling(data,algorithm,setting,init,PRINT)

% pass the data
obj=data.obj; PDE4inf=data.PDE4inf;
geom_opt=data.geom_opt;
ind_trct=data.ind_trct;trct_opt=data.trct_opt;
ntrct=sum(ind_trct(:));

% MCMC settings
stepsz=setting.stepsz; Nleap=setting.Nleap;

% initialization
theta=init.theta; nll=init.nll; g=init.g;
FI=init.FI; cholG=init.cholG;
sampler=str2func(algorithm);

D=length(theta);
% storage of posterior samples
Nsamp=setting.Nsamp; NBurnIn=setting.NBurnIn;
samp=zeros(Nsamp-NBurnIn,D);
loglik = zeros(Nsamp,1);
acpi=0; acpt=0;

% start MCMC run
disp(' ');
disp(['Running ',algorithm,' sampling...']);
for iter=1:Nsamp
    
    if PRINT&&(mod(iter,100)==0)
        disp([num2str(iter) ' iterations completed.']);
        disp(['Online acceptance rate is ',num2str(acpi/100)]);
        acpi=0;
    end
    
    % sample the whole parameter
    try
        switch(algorithm)
            case 'pCN'
                [theta,nll,acpt_ind] = sampler(theta,nll,speye(D),@(theta)GEOM4inf(theta,PDE4inf,ind_trct,obj),stepsz);
            case 'infMALA'
                [theta,nll,g,acpt_ind] = sampler(theta,nll,g,speye(D),@(theta)GEOM4inf(theta,PDE4inf,ind_trct,obj,geom_opt),stepsz);
            case 'infHMC'
                [theta,nll,g,acpt_ind] = sampler(theta,nll,g,speye(D),@(theta)GEOM4inf(theta,PDE4inf,ind_trct,obj,geom_opt),stepsz,Nleap);
            case 'infmMALA'
                [theta,nll,g,FI,cholG,acpt_ind] = sampler(theta,nll,g,FI,cholG,@(theta)GEOM4inf(theta,PDE4inf,ind_trct,obj,geom_opt),stepsz);
            case 'infmHMC'
                [theta,nll,g,FI,cholG,acpt_ind] = sampler(theta,nll,g,FI,cholG,speye(D),@(theta)GEOM4inf(theta,PDE4inf,ind_trct,obj,geom_opt),stepsz,Nleap);
            case 'splitinfmMALA'
                [theta,nll,g,FI,cholG,acpt_ind] = sampler(theta,ind_trct,nll,g,FI,cholG,speye(D-ntrct),@(theta,ind_trct)GEOM4inf(theta,PDE4inf,ind_trct,obj,geom_opt,trct_opt),stepsz);
            case 'splitinfmHMC'
                [theta,nll,g,FI,cholG,acpt_ind] = sampler(theta,ind_trct,nll,g,FI,cholG,speye(D),@(theta,ind_trct)GEOM4inf(theta,PDE4inf,ind_trct,obj,geom_opt,trct_opt),stepsz,Nleap);
            otherwise
                error('Not a listed sampling algorithm!');
        end
    catch
        acpt_ind = 0;
        disp('Crashed and Rejected!');
    end
    acpi=acpi+acpt_ind;
    
    % Start timer after burn-in
    if iter==NBurnIn
        disp('Burn-in complete, now drawing samples.'); tic;
    end
    
    loglik(iter)=-nll;
    % save posterior samples after burnin
    if iter>NBurnIn
        samp(iter-NBurnIn,:)=theta'; acpt=acpt+acpt_ind;
    end
end
time=toc;

fprintf('\n Time consumed: %.2f\n',time);
acpt=acpt/(Nsamp-NBurnIn);
fprintf('\n Final acceptance rate: %.2f\n',acpt);

end