function logc=unknown(region,s,theta)

% truncate at ncmp components
D = length(theta); ncmp = sqrt(D);

% eigen-pairs of covariance kernel
eigv = pi^(-s).*bsxfun(@(x,y)(x.^2+y.^2).^(-s/2),(1:ncmp)',1:ncmp);
cosix = cos(pi.*region.x'*(1:ncmp)); cosiy = cos(pi.*region.y'*(1:ncmp));
eigf = bsxfun(@times,cosix,reshape(cosiy,[],1,ncmp));

plogc = bsxfun(@times,eigv(:)',eigf(:,:)); % (N_region,ncmp^2)
logc = (plogc*theta)';

end