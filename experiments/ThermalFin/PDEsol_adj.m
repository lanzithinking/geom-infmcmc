% This is to solve a classic example of elliptic equation given mesh and
% boundary conditions and get the derivatives of objective wrt some design
% parameter theta using ADJOINT method.
% solution: u; satisfying
% -d(cdu)=0; in Omega
% -cd_n u=Bi*u; on dOmega\Gamma
% -cd_n u=-1; on Gamma
% objective: obj(u)
% output: [u, obj, p_theta obj, p_ij obj * v];

function [u,obju,pobj,p2objv,p2obj]=PDEsol_adj(theta,PDE,obj,opt,PLOT,PRINT)
if nargin<3
    obj=[]; opt=0; PLOT=0; PRINT=0;
elseif nargin<4
    opt=0; PLOT=0; PRINT=0;
elseif nargin<5
    PLOT=0; PRINT=0;
elseif nargin<6
    PRINT=0;
end
u=[]; obju=[]; pobj=[]; p2objv=[]; p2obj=[];
% prepare to solve PDE
p=PDE.p;e=PDE.e;t=PDE.t; % mesh
b=PDE.b;% b_adj=PDE.db; % problems (with boudary conditions)
h=PDE.h;

% dimensions
D=length(theta);

% coefficients
if all(opt==0)
    c=coeff(p,t,[],[],theta,PDE.eigv,PDE.eigf);
elseif all(ismember(opt,[0,1,2]))
    [c,pc]=coeff(p,t,[],[],theta,PDE.eigv,PDE.eigf);
end

if any(ismember([0,1,2],opt))
    % forward solution of u
    [Ks,Fs] = assempde(b,p,e,t,c',0,0);
    u = Ks\Fs;
    % construct interpolator
    if nargout>1
        obju = obj.eval(u);
    end
    % plot the forward solution
    if PLOT
        fig1=figure(1); clf;
        pdesurf(p,t,u);
        title('Forward Solution','fontsize',18);
    end
    % print the forward solution
    if PRINT
        print(fig1,'-dpng','u_fwd');
    end
end
if any(opt==1)
    % solve adjoint equation for pobj
    [~,Fa]=f_adj(p,t,u,[],obj); % calculate rhs at nodes
    lambda = Ks\(Fa.*h^2);
    % plot the adjoint solution
    if PLOT
        fig2=figure(2); clf;
        pdesurf(p,t,lambda);
        title('Adjoint Solution','fontsize',18);
    end
    % print the adjoint solution
    if PRINT
        print(fig2,'-dpng','u_adj');
    end
end
if any(ismember([1,2],opt))
    pobj=zeros(D,1);
    if any(opt==2)
        dK = cell(D,1);
    end
    for i=1:D
        % obtain stiffness matrix with pc_i
        K_i = assema(p,t,pc(:,i)',0,0);
        if any(opt==2)
            dK{i} = K_i;
        end
        % solution of p_i obj
        if any(opt==1)
            pobj(i) = -(lambda'*K_i*u)/h^2;
        end
    end
end
if any(opt==2)
    % metric-vector as a function
    p2objv=@(v,PLOT,PRINT)Fv(v,PLOT,PRINT);
    % solution of p_i obj * p_j obj
    if nargout==5
        p2obj = zeros(D);
        e_d=zeros(D,1);
        for d=1:D
            e_d(d)=1;
            p2obj(:,d)=p2objv(e_d,PLOT*(d==1),PRINT*(d==1));
            e_d(d)=0;
        end
    end
end

function p2objv=Fv(v,PLOT,PRINT)
    if nargin<2
        PLOT=0; PRINT=0;
    elseif nargin<3
        PRINT=0;
    end
    % solve 2nd order forward equation
    K2f = assema(p,t,(pc*v)',0,0);
    F2=K2f*u;
    deltay = Ks\F2;
    % plot the 2nd order forward solution
    if PLOT
        fig3=figure(3); clf;
        pdesurf(p,t,deltay);
        title('2nd order Forward Solution','fontsize',18);
    end
    % print the 2nd order forward solution
    if PRINT
        print(fig3,'-dpng','u2_fwd');
    end
    
    % solve 2nd order adjoint equation
    [~,F2a]=f2_adj(p,t,deltay,[],obj); % calculate rhs at nodes
    deltalambda = Ks\(F2a.*h^2);
    % plot the adjoint solution
    if PLOT
        fig4=figure(4); clf;
        pdesurf(p,t,deltalambda);
        title('2nd order Adjoint Solution','fontsize',18);
    end
    % print the adjoint solution
    if PRINT
        print(fig4,'-dpng','u2_adj');
    end
    
    % calculate metric-vector
    p2objv=zeros(D,1);
    for j=1:D
        % solution of p_ij objv
        p2objv(j) = (deltalambda'*dK{j}*u)/h^2;
    end
end

end