%%%% right hand side f in 2nd order adjoint equation (?) %%%%

function [ft,fn]=f2_adj(p,t,u,time,obj,tol)

if nargin<6
    tol=1e-7;
end
ft=[];fn=[];

if ~isempty(obj.O)
    O=obj.O(:);
else
    O=sparse(pdist2(obj.x,p','chebychev','smallest',1)<tol)';
end

fn=u.*O./obj.sigma2y; % f at nodes
if nargout==2
    return;
end
ft=pdeintrp(p,t,fn); % f at triangle midpoints

end