%%%% This is the main script to sample from posterior of parameters in Elliptic PDE to compare algorithms %%%%
clear;
addpath('../sampler/');
% Random Numbers
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

% MCMC algorithms
MCMC = {'pCN','infMALA','infHMC','infmMALA','infmHMC','splitinfmMALA','splitinfmHMC'};
L_MCMC = length(MCMC);

%% settings

% mesh setting
h=0.2; Bi=.1;
ncmp = 10; D = ncmp^2;

% basis setting
basis_types={'Fourier','PCA'};
basis_type=basis_types{1};
% scale parameter for prior
sigma=1;
% Fouerier basis parameters
alpha=0; s=1.2;
% PCA parameters
l=.1; ex=1;


%% load data
try
    load(['ThermalFin_h',num2str(h),'_D',num2str(D),'_',basis_type,'_priorsd',num2str(sigma),'.mat']);
catch
    % generate data
    % prepare PDE setup for inference
    switch basis_type
        case basis_types{1}
            PDE4inf=PDEsetup(h,Bi,D,basis_type,sigma,alpha,s);
        case basis_types{2}
            PDE4inf=PDEsetup(h,Bi,D,basis_type,sigma,l,ex);
        otherwise
            error('wrong choice!');
    end
    % observations on dOmega\Gamma
    e_idx=unique(PDE4inf.e(1:2,:));
    x=PDE4inf.p(:,e_idx)';
    x((x(:,1)>-.5&x(:,1)<.5)&(x(:,2)==0),:)=[];
    % show positions on the geometry plot
    fig1=figure(1); clf;
    set(fig1,'pos',[0 800 600 400]);
    pdeplot(PDE4inf.p,PDE4inf.e,PDE4inf.t);hold on;plot(x(:,1),x(:,2),'ko','markersize',7);hold off;
    % solve PDE on a finer mesh for observations
    PDE4obs=PDErefine(PDE4inf);
    % u=PDEsol4obs(PDE4obs);
%     theta_truth = randn(D,1);
    theta_truth = sin(bsxfun(@(x,y)x.^2+y.^2,(0.5:ncmp-.5)',0.5:ncmp-.5));
    u=PDEsol_adj(theta_truth(:),PDE4obs);
    % observation operator
    O=sparse(pdist2(x,PDE4obs.p','chebychev','smallest',1)<1e-7)';
    u=u(O);
    % model parameters
    sigma_y=.01*max(u); sigma2y=sigma_y^2;
    y=u+sigma_y.*randn(length(u),1);
    % update observation operator for inferece
    O=sparse(pdist2(x,PDE4inf.p','chebychev','smallest',1)<1e-7)';
    % set up objective
    obj=misfit(x,y,sigma2y,O);
    % save data
    save(['ThermalFin_h',num2str(h),'_D',num2str(D),'_',basis_type,'_priorsd',num2str(sigma),'.mat'],'seed','h','basis_type','theta_truth','PDE4inf','PDE4obs','obj');
end

% count times of solving PDE
global pde_sol_cnt

% choose algorithm
for alg_id=1:L_MCMC

% initialize the count of PDE solving
pde_sol_cnt=zeros(4,1);

% options of geometric quantities required
geom_opt=[0,1*(alg_id>1),2*(alg_id>3)];

% options of truncation
ind_trct=[];trct_opt=zeros(1,3);
if strfind(MCMC{alg_id},'split')
    % truncation number
    rtrct=5; ntrct=rtrct^2;
    % index of truncation: indicator of being kept
    ind_trct=logical(sparse(ncmp,ncmp)); ind_trct(1:rtrct,1:rtrct)=1;
    % truncation option
    trct_opt=[0,0,1];
end

% save into data
data.obj=obj; data.PDE4inf=PDE4inf;
data.geom_opt=geom_opt;
data.ind_trct=ind_trct;data.trct_opt=trct_opt;

%% setting of sampling
stepsz=[.002,.005,.005,1.2,1.2,1,1]; Nleap=ones(1,L_MCMC);
if strfind(MCMC{alg_id},'HMC')
    Nleap(alg_id)=4;
end
setting.stepsz=stepsz(alg_id); setting.Nleap=Nleap(alg_id);
Nsamp = 11000; NBurnIn = 1000;
setting.Nsamp=Nsamp; setting.NBurnIn=NBurnIn;

%% initialization
% theta = randn(D,1); %theta = theta_truth(:);
theta = zeros(D,1);
[nll,g,FI,cholG] = GEOM4inf(theta,PDE4inf,ind_trct,obj,geom_opt,trct_opt);

init.theta=theta; init.nll=nll; init.g=g;
init.FI=FI; init.cholG=cholG;

%% setting of saving
SAVE=1; PRINT=1; PLOT=1;
savepath=[pwd,'/result/'];
filename=['ThermalFin_',char(MCMC(alg_id)),'_D',num2str(D),'_Nleap',num2str(Nleap(alg_id))];
if strfind(MCMC{alg_id},'split')
    filename=[filename,'_ntrct',num2str(ntrct)];
end

%% sampling
[samp,loglik,acpt,time]=sampling(data,char(MCMC(alg_id)),setting,init,PRINT);

%% save results
addpath('./result/');
curTime = regexprep(num2str(fix(clock)),'    ','_');
curfile=[filename,'__',curTime];
if SAVE
    stepsz=setting.stepsz; Nleap=setting.Nleap;
    save([savepath,curfile,'.mat'],'h','data','setting','samp','loglik','acpt','time','pde_sol_cnt');
end
if PRINT
    CalculateStatistics(curfile,savepath);
end
if PLOT
    % some plots
    idx=floor(linspace(1,size(samp,1),min([1e4,size(samp,1)])));
    dim=[1,2,floor(D/2),D];
    fig=figure(1); set(fig,'pos',[0 800 900 300]);
    subplot(1,3,1);
    plot(samp(idx,dim));
    subplot(1,3,2);
    plotmatrix(samp(idx,dim));
    subplot(1,3,3);
    plot(loglik);
    % save to file
%     print(fig,['./figure/',curfile],'-dpng');
end

end
