%%%% right hand side f in adjoint Elliptic PDE wrt misfit function on boundary %%%%

function f=f_onbdy(region,state,obj,tol)

if nargin<4
    tol=1e-7;
end
f=[];

p=[region.x;region.y]';
u=state.u';

[D,I]=pdist2(obj.x,p,'chebychev','smallest',1);

O=sparse(D<tol)';
if(any(O~=0))
    cat('ha!');
end

Y=sparse(size(O,1),size(O,2)); Y(O)=obj.y(I(O));

f=(-(Y-u).*O./obj.sigma2y)';


end