% This is to solve a classic example of elliptic equation given mesh and
% boundary conditions and get the derivatives of solution wrt some design
% parameter theta using sensitivity approach.
% solution: u; derivative: pu, p2u satisfying
% -d(cdu)=0;      % -d(cdpu)=d(pcdu);        % -d(cdp2u)=d(p2cdu)+d(pcdpu)+d(dpupc); in Omega
% -cd_n u=Bi*u;   % -cd_n pu=Bi*pu+pcd_n u;  % -cd_n p2u=Bi*p2u+p2cd_n u+pcd_n pu+d_n pupc; on dOmega\Gamma
% -cd_n u=-1;     % -cd_n pu=pcd_n u;        % -cd_n p2u=p2cd_n u+pcd_n pu+d_n pupc; on Gamma
% output: [u,p_theta u,p_ij u];

function [u,pu,p2u]=PDEsol_sen(theta,PDE,opt,PLOT,PRINT)
if nargin<3
    opt=0; PLOT=0; PRINT=0;
elseif nargin<4
    PLOT=0; PRINT=0;
elseif nargin<5
    PRINT=0;
end
u=[]; pu=[]; p2u=[];
% prepare to solve PDE
p=PDE.p;e=PDE.e;t=PDE.t; % mesh
b=PDE.b;% pb=PDE.db; % problems (with boudary conditions)

% dimensions
Np=size(p,2); D=length(theta);

% specify coefficients
if all(opt==0)
    c=coeff(p,t,[],[],theta,PDE.eigv,PDE.eigf);
elseif all(ismember(opt,[0,1]))
    [c,pc]=coeff(p,t,[],[],theta,PDE.eigv,PDE.eigf);
elseif all(ismember(opt,[0,1,2]))
    [c,pc,p2c]=coeff(p,t,[],[],theta,PDE.eigv,PDE.eigf);
end

if any(ismember([0,1,2],opt))
    % solution of u
%     [K,M,F,Q,G,H,R] = assempde(b,p,e,t,c',0,0);
%     u = assempde(K,M,F,Q,G,H,R);
%     [K,~,~,Q,G,~,~] = assempde(b,p,e,t,c',0,0);
%     u = (K+Q)\G;
    [Ks,Fs] = assempde(b,p,e,t,c',0,0);
    u = Ks\Fs;
    % plot the solution
    if PLOT
        fig1=figure(1); clf;
        pdesurf(p,t,u);
    end
    % print the solution
    if PRINT
        print(fig1,'-dpng','u');
    end
end
if any(ismember([1,2],opt))
    pu=zeros(Np,D);
    if PLOT
        fig2=figure(2); clf;
        Ncol=min([ceil(sqrt(D)),5]);
        Nrow=min([ceil(D/Ncol),5]);
    end
    K1=cell(1,D);
    for i=1:D
        % assembling matrices on RHS
        K1{i} = assema(p,t,pc(:,i)',0,0);
        % solution of p_iu
%         p_iu = (K+Q)\(-K1{i}*u);
        p_iu = Ks\(-K1{i}*u);
        pu(:,i) = p_iu;
        % plot the solutions
        if PLOT&&i<=Nrow*Ncol
            set(0,'CurrentFigure',fig2);
            subplot(Nrow,Ncol,i);
            pdesurf(p,t,p_iu);
        end
    end
   % print the solution
    if PRINT
        print(fig2,'-dpng','pu');
    end
end
if any(opt==2)
    p2u=zeros(Np,D,D);
    if PLOT
        fig3=figure(3); clf;
        plotdim=min([D,5]);
    end
    for i=1:D
        % solve p_iju
        for j=1:i
            % assembling matrices on RHS
            K_ij = assema(p,t,p2c(:,i,j)',0,0);
%             p_iju = (K+Q)\(-K_ij*u-K1{i}*pu(:,j)-K1{j}*pu(:,i));
            p_iju = Ks\(-K_ij*u-K1{i}*pu(:,j)-K1{j}*pu(:,i));
            p2u(:,i,j) = p_iju; p2u(:,j,i) = p_iju;
            if PLOT&&i<=plotdim
                set(0,'CurrentFigure',fig3);
                subplot(plotdim,plotdim,sub2ind([plotdim,plotdim],j,i));
                pdesurf(p,t,p_iju);
            end
        end
    end
    % print the solution
    if PRINT
        print(fig3,'-dpng','p2u');
    end
end

end