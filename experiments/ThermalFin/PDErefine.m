% This is to update setting to solve a classic example of elliptic equation
% and its derivatives wrt some design parameter theta.
% solution: u; satisfying
% -d(cdu)=0; in Omega
% -cd_n u=Bi*u; on dOmega\Gamma
% -cd_n u=-1; on Gamma

function PDE=PDErefine(PDE0,T)
if nargin<2
    T=1;
end

% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed); % force the same random seed

% solution domain
g=PDE0.g;
% figure;
% pdegplot(g, 'edgeLabels', 'on');
% title 'Geometry With Edge Labels Displayed';

% refine mesh
p=PDE0.p;e=PDE0.e;t=PDE0.t;
for i=1:T
    [p,e,t] = refinemesh(g,p,e,t);
end
% figure;
% pdeplot(p,e,t);
% axis equal
% title 'Plate With Mesh'
% xlabel 'X-coordinate'
% ylabel 'Y-coordinate'

% Create a pde entity for a PDE of u
NPDE = 1;
b = createpde(NPDE);
% Create a geometry entity
geometryFromEdges(b,g);
% specify constant boundary conditions
applyBoundaryCondition(b,'Edge',1,'g',1);
applyBoundaryCondition(b,'Edge',2:size(g,2),'q',PDE0.Bi);

% PDE definition
PDE.g=g;
PDE.p=p;PDE.e=e;PDE.t=t;
PDE.b=b;%PDE.db=db;
% PDE.b_blk=b_blk;
PDE.h=PDE0.h*2^(-T);
PDE.Bi=PDE0.Bi;


if isfield(PDE0,'parD')
    % prepare for the PDE coefficients
    Nt = size(t,2); D=PDE0.parD; PDE.parD=D;
    % Triangle point indices
    it1 = t(1,:);
    it2 = t(2,:);
    it3 = t(3,:);
    % Find centroids of triangles
    centroid = (p(:,it1)+p(:,it2)+p(:,it3))/3;

    basis_type=PDE0.basis_type; sigma=PDE0.sigma;
    PDE.basis_type=basis_type; PDE.sigma=sigma;
    % eigen-pairs of covariance kernel
    switch basis_type
        case 'Fourier'
            % 2d fourier basis
            ncmp = sqrt(D); alpha=PDE0.alpha;s=PDE0.s; PDE.alpha=alpha;PDE.s=s;
            eigv = sigma.*(alpha+pi^2.*bsxfun(@(x,y)(x.^2+y.^2),(0.5:ncmp-.5)',0.5:ncmp-.5)).^(-s/2);
            cosix = cos(pi.*centroid(1,:)'*(0.5:ncmp-.5)); cosiy = cos(pi.*centroid(2,:)'*(0.5:ncmp-.5));
            eigf = bsxfun(@times,cosix,reshape(cosiy,Nt,1,ncmp));
        case 'PCA'
            l=PDE0.l;ex=PDE0.ex; PDE.l=l;PDE.ex=ex;
            C = exp(-pdist2(centroid',centroid').^ex/(2*l^ex));
            % number of components in PCA
            K=min([D,Nt]);
            [evc,ev] = eigs(C,K,'LA'); % there are some approximations behind it, I guess, check C*evc./evc! but much faster
            eigv = sigma.*sqrt(diag(ev)); eigf = evc; % ok under fixed random seed
        otherwise
            error('Wrong option!');
    end

    % PDE coefficient decomposition
    PDE.eigv=eigv; PDE.eigf=eigf;
end

end
