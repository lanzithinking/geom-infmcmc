% This is to solve the thermal conductivitiy equation (elliptic) with true
% thermal conductivity field to obtain observations by adding noise to
% the solutions.

% solution: u satisfying
% -d(cdu)=0;    in Omega
% -cd_n u=Bi*u;   on dOmega\Gamma
% -cd_n u=-1;   on Gamma
% output: u

function u=PDEsol4obs(PDE,PLOT,PRINT)
if nargin<2
    PLOT=0; PRINT=0;
elseif nargin<3
    PRINT=0;
end
u=[];
% prepare to solve PDE
p=PDE.p;e=PDE.e;t=PDE.t; % mesh
b=PDE.b;% pb=PDE.db; % problems (with boudary conditions)

% solution
% c=coeff(p,t,[],[],1.5,randn(4^2,1));
[~,c] = condct(p,t,[],[]);
u = assempde(b,p,e,t,c',0,0);
% [K,M,F,Q,G,H,R] = assempde(b,p,e,t,c',0,0);
% u = (K+Q)\G;
% plot the solution
if PLOT
    fig1=figure(1); clf;
    pdesurf(p,t,u);
end
% print the solution
if PRINT
    print(fig1,'-dpng','u');
end

end