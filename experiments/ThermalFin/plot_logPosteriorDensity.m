clear;
addpath('../');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

% mesh setting
h=0.2; Bi=.1;
ncmp = 10; D = ncmp^2;

% basis setting
basis_types={'Fourier','PCA'};
basis_type=basis_types{1};
% scale parameter for prior
sigma=1e-1;
% Fouerier basis parameters
alpha=0; s=1.2;
% PCA parameters
l=.1; ex=2;

% prepare PDE setup for inference
switch basis_type
    case basis_types{1}
        PDE4inf=PDEsetup(h,Bi,D,basis_type,sigma,alpha,s);
    case basis_types{2}
        PDE4inf=PDEsetup(h,Bi,D,basis_type,sigma,l,ex);
    otherwise
        error('wrong choice!');
end
% observations on dOmega\Gamma
e_idx=unique(PDE4inf.e(1:2,:));
x=PDE4inf.p(:,e_idx)';
x((x(:,1)>-.5&x(:,1)<.5)&(x(:,2)==0),:)=[];
% show positions on the geometry plot
fig1=figure(1); clf;
set(fig1,'pos',[0 800 600 400]);
pdeplot(PDE4inf.p,PDE4inf.e,PDE4inf.t);hold on;plot(x(:,1),x(:,2),'ko','markersize',7);hold off;
% solve PDE on a finer mesh for observations
PDE4obs=PDErefine(PDE4inf);
% u=PDEsol4obs(PDE4obs);
% theta_truth = randn(D,1);
theta_truth = sin(bsxfun(@(x,y)x.^2+y.^2,(1:ncmp)',1:ncmp));
u=PDEsol_adj(theta_truth(:),PDE4obs);
% observation operator
O=sparse(pdist2(x,PDE4obs.p','chebychev','smallest',1)<1e-7)';
u=u(O);
% model parameters
sigmay=.01*max(u); sigma2y=sigmay^2;
y=u+sigmay.*randn(length(u),1);
% update observation operator for inferece
O=sparse(pdist2(x,PDE4inf.p','chebychev','smallest',1)<1e-7)';
% set up objective
obj=misfit(x,y,sigma2y,O);


% plot pairwise posterior density
dim=[1,2];
fig2=figure(2); clf;
set(fig2,'pos',[0 150 800 350]);
subplot(1,2,1);
ezsurf(@(x,y)logPosterior(bsxfun(@plus,x*(dim(1)==(1:D))+y*(dim(2)==(1:D)),theta_truth(:)'.*(dim(1)~=(1:D)&dim(2)~=(1:D))),PDE4inf,obj),[-10,10,-10,10]);
xlabel(['d_{',num2str(dim(1)),'}']); ylabel(['d_{',num2str(dim(2)),'}']);
title('log Posterior Surface');
subplot(1,2,2);
ezcontour(@(x,y)logPosterior(bsxfun(@plus,x*(dim(1)==(1:D))+y*(dim(2)==(1:D)),theta_truth(:)'.*(dim(1)~=(1:D)&dim(2)~=(1:D))),PDE4inf,obj),[-10,10,-10,10]);
xlabel(['d_{',num2str(dim(1)),'}']); ylabel(['d_{',num2str(dim(2)),'}']);
title('log Posterior Contour');
% save to file
% print(fig2,['logPosteriorDensity_',obs_type],'-dpng');
