%%% This is to plot correlation of posterior in groundwater problem. %%%

clear;
addpath('~/Documents/MATLAB/tight_subplot/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

% algorithms
alg={'pCN','infMALA','HHMC','infMMALA','splitinfMMALA','splitinfmHMC'};
alg_name={'pCN','$$\infty$$-MALA','H-HMC','$$\infty$$-MMALA','split$$\infty$$-MMALA','split$$\infty$$-mHMC'};
Nalg=length(alg);

 % set plot
fig1=figure(1); clf;
% set(fig1,'windowstyle','docked');
set(fig1,'pos',[0 800 900 450]);
% fig=figure('visible','off');
ha=tight_subplot(2,ceil(Nalg/2),[.15 0.085],[.1,.06],[.07,.05]);

% mesh setting
h=0.2;

% % true conductivity field
% % load data
% load(['ThermalFin_h',num2str(h),'.mat']);
% % mesh
% p=PDE4inf.p;e=PDE4inf.e;t=PDE4inf.t;
% % conductivity
% c=condct(p,t,[],[]);
% % plot
% % subplot(2,ceil(Nalg/2),1);
% subplot(ha(1));
% pdeplot(p,e,t,'xydata',c);%,'contour','on');
% title('True Conductivity','fontsize',18);
% xlim([-3.1,3.1]);ylim([-.1,4.1]);
% xlabel('x_1','fontsize',15);ylabel('x_2','fontsize',15,'rot',0);
% ylabh=get(gca,'YLabel');set(ylabh,'Position',get(ylabh,'Position') - [0.1 0 0]);
% set(gca,'FontSize',15); box on;

% estimate permeability
s=1.2; ncmp = 10; D = ncmp^2;
% load data
files = dir('./summary');
nfiles = length(files) - 2;

for i=1:Nalg
    for j=1:nfiles
        if ~isempty(strfind(files(j+2).name,['_',alg{i},'_D',num2str(D),'_']))
                    load(strcat('./summary/', files(j+2).name));
        end
    end
    % estimate correlation of posterior samples
    cor_theta=corr(samp);
    % plot
    subplot(ha(i));
    imagesc(cor_theta);
    title(['Post-Corr by ',alg_name{i}],'fontsize',18,'interpreter','latex');
%     xlabel('x_1','fontsize',15);ylabel('x_2','fontsize',15,'rot',0);
%     ylabh=get(gca,'YLabel');set(ylabh,'Position',get(ylabh,'Position') - [10 0 0]);
    set(gca,'Ydir','normal');
    set(gca,'FontSize',15);
    colorbar;
end

% save to file
% print(fig,'-dpng','./figure/posteriorcorrelation');