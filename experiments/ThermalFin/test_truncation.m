%%%% test PDE solvers %%%%
clear;

% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

%%%% generate data %%%%
% mesh settings
h=.2;
% setting for solving PDE
s=1.5; ncmp = 15; D = ncmp^2;
Bi=.1;
% solve PDE
PDE=PDEsetup(h,Bi,D,'PCA');
u=PDEsol4obs(PDE);
% model parameters
sigma2y=.1^2;
% observations on dOmega\Gamma
e_idx=unique(PDE.e(1:2,:));
x=PDE.p(:,e_idx)';
x((x(:,1)>-.5&x(:,1)<.5)&(x(:,2)==0),:)=[];
% show on the geometry plot
% pdeplot(PDE.p,PDE.e,PDE.t);hold on;plot(x(:,1),x(:,2),'ko','markersize',7);hold off;
% observation operator
O=sparse(pdist2(x,PDE.p','chebychev','smallest',1)<1e-7)';
u=u(O);
y=u+sqrt(sigma2y).*randn(length(u),1);
% set up objective
obj=misfit(x,y,sigma2y,O);


% fix parameter
theta = randn(D,1);

%%%% Gradient dJ/dtheta and Fisher %%%%

% full version %
[u,nll,dnll,~,FI]=PDEsol_adj(theta,PDE,obj,[0,1,2]);


% truncation number
rtrct=5; ntrct=rtrct^2;
% index of truncation: indicator of being kept
% ind_trct=logical(sparse(ncmp,ncmp)); ind_trct(1:rtrct,1:rtrct)=1;
ind=sparse(ncmp,1); ind(randsample(ncmp,rtrct))=1;
ind_trct=logical(ind*ind');
% truncation option
trct_opt=[0,0,1];

% truncated version %
[u_t,nll_t,dnll_t,~,FI_t]=PDEsol_adj_trct(theta,PDE,ind_trct,obj,[0,1,2],trct_opt);

%%% Comparison %%%

% compare
fprintf('\nInf-norm difference in u: %.8e \n',max(abs(u-u_t)));
fprintf('\nDifference in nll: %.8e \n',abs(nll-nll_t));
if trct_opt(2)
    g_diff = max(abs(dnll(ind_trct)-dnll_t));
else
    g_diff = max(abs(dnll-dnll_t));
end
fprintf('\nInf-norm difference of gradients: %.8e \n',g_diff);
if trct_opt(3)
    F_diff = norm(FI(ind_trct,ind_trct)-FI_t)/h;
else
    F_diff = norm(FI-FI_t)/meshsz;
end
fprintf('\nScaled 2-norm difference of Fisher: %.8e \n',F_diff);
