%%%% True conductivity field of Thermal Fin model %%%%

function [condct_n,condct_t]=condct(p,t,u,time,PLOT)
if nargin<5
    PLOT=0;
end
condct_n=[]; condct_t=[];

switch nargout
    case 1
        condct_n=exp(mvnpdf(p',[0,2],.3^2.*eye(2)));
        if PLOT
            pdeplot(p,[],t,'xydata',condct_n);
        end
    case 2
        % Triangle point indices
        it1 = t(1,:);
        it2 = t(2,:);
        it3 = t(3,:);
        % Find centroids of triangles
        centroid = (p(:,it1)+p(:,it2)+p(:,it3))/3;
        condct_t=exp(mvnpdf(centroid',[0,2],.3^2.*eye(2)));
        if PLOT
            pdeplot(p,[],t,'xydata',condct_t');
        end
    otherwise
        error('Wrong number of outputs!');
end

end
