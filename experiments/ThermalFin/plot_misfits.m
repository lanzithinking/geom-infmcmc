%%% This is to plot trace of misfits in groundwater problem. %%%

clear;
% addpath('~/Documents/MATLAB/tight_subplot/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

% algorithms
alg={'pCN','infMALA','HHMC','infMMALA','splitinfMMALA','splitinfmHMC'};
alg_name={'pCN','$$\infty$$-MALA','H-HMC','$$\infty$$-MMALA','split$$\infty$$-MMALA','split$$\infty$$-mHMC'};
Nalg=length(alg);

D=100;

% plot specifications
colors = distinguishable_colors(Nalg);
styles = {'-','-.','--','-','--',':','--'};

 % set plot
fig1=figure(1); clf;
% set(fig1,'windowstyle','docked');
% set(fig1,'pos',[0 800 900 450]);
% fig=figure('visible','off');
% ha=tight_subplot(2,ceil(Nalg/2),[.15 0.085],[.1,.06],[.07,.05]);


% gather misfits by different algorithms
% load data
files = dir('./summary');
nfiles = length(files) - 2;

for i=1:Nalg
    for j=1:nfiles
        if ~isempty(strfind(files(j+2).name,['_',alg{i},'_D',num2str(D),'_']))
                    load(strcat('./summary/', files(j+2).name));
        end
    end
    % create misfit
    if ~exist('misfit','var')
        misfit=zeros(length(loglik),Nalg);
    end
    misfit(:,i)=-loglik;
end

% semilogy(misfit(1:200,:));
for i=1:Nalg
    semilogy(1:400,misfit(1:400,i), styles{i},'color',colors(i,:),'linewidth',2); hold on;
end
drawnow;
set(gca,'FontSize',15);
xlim([1,400]);ylim([300,600]);
xlabel('Iterations','FontSize',19); ylabel('Data  Misfit','FontSize',19); 

legend(alg_name,'FontSize',14,'interpreter','latex','box','off');

% save to file
% print(fig,'-dpng','./figure/convergence');