%%%% test PDE solvers %%%%
clear;

addpath('../');
PLOT=0; PRINT=0;

% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

%%%% generate data %%%%
% mesh settings
h=.2;
% setting for solving PDE
s=1.5; ncmp = 15; D = ncmp^2;
Bi=.1;
% solve PDE
PDE=PDEsetup(h,Bi,D,'PCA');
u=PDEsol4obs(PDE);
% model parameters
sigma2y=.1^2;
% observations on dOmega\Gamma
e_idx=unique(PDE.e(1:2,:));
x=PDE.p(:,e_idx)';
x((x(:,1)>-.5&x(:,1)<.5)&(x(:,2)==0),:)=[];
% show on the geometry plot
% pdeplot(PDE.p,PDE.e,PDE.t);hold on;plot(x(:,1),x(:,2),'ko','markersize',7);hold off;
% observation operator
O=sparse(pdist2(x,PDE.p','chebychev','smallest',1)<1e-7)';
u=u(O);
y=u+sqrt(sigma2y).*randn(length(u),1);
% set up objective
obj=misfit(x,y,sigma2y,O);


% fix parameter
theta = randn(D,1);

%%%% Gradient dJ/dtheta and Fisher %%%%

% by finite difference %
pobj_fd=zeros(D,1);
eps=1e-3;
for i=1:D
    theta_p=theta;theta_m=theta;
    theta_p(i)=theta_p(i)+eps;theta_m(i)=theta_m(i)-eps;
    [~,obju_p]=PDEsol_adj(theta_p,PDE,obj);
    [~,obju_m]=PDEsol_adj(theta_m,PDE,obj);
    pobj_fd(i)=(obju_p-obju_m)/(2*eps);
end

tic;
% by sensitivity approach %
[u,pu,p2u]=PDEsol_sen(theta,PDE,[0,1],PLOT,PRINT);
[~,f]=f_adj([],[],u,[],obj);
pobj_sen=pu'*f;
Opu=bsxfun(@times,O(:),pu);
F_sen=Opu'*Opu./sigma2y;
time_sen=toc;

tic;
% by adjoint method %
[u,obju,pobj_adj,~,F_adj]=PDEsol_adj(theta,PDE,obj,[0,1,2],PLOT,PRINT);
time_adj=toc;

%%% Comparison %%%

% compare
fprintf('\nInf-norm difference of gradients between sensitivity and FD: %.8e \n',max(abs(pobj_sen-pobj_fd)));
fprintf('Inf-norm difference of gradients between adjoint and FD: %.8e \n\n',max(abs(pobj_adj-pobj_fd)));

fprintf('Scaled 2-norm difference of gradients between sensitivity and FD: %.8e \n',norm(pobj_sen-pobj_fd)*h);
fprintf('Scaled 2-norm difference of gradients between adjoint and FD: %.8e \n\n',norm(pobj_adj-pobj_fd)*h);


fprintf('\nInf-norm difference of Fisher between sensitivity and adjoint: %.8e \n',max(abs(F_sen(:)-F_adj(:))));

fprintf('Scaled 2-norm difference of Fisher between sensitivity and adjoint: %.8e \n\n',norm(F_sen-F_adj)*h);

fprintf('\nTime is %.4f seconds for sensitivity and %.4f seconds for adjoint, %.2f faster \n\n',time_sen,time_adj,time_sen/time_adj);

% plot
if PLOT
    fig10=figure(10); set(fig10,'pos',[0 800 1000 400]); clf;
    % gradient by FD
    subplot(1,3,1,'position',[.06,.12,.26,.8]);
    imagesc(reshape(pobj_fd,ncmp,ncmp));
    title('Finte Difference','FontSize',20);
    xlabel('d_1','fontsize',19);ylabel('d_2','fontsize',19,'rot',0);
    set(gca,'Ydir','normal');
    set(gca,'FontSize',15);
    % gradient by sensitivity
    subplot(1,3,2,'position',[.38,.12,.26,.8]);
    imagesc(reshape(pobj_sen,ncmp,ncmp));
    title('Sensitivity Approach','FontSize',20);
    xlabel('d_1','fontsize',19);ylabel('d_2','fontsize',19,'rot',0);
    set(gca,'Ydir','normal');
    set(gca,'FontSize',15);
    % gradient by adjoint
    subplot(1,3,3,'position',[.70,.12,.26,.8]);
    imagesc(reshape(pobj_adj,ncmp,ncmp));
    title('Adjoint Solver','FontSize',20);
    xlabel('d_1','fontsize',19);ylabel('d_2','fontsize',19,'rot',0);
    set(gca,'Ydir','normal');
    set(gca,'FontSize',15);
end