% This is to set up to jointly solve a classic example of elliptic equation
% and its derivatives wrt some design parameter theta.
% solution: u; satisfying
% -d(cdu)=0; in Omega
% -cd_n u=Bi*u; on dOmega\Gamma
% -cd_n u=-1; on Gamma

function PDE=PDEsetup(hmax,Bi,D,basis_type,sigma,varargin)
narginchk(0,7);
if nargin<1
    hmax=.2; Bi=.1; D=0; basis_type='Fourier'; sigma=1;
elseif nargin<2
    Bi=.1; D=0; basis_type='Fourier'; sigma=1;
elseif nargin<3
    D=0; basis_type='Fourier'; sigma=1;
elseif nargin<4
    basis_type='Fourier'; sigma=1;
elseif nargin<5
    sigma=1;
end

% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed); % force the same random seed

% define solution domain
load('ThermalFinDOMAIN.mat','g');
% figure;
% pdegplot(g, 'edgeLabels', 'on');
% title 'Geometry With Edge Labels Displayed';

% creat triangular mesh
[p,e,t] = initmesh(g, 'Hmax', hmax);
% figure;
% pdeplot(p,e,t);
% axis equal
% title 'Plate With Mesh'
% xlabel 'X-coordinate'
% ylabel 'Y-coordinate'

% Create a pde entity for a PDE of u
NPDE = 1;
b = createpde(NPDE);
% Create a geometry entity
geometryFromEdges(b,g);
% specify constant boundary conditions
applyBoundaryCondition(b,'Edge',1,'g',1);
applyBoundaryCondition(b,'Edge',2:size(g,2),'q',Bi);


% % Create a pde entity for the adjoint PDE of lambda
% db = createpde(NPDE);
% % Create a geometry entity
% geometryFromEdges(db,g);
% % specify boundary conditions
% applyBoundaryCondition(db,'Edge',2:2:size(g,2),'q',Bi);

% % Create a pde entity for the stiffness matrices in bulk
% b_blk = pde(ncmp^2);

% PDE definition
PDE.g=g;
PDE.p=p;PDE.e=e;PDE.t=t;
PDE.b=b;%PDE.db=db;
% PDE.b_blk=b_blk;
PDE.h=hmax;
PDE.Bi=Bi;

if D~=0
    % prepare for the PDE coefficients
    PDE.parD=D; % dimension of parameters
    Nt = size(t,2);
    % Triangle point indices
    it1 = t(1,:);
    it2 = t(2,:);
    it3 = t(3,:);
    % Find centroids of triangles
    centroid = (p(:,it1)+p(:,it2)+p(:,it3))/3;

    PDE.basis_type=basis_type; PDE.sigma=sigma;
    % eigen-pairs of covariance kernel
    switch basis_type
        case 'Fourier'
            % 2d fourier basis
            if isempty(varargin)
                varargin{1}=1; % scale parameter
                varargin{2}=1.1; % smoothness parameter
            elseif length(varargin)==1
                varargin{2}=1.1;
            end
            ncmp = sqrt(D); alpha=varargin{1};s=varargin{2}; PDE.alpha=alpha;PDE.s=s;
            eigv = sigma.*(alpha+pi^2.*bsxfun(@(x,y)(x.^2+y.^2),(0.5:ncmp-.5)',0.5:ncmp-.5)).^(-s/2);
            cosix = cos(pi.*centroid(1,:)'*(0.5:ncmp-.5)); cosiy = cos(pi.*centroid(2,:)'*(0.5:ncmp-.5));
            eigf = bsxfun(@times,cosix,reshape(cosiy,Nt,1,ncmp));
        case 'PCA'
            if isempty(varargin)
                varargin{1}=.1; % correlation length of GP
                varargin{2}=2; % exponential in GP prior
            elseif length(varargin)==1
                varargin{2}=2;
            end
            l=varargin{1};ex=varargin{2}; PDE.l=l;PDE.ex=ex;
            C = exp(-pdist2(centroid',centroid').^ex/(2*l^ex));
            % number of components in PCA
            K=min([D,Nt]);
            [evc,ev] = eigs(C,K,'LA'); % there are some approximations behind it, I guess, check C*evc./evc! but much faster
            eigv = sigma.*sqrt(diag(ev)); eigf = evc; % ok under fixed random seed
        otherwise
            error('Wrong option!');
    end

    % PDE coefficient decomposition
    PDE.eigv=eigv; PDE.eigf=eigf;
end

end
