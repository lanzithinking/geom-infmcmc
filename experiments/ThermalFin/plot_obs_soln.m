%%% This is to plot observations and forward solution of thermal fin problem. %%%
clear;
addpath('~/Documents/MATLAB/tight_subplot/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

% mesh setting
h=0.2; Bi=.1;
ncmp = 10; D = ncmp^2;

% basis setting
basis_types={'Fourier','PCA'};
basis_type=basis_types{1};
% scale parameter for prior
sigma=1;
% Fouerier basis parameters
alpha=0; s=1.2;
% PCA parameters
l=.1; ex=1;

% prepare PDE setup for inference
switch basis_type
    case basis_types{1}
        PDE4inf=PDEsetup(h,Bi,D,basis_type,sigma,alpha,s);
    case basis_types{2}
        PDE4inf=PDEsetup(h,Bi,D,basis_type,sigma,l,ex);
    otherwise
        error('wrong choice!');
end
% observations on dOmega\Gamma
e_idx=unique(PDE4inf.e(1:2,:));
x=PDE4inf.p(:,e_idx)';
x((x(:,1)>-.5&x(:,1)<.5)&(x(:,2)==0),:)=[];
% solve PDE on a finer mesh for observations
PDE4obs=PDErefine(PDE4inf);
% u=PDEsol4obs(PDE4obs);
% theta_truth = randn(D,1);
theta_truth = sin(bsxfun(@(x,y)x.^2+y.^2,(0.5:ncmp-.5)',0.5:ncmp-.5));
u=PDEsol_adj(theta_truth(:),PDE4obs);
% observation operator
O=sparse(pdist2(x,PDE4obs.p','chebychev','smallest',1)<1e-7)';
u_obs=u(O);
% model parameters
sigmay=.01*max(u_obs); sigma2y=sigmay^2;
y=u_obs+sigmay.*randn(length(u_obs),1);
% update observation operator for inferece
O=sparse(pdist2(x,PDE4inf.p','chebychev','smallest',1)<1e-7)';
% set up objective
obj=misfit(x,y,sigma2y,O);


% show on the geometry plot
fig=figure(1); clf;
set(fig,'pos',[0 800 800 350]);
ha=tight_subplot(1,2,[0 .08],[.12,.08],[.08,.1]);


subplot(ha(1));
pdeplot(PDE4inf.p,PDE4inf.e,PDE4inf.t);hold on;plot(x(:,1),x(:,2),'ko','markersize',8);hold off;
xlabel('x_1','fontsize',15);ylabel('x_2','fontsize',15,'rot',0);
ylabh=get(gca,'YLabel');set(ylabh,'Position',get(ylabh,'Position') - [0.2 0 0]);
set(gca,'FontSize',14);


% plot PDE solution
h_sub2=subplot(ha(2));
pdeplot(PDE4obs.p,PDE4obs.e,PDE4obs.t,'xydata',u,'contour','on','colorbar','off');
% title('Forward Solution','fontsize',15,'interpreter','latex');
xlabel('x_1','fontsize',15);ylabel('x_2','fontsize',15,'rot',0);
ylabh=get(gca,'YLabel');set(ylabh,'Position',get(ylabh,'Position') - [0.2 0 0]);
set(gca,'FontSize',14);
h2_pos=h_sub2.Position;
colorbar('position',[sum(h2_pos([1,3]))+.02 h2_pos(2) 0.02 h2_pos(4)],'linewidth',.1);

% save to file
fig.PaperPositionMode = 'auto';
print(fig,'./summary/obs_soln','-dpng','-r0');