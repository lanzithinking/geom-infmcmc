%%% This is to plot some posteriors of samples. %%%

clear;
addpath('../');
addpath('~/Documents/MATLAB/tight_subplot/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

% algorithms
% alg={'pCN','infMALA','HHMC','infMMALA','splitinfMMALA'};
alg={'HHMC'};%{'splitinfMMALA'};
alg_name={'pCN','$$\infty$$-MALA','H-HMC','$$\infty$$-MMALA','split$$\infty$$-MMALA'};
Nalg=length(alg);

% dimension setting
ncmp = 10; D = ncmp^2;
dev=zeros(D,4);
dev_name={'difference in CDF','K-L divergence'};

% estimate the deviation measure
% load data
files = dir('./ANAposterior');
nfiles = length(files) - 2;

for i=1:Nalg
    for j=1:nfiles
        if ~isempty(strfind(files(j+2).name,['_',alg{i},'_D',num2str(D),'_']))
                    load(strcat('./ANAposterior/', files(j+2).name));
        end
    end
    % estimate sample mean and sample standard deviation
    m=mean(samp); s=std(samp);
    % criterion 1: difference in CDF
    tic;
    for d=1:D
        [F,X]=ecdf(samp(:,d));
        Phi=normcdf(X,0,1);
        dev(d,1)=max(abs(F-Phi));
%         dev(d,1)=norm(F-Phi);
        Phi=normcdf(X,m(d),s(d));
        dev(d,3)=max(abs(F-Phi));
    end
    toc
    % criterion 2: K-L divergence
    tic;
    for d=1:D
        f=ksdensity(samp(:,d),samp(:,d));
        phi=normpdf(samp(:,d),0,1);
        dev(d,2)=mean(log(f)-log(phi));
        phi=normpdf(samp(:,d),m(d),s(d));
        dev(d,4)=mean(log(f)-log(phi));
    end
    toc
end

% find the largest 3 deviations
[Y,I]=sort(dev,1,'descend');
% max3=I(1:3,[1,3]); % using CDF
max3=I(1:3,[2,4]); % using KL divergence

% set plot
fig1=figure(1); clf;
% set(fig1,'windowstyle','docked');
set(fig1,'pos',[0 800 900 500]);
% fig=figure('visible','off');
ha=tight_subplot(2,3,[.12 0.1],[.1,.1],[.07,.05]);

% plot
for i=1:2
    if i==1
        mu=0;sigma=1;
    end
    for j=1:3
        subplot(ha((i-1)*3+j));
%         histfit(samp(:,max3(j,i)),20,'kernel');
        histogram(samp(:,max3(j,i)),'normalization','pdf'); hold on;
        if i==2
            mu=m(max3(j,i));sigma=s(max3(j,i));
            RANGE=[min(samp(:,max3(j,i))),max(samp(:,max3(j,i)))];
        else
            RANGE=[min([-3,min(samp(:,max3(j,i)))]),max([3,max(samp(:,max3(j,i)))])];
        end
        [f,xi]=ksdensity(samp(:,max3(j,i))); plot(xi,f,'--r','linewidth',2); hold on;
        fplot(@(x)normpdf(x,mu,sigma),[RANGE(1),RANGE(2)],'b-'); hold off;
        set(gca,'FontSize',15);
        xlim([RANGE(1),RANGE(2)]);
        xlabel(['u_{',num2str(max3(j,i)),'}'],'fontsize',15);
    end
end
h = suptitle('3 posteriors most deviated from prior/Gaussian');
set(h,'FontSize',18,'FontWeight','normal');

% save to file
% print(fig,'-dpng','./figure/somePosteriors');