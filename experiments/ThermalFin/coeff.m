%%%% coefficient function of Elliptic PDE %%%%

function [c,pc,p2c]=coeff(p,t,u,time,theta,varargin)
narginchk(5,7);
if isempty(varargin)
    s=1.1;
elseif length(varargin)==1
    s=varargin{1};
else
    eigv=varargin{1};eigf=varargin{2};
end
c=[]; pc=[]; p2c=[];

% use dct2: more error introduced due to interpolation
% A=meshsz/2.*eigv.*reshape(theta,size(eigv)); A(:,1)=2.*A(:,1); A(1,2:end)=2.*A(1,2:end);
% logc=dct2(A,meshsz,meshsz);
% logc=pdeintrp(p,t,logc(:));

% dimension
D = length(theta);

if exist('s','var')
    % prepare for the PDE coefficients
    Nt = size(t,2);
    % Triangle point indices
    it1 = t(1,:);
    it2 = t(2,:);
    it3 = t(3,:);
    % Find centroids of triangles
    centroid = (p(:,it1)+p(:,it2)+p(:,it3))/3;
    % 2d fourier basis
    ncmp = sqrt(D);
    eigv = pi^(-s).*bsxfun(@(x,y)(x.^2+y.^2).^(-s/2),((1:ncmp)-.5)',(1:ncmp)-.5);
    cosix = cos(pi.*centroid(1,:)'*((1:ncmp)-.5)); cosiy = cos(pi.*centroid(2,:)'*((1:ncmp)-.5));
    eigf = bsxfun(@times,cosix,reshape(cosiy,Nt,1,ncmp));
end

plogc = bsxfun(@times,eigv(:)',eigf(:,:)); % (Nt,D)
logc = plogc*theta;
c = exp(logc); % (Nt,1)

if nargout>1
    pc = bsxfun(@times,c,plogc); % (Nt,D)
end
if nargout>2
    p2c = bsxfun(@times,pc,reshape(plogc,[],1,D)); % (Nt,D,D)
end

end