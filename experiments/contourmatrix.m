% This is the generic function to plot pairwise marginal density contours
% as a matrix to visualize high dimensional data by Shiwei Lan (2014).

function H=contourmatrix(X,dim,Ncontour)
if ~exist('kde')
    addpath('~/documents/matlab/');
end

[N,D]=size(X);
if nargin<2
    dim=1:D; Ncontour=7;
elseif nargin<3
    Ncontour=7;
end

% open window for plots
H=figure(1); hold on;
L=length(dim);

for i=1:L
    subplot(L,L,sub2ind([L,L],i,i));
    [f,x]=hist(X(:,i),50);
    bar(x,f/trapz(x,f));hold on;
    [F,XI]=ksdensity(X(:,i),'npoints',50);plot(XI,F,'r-','linewidth',1.5);hold off;
%     histfit(X(:,i),50,'kernel');
    if i>1
        for j=1:i-1
            subplot(L,L,sub2ind([L,L],j,i));
            p=kde(X(:,[i,j])',.2);
            [h,x,y]=hist(p,50);
            contour(x,y,h,Ncontour);
        end
    end
end
hold off;

end