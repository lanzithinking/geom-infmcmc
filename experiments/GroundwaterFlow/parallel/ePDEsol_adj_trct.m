% This is to solve a classic example of elliptic equation given mesh and
% boundary conditions and get the derivatives of objective wrt some design
% parameter theta using ADJOINT method with truncation on parameters.
% solution: u; satisfying
% d(cdu)=0;
% u(x,0)=x;
% u(x,1)=1-x;
% d_x u(0,y)=0;
% d_x u(1,y)=0;
% objective: obj(u)
% output: [u, obj, p_theta obj, p_ij obj];

function [u,obju,pobj,p2obj]=ePDEsol_adj_trct(theta,PDE,ind_trct,obj,opt,trct_opt,PLOT,PRINT)
if nargin<3
    ind_trct=[]; obj=[]; opt=0; trct_opt=[0,0,0]; PLOT=0; PRINT=0;
elseif nargin<4
    obj=[]; opt=0; trct_opt=[0,0,0]; PLOT=0; PRINT=0;
elseif nargin<5
    opt=0; trct_opt=[0,0,0]; PLOT=0; PRINT=0;
elseif nargin<6
    trct_opt=[0,0,0]; PLOT=0; PRINT=0;
elseif nargin<7
    PLOT=0; PRINT=0;
elseif nargin<8
    PRINT=0;
end
u=[]; obju=[]; pobj=[]; p2obj=[];
% prepare to solve PDE
p=PDE.p;e=PDE.e;t=PDE.t; % mesh
b=PDE.b; b_adj=PDE.db; % problems (with boudary conditions)

% setup parallel enviroment
if isempty(gcp)
    parpool(6);
end

% dimensions and stepsize
Np=size(p,2); meshsz=sqrt(Np); h=1/(meshsz-1);
D=length(theta);
if isempty(ind_trct)
    ind_trct=true(D,1);
end

% index of truncation
I=find(ind_trct);
if trct_opt(1)
    theta = theta(I);
end

% coefficients
if all(opt==0)
    c=coeff(p,t,[],[],theta,PDE.eigv,PDE.eigf);
elseif all(ismember(opt,[0,1,2]))
    [c,pc]=coeff(p,t,[],[],theta,PDE.eigv,PDE.eigf);
end

if any(ismember([0,1,2],opt))
    % forward solution of u
    [K,F,B,ud] = assempde(b,p,e,t,-c',0,0);
    u1 = K\F; % solution without Dirichlet boundary
    u = B*u1+ud; % true solution
    % construct interpolator
    if nargout>1
        obju = obj.eval(u);
    end
    % plot the forward solution
    if PLOT
        fig1=figure(1); clf;
        pdesurf(p,t,u);
        title('Forward Solution','fontsize',18);
    end
    % print the forward solution
    if PRINT
        print(fig1,'-dpng','u_fwd');
    end
end
if any(opt==1)
    % solve adjoint equation for pobj
    [~,Fa]=f_adj(p,t,u,[],obj); % calculate rhs at nodes
    Fa=-Fa.*h^2; % times element
    Fa=reshape(Fa,meshsz,meshsz); Fa=Fa(:,2:meshsz-1); % remove Dirichlet boundary
    lambda = B*(K\Fa(:));
    % plot the adjoint solution
    if PLOT
        fig2=figure(2); clf;
        pdesurf(p,t,lambda);
        title('Adjoint Solution','fontsize',18);
    end
    % print the adjoint solution
    if PRINT
        print(fig2,'-dpng','u_adj');
    end
end
if any(ismember([1,2],opt))
    if ~trct_opt(2)%&&ntrct<D
        I_1=1:D;
    else
        I_1=I;
    end
    dim_p1=length(I_1);
    pobj=zeros(dim_p1,1);
    if any(opt==2)
        dK = cell(dim_p1,1);
    end
    parfor i=1:dim_p1
        % obtain stiffness matrix with pc_i
        [K_i,~] = assempde(b_adj,p,e,t,-pc(:,I_1(i))',0,0);
        if any(opt==2)
            dK{i} = K_i;
        end
        % solution of p_i obj
        if any(opt==1)
            pobj(i) = (lambda'*K_i*u)/h^2;
        end
    end
end
if any(opt==2)
    
    if ~trct_opt(3)%&&ntrct<D
        I_2=1:D;
    else
        I_2=I;
    end
    dim_p2=length(I_2);
    if trct_opt(3)==trct_opt(2)
        I_2in1=1:dim_p1;
    elseif trct_opt(3)>trct_opt(2)
        I_2in1=I;
    else
        error('Error! Have to truncate on metric because gradient has been truncated!');
    end
    
    p2obj=zeros(dim_p2);
    if PLOT
        fig3=figure(3); clf;
        fig4=figure(4); clf;
        Ncol=min([ceil(sqrt(dim_p2)),5]);
        Nrow=min([ceil(dim_p2/Ncol),5]);
    end
    parfor i=1:dim_p2
        v=zeros(dim_p2,1); v(i)=1;
        % solve 2nd order forward equation
        [K2f,F2f]=assempde(b_adj,p,e,t,-(pc(:,I_2)*v)',0,0);
        F2=K2f*u-F2f;
        F2=reshape(F2,meshsz,meshsz); F2=F2(:,2:meshsz-1); % remove Dirichlet boundary
        deltay = B*(K\F2(:));
        % plot the 2nd order forward solution
%         if PLOT&&i<=Nrow*Ncol
%             set(0,'CurrentFigure',fig3);
%             subplot(Nrow,Ncol,i);
%             pdesurf(p,t,deltay);
%         end
        
        % solve 2nd order adjoint equation
        [~,F2a]=f2_adj(p,t,deltay,[],obj); % calculate rhs at nodes
        F2a=-F2a.*h^2; % times element
        F2a=reshape(F2a,meshsz,meshsz); F2a=F2a(:,2:meshsz-1); % remove Dirichlet boundary
        deltalambda = B*(K\F2a(:));
        % plot the adjoint solution
%         if PLOT&&i<=Nrow*Ncol
%             set(0,'CurrentFigure',fig4);
%             subplot(Nrow,Ncol,i);
%             pdesurf(p,t,deltalambda);
%         end
        
        % calculate metric-vector
        for j=1:dim_p2
            % solution of p_ij obj
            p2obj(i,j) = -(deltalambda'*dK{I_2in1(j)}*u)/h^2;
        end
    end
    
    % add common titles
    if PLOT
        set(0,'CurrentFigure',fig3);
        suptitle('2nd order Forward Solution','fontsize',18);
        set(0,'CurrentFigure',fig4);
        suptitle('2nd order Adjoint Solution','fontsize',18);
    end
    
    % print solutions
    if PRINT
        % print the 2nd order forward solution
        print(fig3,'-dpng','u2_fwd');
        % print the 2nd order adjoint solution
        print(fig4,'-dpng','u2_adj');
    end
    
end


end