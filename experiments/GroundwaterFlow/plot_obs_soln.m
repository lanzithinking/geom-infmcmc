%%% This is to plot observations and forward solution of groundwater problem. %%%
clear;
addpath('~/Documents/MATLAB/tight_subplot/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

% mesh setting
Nmesh=40; % for data generating
ncmp = 10; D = ncmp^2;

% basis setting
basis_types={'Fourier','PCA'};
basis_type=basis_types{1};
% scale parameter for prior
sigma=1;
% Fouerier basis parameters
alpha=0; s=1.1;
% PCA parameters
l=.2; ex=2;
% observation setting
obs_types={'uniform','circle'};
obs_type=obs_types{2}; Ncirc=1;
% standaard deviation of noise in observation
sigma_y=1e-2;

% generate data
% theta_truth = randn(D,1);
theta_truth = sin(bsxfun(@(x,y)x.^2+y.^2,(0.5:ncmp-.5)',0.5:ncmp-.5));
% prepare PDE solver for observation
switch basis_type
    case basis_types{1}
        PDE4obs=ePDEsetup(Nmesh,D,basis_type,sigma,alpha,s);
    case basis_types{2}
        PDE4obs=ePDEsetup(Nmesh,D,basis_type,sigma,l,ex);
    otherwise
        error('wrong choice!');
end
u=ePDEsol_adj(theta_truth(:),PDE4obs);
% observations
switch obs_type
    case 'uniform'
        obs_sz=5;
        obs_idx=ceil(linspace(1,Nmesh+1,obs_sz+1));
        O=logical(sparse(Nmesh+1,Nmesh+1)); O(obs_idx,obs_idx)=1; % observation operator
        x=PDE4obs.p(:,O(:))';
    case 'circle'
        p=PDE4obs.p;
        obs_ind=false(1,size(p,2));
        for i=1:Ncirc
            obs_ind=obs_ind|abs(sum((p-.5).^2)-(2^(-i))^2)<1e-2; % on circle(s)
        end
        x=[p(:,obs_ind)';.5,.5];
        x=unique(x,'rows');
    otherwise
        error('Wrong option!');
end

% observation operator
O=sparse(pdist2(x,PDE4obs.p','chebychev','smallest',1)<1e-7)';
u_obs=u(O);
y=u_obs(:)+sigma_y.*randn(length(u_obs),1);
% set up objective
obj=misfit(x,y,sigma_y^2,O);

% prepare PDE solver for inference
meshsz=20; % mesh size for inference
switch basis_type
    case basis_types{1}
        PDE4inf=ePDEsetup(meshsz,D,basis_type,sigma,alpha,s);
    case basis_types{2}
        PDE4inf=ePDEsetup(meshsz,D,basis_type,sigma,l,ex);
    otherwise
        error('wrong choice!');
end
% update objective is the mesh size changes
if meshsz~=Nmesh
    obj.O=sparse(pdist2(obj.x,PDE4inf.p','chebychev','smallest',1)<1e-7)';
    x=PDE4inf.p(:,obj.O)';
%     subplot(1,2,2);
%     pdeplot(PDE4inf.p,PDE4inf.e,PDE4inf.t);hold on;plot(x(:,1),x(:,2),'ko','markersize',8);hold off;
    obj.y=obj.y(ismember(obj.x,x,'rows'));
    obj.x=x;
end


% show on the geometry plot
fig=figure(1); clf;
set(fig,'pos',[0 800 800 350]);
ha=tight_subplot(1,2,[0 .08],[.12,.08],[.08,.1]);


subplot(ha(1));
pdeplot(PDE4inf.p,PDE4inf.e,PDE4inf.t);hold on;plot(x(:,1),x(:,2),'ko','markersize',8);hold off;
xlabel('x_1','fontsize',15);ylabel('x_2','fontsize',15,'rot',0);
ylabh=get(gca,'YLabel');set(ylabh,'Position',get(ylabh,'Position') - [0.07 0 0]);
set(gca,'FontSize',14);


% plot PDE solution
h_sub2=subplot(ha(2));
pdeplot(PDE4obs.p,PDE4obs.e,PDE4obs.t,'xydata',u,'contour','on','colorbar','off');
% title('Forward Solution','fontsize',15,'interpreter','latex');
xlabel('x_1','fontsize',15);ylabel('x_2','fontsize',15,'rot',0);
ylabh=get(gca,'YLabel');set(ylabh,'Position',get(ylabh,'Position') - [0.07 0 0]);
set(gca,'FontSize',14);
h2_pos=h_sub2.Position;
colorbar('position',[sum(h2_pos([1,3]))+.02 h2_pos(2) 0.02 h2_pos(4)],'linewidth',.1);

% save to file
fig.PaperPositionMode = 'auto';
print(fig,'./summary/obs_soln','-dpng','-r0');