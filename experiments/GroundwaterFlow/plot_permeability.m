%%% This is to plot permeability of groundwater problem. %%%

clear;
addpath('~/Documents/MATLAB/tight_subplot/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

% algorithms
alg={'pCN','infMALA','HHMC','infMMALA','splitinfMMALA','splitinfmHMC'};
alg_name={'pCN','$$\infty$$-MALA','$$\infty$$-HMC','$$\infty$$-MMALA','split$$\infty$$-MMALA','split$$\infty$$-mHMC'};
Nalg=length(alg);

% mesh setting
Nmesh=40; % for data generating
ncmp = 10; D = ncmp^2;
basis_type='Fourier';
sigma=1; %s=1.1;
obs_type='circle';
sigma_y=1e-2;

% true permeability
% load data
load(['ellipticPDE_Nmesh',num2str(Nmesh),'_D',num2str(D),'_',basis_type,'_priorsd',num2str(sigma),'_',obs_type,'_noisesd',num2str(sigma_y),'.mat']);% mesh
p=PDE4obs.p;e=PDE4obs.e;t=PDE4obs.t;
% permeability
c=coeff(p,t,[],[],theta_truth(:),PDE4obs.eigv,PDE4obs.eigf);
% plot
fig1=figure(1); clf;
pdeplot(p,e,t,'xydata',c,'contour','on','colorbar','off');
title('True Permeability','fontsize',18);
xlabel('x_1','fontsize',15);ylabel('x_2','fontsize',15,'rot',0);
ylabh=get(gca,'YLabel');set(ylabh,'Position',get(ylabh,'Position') - [0.05 0 0]);
set(gca,'FontSize',15);

 % set plot for estimated permeability
fig2=figure(2); clf;
% set(fig1,'windowstyle','docked');
set(fig2,'pos',[0 800 800 500]);
% fig=figure('visible','off');
nCOL=ceil(Nalg/2);
ha1=tight_subplot(2,nCOL,[.13 .07],[.1,.08],[.07,.1]);

fig3=figure(3); clf;
set(fig3,'pos',[0 0 800 500]);
ha2=tight_subplot(2,nCOL,[.13 .07],[.1,.08],[.07,.1]);

% estimate permeability
% load data
files = dir('./summary');
nfiles = length(files) - 2;

for i=1:Nalg
    found=0;
    for j=1:nfiles
        if ~isempty(strfind(files(j+2).name,['_',alg{i},'_D',num2str(D),'_']))
            load(strcat('./summary/', files(j+2).name));
            found=1;
        end
    end
    if found
        % mesh
        PDE4inf=data.PDE4inf;
        p=PDE4inf.p;e=PDE4inf.e;t=PDE4inf.t;
        eigv=PDE4inf.eigv;eigf=PDE4inf.eigf;
%         % estimate permeability
%         theta_est=mean(samp)';
%         c=coeff(p,t,[],[],theta_est,s);
        % permeabilities
        Nsamp=size(samp,1); Nt = size(t,2);
        u=zeros(Nsamp,Nt);
        for n=1:Nsamp
            u(n,:)=coeff(p,t,[],[],samp(n,:)',eigv,eigf);
        end
        u_mean=mean(u); u_var=var(u);
        % plot
        set(0,'currentfigure',fig2);
        h_sub1=subplot(ha1(i));
        pdeplot(p,e,t,'xydata',u_mean,'contour','on','colorbar','off');
        title(alg_name{i},'fontsize',15,'interpreter','latex');
        xlabel('x_1','fontsize',15);ylabel('x_2','fontsize',15,'rot',0);
        ylabh=get(gca,'YLabel');set(ylabh,'Position',get(ylabh,'Position') - [0.1 0 0]);
        set(gca,'FontSize',15);
        set(0,'currentfigure',fig3);
        h_sub2=subplot(ha2(i));
        pdeplot(p,e,t,'xydata',u_var,'contour','on','colorbar','off');
        title(alg_name{i},'fontsize',15,'interpreter','latex');
        xlabel('x_1','fontsize',15);ylabel('x_2','fontsize',15,'rot',0);
        ylabh=get(gca,'YLabel');set(ylabh,'Position',get(ylabh,'Position') - [0.1 0 0]);
        set(gca,'FontSize',15);
    end
end
set(0,'currentfigure',fig2);
% set same color range
colorbar('position',[sum(h_sub1.Position([1,3]))+.02 h_sub1.Position(2) 0.02 0.82],'linewidth',.1);
% caxis([0,8.5]);
% h = suptitle('Estimate of Mean');
% set(h,'FontSize',18,'FontWeight','normal');
set(0,'currentfigure',fig3);
% set same color range
colorbar('position',[sum(h_sub2.Position([1,3]))+.02 h_sub2.Position(2) 0.02 0.82],'linewidth',.1);
% caxis([0,8.5]);
% h = suptitle('Estimate of Variance');
% set(h,'FontSize',18,'FontWeight','normal');

% save to file
% print(fig,'-dpng','./figure/permeability');