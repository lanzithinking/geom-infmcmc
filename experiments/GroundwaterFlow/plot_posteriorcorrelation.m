%%% This is to plot correlation of posterior in groundwater problem. %%%

clear;
addpath('~/Documents/MATLAB/tight_subplot/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

% algorithms
alg={'pCN','infMALA','HHMC','infMMALA','splitinfMMALA'};
alg_name={'pCN','$$\infty$$-MALA','H-HMC','$$\infty$$-MMALA','split$$\infty$$-MMALA'};
Nalg=length(alg);

 % set plot
fig1=figure(1); clf;
% set(fig1,'windowstyle','docked');
set(fig1,'pos',[0 800 900 450]);
% fig=figure('visible','off');
ha=tight_subplot(2,ceil(Nalg/2),[.15 0.085],[.1,.06],[.07,.05]);

% mesh setting
Nmesh=50; % for data generating
s=1.1; ncmp = 10; D = ncmp^2;

% true parameters theta
% load data
load(['ellipticPDE',num2str(Nmesh),'_D',num2str(D),'.mat']);
% plot
subplot(ha(1));
imagesc(theta_truth);
title('True Parameter','fontsize',18);
xlabel('d_1','fontsize',15);ylabel('d_2','fontsize',15,'rot',0);
ylabh=get(gca,'YLabel');set(ylabh,'Position',get(ylabh,'Position') - [1 0 0]);
set(gca,'Ydir','normal');
set(gca,'FontSize',15);
colorbar;

% estimate permeability
% load data
files = dir('./summary');
nfiles = length(files) - 2;

for i=1:Nalg
    for j=1:nfiles
        if ~isempty(strfind(files(j+2).name,['_',alg{i},'_D',num2str(D),'_']))
                    load(strcat('./summary/', files(j+2).name));
        end
    end
    % estimate correlation of posterior samples
    cor_theta=corr(samp);
    % plot
    subplot(ha(i+1));
    imagesc(cor_theta);
    title(['Post-Corr by ',alg_name{i}],'fontsize',18,'interpreter','latex');
%     xlabel('x_1','fontsize',15);ylabel('x_2','fontsize',15,'rot',0);
%     ylabh=get(gca,'YLabel');set(ylabh,'Position',get(ylabh,'Position') - [10 0 0]);
    set(gca,'Ydir','normal');
    set(gca,'FontSize',15);
    colorbar;
end

% save to file
% print(fig,'-dpng','./figure/posteriorcorrelation');