%%% This is to plot trace of misfits in groundwater problem. %%%

clear;
addpath('~/Documents/MATLAB/tight_subplot/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

% algorithms
alg={'pCN','infMALA','infHMC','infmMALA','infmHMC','splitinfmMALA','splitinfmHMC'};
alg_name={'pCN','$$\infty$$-MALA','$$\infty$$-HMC','$$\infty$$-mMALA','$$\infty$$-mHMC','split$$\infty$$-mMALA','split$$\infty$$-mHMC'};
Nalg=length(alg);

D=100;

% plot specifications
colors = distinguishable_colors(Nalg);
styles = {'-','-.','--','-','--',':','--'};

% set plot
fig=figure(1); clf;
set(fig,'pos',[0 800 900 450]);
% fig=figure('visible','off');
ha=tight_subplot(2,1,[.12 0],[.1,.06],[.07,.2]);


% gather misfits by different algorithms
spiter=zeros(Nalg,1); % time per iteration
% load data
files = dir('./summary');
nfiles = length(files) - 2;

for i=1:Nalg
    found=0;
    for j=1:nfiles
        if ~isempty(strfind(files(j+2).name,['_',alg{i},'_D',num2str(D),'_']))
            load(strcat('./summary/', files(j+2).name));
            found=1;
        end
    end
    if found
        % create misfit
        if ~exist('misfit','var')
            misfit=zeros(length(loglik),Nalg);
        end
        misfit(:,i)=-loglik;
        spiter(i)=time/(setting.Nsamp-setting.NBurnIn);
    end
end

% plot misfits vs iterations
subplot(ha(1));
max_iter = 200;
for i=1:Nalg
    semilogy(misfit(1:max_iter,i), styles{i},'color',colors(i,:),'linewidth',1.5); hold on;
end
drawnow;
set(gca,'FontSize',14);
xlim([1,max_iter]);ylim([0,40]);
xlabel('Iterations','FontSize',15); ylabel('Data  Misfit','FontSize',15);

% plot misifits vs time
h2_sub=subplot(ha(2));
max_time = 40;
for i=1:Nalg
    nsamp_in=floor(max_time/spiter(i));
    semilogy(linspace(0,max_time,nsamp_in),misfit(1:nsamp_in,i), styles{i},'color',colors(i,:),'linewidth',1.5); hold on;
end
drawnow;
% set(gca,'XTick',1:max_time);
set(gca,'FontSize',14);
xlim([1,max_time]);ylim([0,40]);
xlabel('Seconds','FontSize',15); ylabel('Data  Misfit','FontSize',15);

lgnd=legend(alg_name,'FontSize',13,'interpreter','latex','location','EastOutside');
h2_pos=h2_sub.Position;
set(lgnd,'position',[sum(h2_pos([1,3]))+.08 h2_pos(2) 0.05 h2_pos(4)*2.33]);

% save to file
fig.PaperPositionMode = 'auto';
print(fig,'./summary/misfit','-dpng','-r0');