%%%% This is MCMC sampling by HHMC %%%%
clear;
addpath('../sampler/');
addpath('../');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

% mesh setting
Nmesh=50; % for data generating
s=1.1; ncmp = 10; D = ncmp^2;

try
    % load data
    load(['ellipticPDE',num2str(Nmesh),'_D',num2str(D),'.mat']);
catch
    % generate data
%     theta_truth = randn(D,1);
    theta_truth = sin(bsxfun(@(x,y)x.^2+y.^2,(1:ncmp)',1:ncmp));
    % prepare PDE solver
    PDE=ePDEsetup(Nmesh,D);
    u=ePDEsol_adj(theta_truth(:),PDE);
    % model parameters
    sigma2y=1e-4; mtheta=0; sigma2theta=1;
    % observations on a circle
    p=PDE.p;
    obs_ind=abs(sum((p-.5).^2)-.5^2)<1e-4;
    x=[p(:,obs_ind)';.5,.5];
    % show on the geometry plot
%     pdeplot(PDE.p,PDE.e,PDE.t);hold on;plot(x(:,1),x(:,2),'ko','markersize',8);hold off;
    % observation operator
    O=sparse(pdist2(x,p','chebychev','smallest',1)<1e-7)';
    u=u(O);
    y=u(:)+sqrt(sigma2y).*randn(length(u),1);
    % set up objective
    obj=misfit(x,y,sigma2y,O);
    % save data
    save(['ellipticPDE',num2str(Nmesh),'_D',num2str(D),'.mat'],'seed','Nmesh','PDE','D','theta_truth','obj','mtheta','sigma2theta');
end

% sampling setting
stepsz = .03; Nleap = 4;

% allocation to save
Nsamp = 6000; NBurnIn = 1000;
samp = zeros(Nsamp-NBurnIn,D);
loglik = zeros(Nsamp,1);
accp = 0; % online acceptance
acpt = 0; % final acceptance rate
global pde_sol_cnt
pde_sol_cnt=zeros(4,1);

% prepare PDE solver for inference
meshsz=20; % mesh size for inference
PDE20=ePDEsetup(meshsz,D,'PCA',.5,.1,1);
% update objective is the mesh size changes
if meshsz~=Nmesh
    obj.O=sparse(pdist2(obj.x,PDE20.p','chebychev','smallest',1)<1e-7)';
    x=PDE20.p(:,obj.O)';
    obj.y=obj.y(ismember(obj.x,x,'rows'));
    obj.x=x;
end

% Initialize
theta = randn(D,1);
[nll,g] = GEOM4inf(theta,PDE20,[],obj,0,sigma2theta,[0,1]);

disp(' ');
disp('Running Hilbert space HMC...');
for Iter = 1:Nsamp
    
    % display every 100 iterations
    if mod(Iter,100) == 0
        disp([num2str(Iter) ' iterations completed.']);
        disp(['Current acceptance: ',num2str(accp/100)]);
        accp=0;
    end
    
    % sample with HHMC
    [theta,nll,g,acpt_idx] = HHMC(theta,nll,g,sqrt(sigma2theta).*speye(D),...
                                  @(theta)GEOM4inf(theta,PDE20,[],obj,0,sigma2theta,[0,1]),stepsz,Nleap);
    accp = accp + acpt_idx;
    
    % Start timer after burn-in
    if Iter == NBurnIn
        disp('Burn-in complete, now drawing samples.'); tic;
    end
    
    loglik(Iter) = -nll;
    % Save samples if required
    if Iter > NBurnIn
        samp(Iter-NBurnIn,:) = theta';
        acpt = acpt + acpt_idx;
    end
    
end

% save results
time=toc;
acpt=acpt/(Nsamp-NBurnIn);
curTime = regexprep(num2str(fix(clock)),'    ','_');
curfile=['EllipticPDE_HHMC_D',num2str(D),'__', curTime];
save(['result/',curfile,'.mat'],'meshsz','PDE20','stepsz','Nleap','samp','loglik','acpt','time','pde_sol_cnt');
disp(' ');
disp(['Accpetance Rate of HHMC: ',num2str(acpt)]);
disp(' ');
addpath('./result/');
CalculateStatistics(curfile,'./result/');
% some plots
fig1=figure(1); set(fig1,'pos',[0 800 900 300]);
idx=floor(linspace(1,size(samp,1),min([1e4,size(samp,1)])));
dim=[1,2,floor(D/2),D];
subplot(1,3,1);
plot(samp(idx,dim));
subplot(1,3,2);
plotmatrix(samp(idx,dim));
subplot(1,3,3);
plot(loglik);