%%% This is to plot permeability of groundwater problem. %%%

clear;
addpath('~/Documents/MATLAB/subplot_tight/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

% algorithms
alg={'pCN','infMALA','HHMC','infMMALA','splitinfMMALA','splitinfmHMC'};
alg_name={'pCN','$$\infty$$-MALA','H-HMC','$$\infty$$-MMALA','split$$\infty$$-MMALA','split$$\infty$$-mHMC'};
Nalg=length(alg);

 % set plot
fig1=figure(1); clf;
% set(fig1,'windowstyle','docked');
set(fig1,'pos',[0 800 1100 450]);
% fig=figure('visible','off');

% mesh setting
Nmesh=40; % for data generating
s=1.1; ncmp = 10; D = ncmp^2;
basis_type='Fourier'; obs_type='circle';
nCOL=ceil(Nalg/2)+2;

% true permeability
% load data
load(['ellipticPDE_Nmesh',num2str(Nmesh),'_D',num2str(D),'_',basis_type,'_',obs_type,'.mat']);
% mesh
p=PDE4obs.p;e=PDE4obs.e;t=PDE4obs.t;
% permeability
u_mean=coeff(p,t,[],[],theta_truth(:),s);
% plot
subplot_tight(2,nCOL,[1,2,nCOL+[1,2]],[.12,.05]);
pdeplot(p,e,t,'xydata',u_mean,'contour','on','colorbar','off');
title('True Permeability','fontsize',18);
xlabel('x_1','fontsize',15);ylabel('x_2','fontsize',15,'rot',0);
ylabh=get(gca,'YLabel');set(ylabh,'Position',get(ylabh,'Position') - [0.05 0 0]);
set(gca,'FontSize',15);

% estimate permeability
% load data
files = dir('./summary');
nfiles = length(files) - 2;

for i=1:Nalg
    found=0;
    for j=1:nfiles
        if ~isempty(strfind(files(j+2).name,['_',alg{i},'_D',num2str(D),'_']))
            load(strcat('./summary/', files(j+2).name));
            found=1;
        end
    end
    if found
        % mesh
        PDE4inf=data.PDE4inf;
        p=PDE4inf.p;e=PDE4inf.e;t=PDE4inf.t;
        eigv=PDE4inf.eigv;eigf=PDE4inf.eigf;
        % permeabilities
        Nsamp=size(samp,1); Nt = size(t,2);
        u=zeros(Nsamp,Nt);
        for n=1:Nsamp
            u(n,:)=coeff(p,t,[],[],samp(n,:)',eigv,eigf);
        end
        u_mean=mean(u); u_var=var(u);
        % plot
        subplot_tight(2,nCOL,(ceil(i/ceil(Nalg/2))-1)*nCOL+mod(i-1,ceil(Nalg/2))+1+2,[.12,.05]);
        pdeplot(p,e,t,'xydata',u_var,'contour','on','colorbar','off');
        title(alg_name{i},'fontsize',15,'interpreter','latex');
        xlabel('x_1','fontsize',15);ylabel('x_2','fontsize',15,'rot',0);
        ylabh=get(gca,'YLabel');set(ylabh,'Position',get(ylabh,'Position') - [0.1 0 0]);
        set(gca,'FontSize',15);
    end
end
% set same color range
colorbar('position',[.955 0.12 0.015 0.76],'linewidth',.1);
% caxis([0,8.5]);

% save to file
% print(fig,'-dpng','./figure/permeability');