% generate data
function obj=generateData(Nmesh,D,basis_type,prior_sd,obs_type,noise_sd,PLOT,SAVE,varargin)
narginchk(2,10);
if nargin<3
    basis_type='Fourier';prior_sd=1;obs_type='uniform';noise_sd=1e-2;PLOT=0;SAVE=1;
elseif nargin<4
    prior_sd=1;obs_type='uniform';noise_sd=1e-2;PLOT=0;SAVE=1;
elseif nargin<5
    obs_type='uniform';noise_sd=1e-2;PLOT=0;SAVE=1;
elseif nargin<6
    noise_sd=1e-2;PLOT=0;SAVE=1;
elseif nargin<7
    PLOT=0;SAVE=1;
end

% Random Numbers...
seed = RandStream('mt19937ar','Seed',year(date));
RandStream.setGlobalStream(seed);

% generate the true parameter
% theta_truth = randn(D,1);
ncmp = sqrt(D);
theta_truth = sin(bsxfun(@(x,y)x.^2+y.^2,(0.5:ncmp-.5)',0.5:ncmp-.5));
% prepare PDE solverfor observation
PDE4obs=ePDEsetup(Nmesh,D,basis_type,prior_sd,varargin{:});
u=ePDEsol_adj(theta_truth(:),PDE4obs);
% observations
p=PDE4obs.p;
switch obs_type
    case 'uniform'
        obs_sz=5;
        obs_idx=ceil(linspace(1,Nmesh+1,obs_sz+1));
        O=logical(sparse(Nmesh+1,Nmesh+1)); O(obs_idx,obs_idx)=1; % observation operator
        x=p(:,O(:))';
    case 'circle'
        obs_ind=false(1,size(p,2));
        for i=1:1
            obs_ind=obs_ind|abs(sum((p-.5).^2)-(2^(-i))^2)<1e-2; % on circle(s)
        end
        x=[p(:,obs_ind)';.5,.5];
    otherwise
        error('Wrong option!');
end
x=unique(x,'rows');
% show on the geometry plot
if PLOT
    pdeplot(PDE4obs.p,PDE4obs.e,PDE4obs.t);hold on;plot(x(:,1),x(:,2),'ko','markersize',8);hold off;
end

% observation operator
O=sparse(pdist2(x,PDE4obs.p','chebychev','smallest',1)<1e-7)';
u=u(O);
y=u(:)+noise_sd.*randn(length(u),1);
% set up objective
obj=misfit(x,y,noise_sd^2,O);
% save data
if SAVE
    save(['ellipticPDE_Nmesh',num2str(Nmesh),'_D',num2str(D),'_',basis_type,'_priorsd',num2str(prior_sd),'_',obs_type,'_noisesd',num2str(noise_sd),'.mat'],'seed','Nmesh','D','basis_type','obs_type','theta_truth','PDE4obs','obj');
end

end