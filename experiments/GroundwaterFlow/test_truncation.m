%%%% test PDE solvers with truncation %%%%
clear;

% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

%%%% generate data %%%%
% mesh settings
meshsz = 50; s=1.5; ncmp = 15; D = ncmp^2;
theta_truth = randn(D,1);
% prepare PDE solver
PDE=ePDEsetup(meshsz,D,'PCA');
u=reshape(ePDEsol_adj(theta_truth,PDE),meshsz+1,meshsz+1);
% model parameters
sigma2y=.1^2; %sigma2theta=1;
% observations
obs_sz=10;
% obs_idx=1+meshsz/obs_sz.*(0:obs_sz);
obs_idx=linspace(1,meshsz+1,obs_sz+1);
O=sparse(meshsz+1,meshsz+1); O(obs_idx,obs_idx)=1; % observation operator
seq=linspace(0,1,obs_sz+1);
[sx,sy]=meshgrid(seq);
x=[sx(:),sy(:)];
u=u(obs_idx,obs_idx);
y=u(:)+sqrt(sigma2y).*randn((1+obs_sz)^2,1);
% set up objective
obj=misfit(x,y,sigma2y,O);

%%%% Gradient dJ/dtheta and Fisher %%%%

% full version %
[u,nll,dnll,~,FI]=ePDEsol_adj(theta_truth,PDE,obj,[0,1,2]);


% truncation number
rtrct=5; ntrct=rtrct^2;
% index of truncation: indicator of being kept
% ind_trct=logical(sparse(ncmp,ncmp)); ind_trct(1:rtrct,1:rtrct)=1;
ind=sparse(ncmp,1); ind(randsample(ncmp,rtrct))=1;
ind_trct=logical(ind*ind');
% truncation option
trct_opt=[0,1,1];

% truncated version %
[u_t,nll_t,dnll_t,~,FI_t]=ePDEsol_adj_trct(theta_truth,PDE,ind_trct,obj,[0,1,2],trct_opt);

%%% Comparison %%%

% compare
fprintf('\nInf-norm difference in u: %.8e \n',max(abs(u-u_t)));
fprintf('\nDifference in nll: %.8e \n',abs(nll-nll_t));
if trct_opt(2)
    g_diff = max(abs(dnll(ind_trct)-dnll_t));
else
    g_diff = max(abs(dnll-dnll_t));
end
fprintf('\nInf-norm difference of gradients: %.8e \n',g_diff);
if trct_opt(3)
    F_diff = norm(FI(ind_trct,ind_trct)-FI_t)/meshsz;
else
    F_diff = norm(FI-FI_t)/meshsz;
end
fprintf('\nScaled 2-norm difference of Fisher: %.8e \n',F_diff);
