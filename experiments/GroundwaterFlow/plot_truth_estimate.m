%%% This is to plot true and estimated permeability of groundwater problem. %%%

clear;
addpath('~/Documents/MATLAB/tight_subplot/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

% algorithms
alg={'pCN','infMALA','infHMC','infmMALA','infmHMC','splitinfmMALA','splitinfmHMC'};
alg_name={'pCN','$$\infty$$-MALA','$$\infty$$-HMC','$$\infty$$-mMALA','$$\infty$$-mHMC','split$$\infty$$-mMALA','split$$\infty$$-mHMC'};
Nalg=length(alg);

% mesh setting
Nmesh=40; % for data generating
ncmp = 10; D = ncmp^2;
basis_type='Fourier';
sigma=1; %s=1.1;
obs_type='circle';
sigma_y=1e-2;


 % set plot for estimated permeability
fig=figure(1); clf;
set(fig,'pos',[0 800 1000 500]);
% fig=figure('visible','off');
nCOL=ceil(Nalg/2);
ha=tight_subplot(2,nCOL,[.135 .05],[.09,.06],[.05,.07]);

% true permeability
% load data
load(['ellipticPDE_Nmesh',num2str(Nmesh),'_D',num2str(D),'_',basis_type,'_priorsd',num2str(sigma),'_',obs_type,'_noisesd',num2str(sigma_y),'.mat']);% mesh
p=PDE4obs.p;e=PDE4obs.e;t=PDE4obs.t;
% permeability
c=coeff(p,t,[],[],theta_truth(:),PDE4obs.eigv,PDE4obs.eigf);

% meshsz=20;alpha=0;s=1.1;
% PDE4inf=ePDEsetup(meshsz,D,basis_type,sigma,alpha,s);
% p=PDE4inf.p;e=PDE4inf.e;t=PDE4inf.t;
% % cosix = cos(pi.*p(1,:)'*(.5:ncmp-.5)); cosiy = cos(pi.*p(2,:)'*(.5:ncmp-.5));
% % eigf = bsxfun(@times,cosix,reshape(cosiy,size(p,2),1,ncmp));
% % Triangle point indices
% it1 = t(1,:); it2 = t(2,:); it3 = t(3,:);
% % Find centroids of triangles
% centroid = (p(:,it1)+p(:,it2)+p(:,it3))/3;
% cosix = cos(pi.*centroid(1,:)'*(.5:ncmp-.5)); cosiy = cos(pi.*centroid(2,:)'*(.5:ncmp-.5));
% eigf = bsxfun(@times,cosix,reshape(cosiy,size(t,2),1,ncmp));
% plogc = bsxfun(@times,PDE4inf.eigv(:)',eigf(:,:));
% logc = plogc*theta_truth(:);
% c = exp(logc);

% plot
subplot(ha(1));
pdeplot(p,e,t,'xydata',c,'contour','on','colorbar','off');
title('True Permeability','fontsize',15);
xlabel('x_1','fontsize',15);ylabel('x_2','fontsize',15,'rot',0);
ylabh=get(gca,'YLabel');set(ylabh,'Position',get(ylabh,'Position') - [0.08 0 0]);
set(gca,'FontSize',14);
caxis([.95,1.51]);

fig2=figure(2); clf;
set(fig2,'pos',[0 0 1000 500]);
ha2=tight_subplot(2,nCOL,[.135 .05],[.09,.06],[.05,.07]);

% estimate permeability
% load data
files = dir('./summary');
nfiles = length(files) - 2;

for i=1:Nalg
    found=0;
    for j=1:nfiles
        if ~isempty(strfind(files(j+2).name,['_',alg{i},'_D',num2str(D),'_']))
            load(strcat('./summary/', files(j+2).name));
            found=1;
        end
    end
    if found
        % mesh
        PDE4inf=data.PDE4inf;
        p=PDE4inf.p;e=PDE4inf.e;t=PDE4inf.t;
        eigv=PDE4inf.eigv;eigf=PDE4inf.eigf;
        % estimate permeability
%         theta_est=mean(samp)';
%         c=coeff(p,t,[],[],theta_est,s);
        Nsamp=size(samp,1); Nt = size(t,2);
        u=zeros(Nsamp,Nt);
        for n=1:Nsamp
            u(n,:)=coeff(p,t,[],[],samp(n,:)',eigv,eigf);
        end
        u_mean=mean(u); u_var=var(u);
        % plot
        set(0,'currentfigure',fig);
        h_sub=subplot(ha(1+i));
        pdeplot(p,e,t,'xydata',u_mean,'contour','on','colorbar','off');
        title(alg_name{i},'fontsize',15,'interpreter','latex');
        xlabel('x_1','fontsize',15);ylabel('x_2','fontsize',15,'rot',0);
        ylabh=get(gca,'YLabel');set(ylabh,'Position',get(ylabh,'Position') - [0.08 0 0]);
        set(gca,'FontSize',14);
        caxis([.95,1.51]);
        set(0,'currentfigure',fig2);
        h2_sub=subplot(ha2(1+i));
        pdeplot(p,e,t,'xydata',u_var,'contour','on','colorbar','off');
        title(alg_name{i},'fontsize',15,'interpreter','latex');
        xlabel('x_1','fontsize',15);ylabel('x_2','fontsize',15,'rot',0);
        ylabh=get(gca,'YLabel');set(ylabh,'Position',get(ylabh,'Position') - [0.08 0 0]);
        set(gca,'FontSize',14);
        caxis([0,.21]);
    end
end
set(0,'currentfigure',fig);
% set same color range
h_pos=h_sub.Position;
colorbar('position',[sum(h_pos([1,3]))+.02 h_pos(2) 0.02 h_pos(4)*2.39],'linewidth',.1);
% caxis([0,8.5]);
% h = suptitle('Estimate of Mean');
% set(h,'FontSize',18,'FontWeight','normal');
set(0,'currentfigure',fig2);
% set same color range
h2_pos=h2_sub.Position;
colorbar('position',[sum(h2_pos([1,3]))+.02 h2_pos(2) 0.02 h2_pos(4)*2.39],'linewidth',.1);
% caxis([0,8.5]);
% h = suptitle('Estimate of Variance');
% set(h,'FontSize',18,'FontWeight','normal');

% save to file
fig.PaperPositionMode = 'auto';
print(fig,'./summary/truth_est','-dpng','-r0');
fig2.PaperPositionMode = 'auto';
print(fig2,'./summary/est_var','-dpng','-r0');