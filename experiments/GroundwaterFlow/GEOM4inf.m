% Geometric quantities includeing nll,g,FI_t,cholG_t %
function [nll,g,FI_t,cholG_t] = GEOM4inf(theta,PDE,ind_trct,obj,geom_opt,trct_opt)
if nargin<5
    geom_opt=0; trct_opt=[0,0,0];
elseif nargin<6
    trct_opt=[0,0,0];
end
nll=[]; g=[]; FI_t=[]; cholG_t=[];

% forward model to solve elliptic PDE given (truncated) theta
[~,nll,dnll,~,FI_t]=ePDEsol_adj_trct(theta,PDE,ind_trct,obj,geom_opt,trct_opt);

% dimension and truncation number
D=length(theta);
if isempty(ind_trct)
    ind_trct=true(D,1);
end

if nargout>1&&any(geom_opt>0)
    
    g = sparse(D,1);
    if any(geom_opt==2)
        g(ind_trct) = FI_t*theta(ind_trct);
        if nargout>3
            cholG_t = chol(FI_t+speye(size(FI_t)));
        end
    end
    
    if trct_opt(2) % dnll of size ntrct
        g(ind_trct) = g(ind_trct) - dnll;
    else % dnll full size D
        g = g - dnll;
    end
    
end

end