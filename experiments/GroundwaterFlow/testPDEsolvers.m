%%%% test PDE solvers %%%%
clear;

PLOT=0;

% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

%%%% generate data %%%%
% mesh settings
meshsz = 50; s=1.5; ncmp = 15; D = ncmp^2;
theta_truth = randn(D,1);
% prepare PDE solver
PDE=ePDEsetup(meshsz,D,'PCA');
u=reshape(ePDEsol_adj(theta_truth,PDE),meshsz+1,meshsz+1);
% model parameters
sigma2y=.1^2; %sigma2theta=1;
% observations
obs_sz=10;
% obs_idx=1+meshsz/obs_sz.*(0:obs_sz);
obs_idx=linspace(1,meshsz+1,obs_sz+1);
O=sparse(meshsz+1,meshsz+1); O(obs_idx,obs_idx)=1; % observation operator
seq=linspace(0,1,obs_sz+1);
[sx,sy]=meshgrid(seq);
x=[sx(:),sy(:)];
u=u(obs_idx,obs_idx);
y=u(:)+sqrt(sigma2y).*randn((1+obs_sz)^2,1);
% set up objective
obj=misfit(x,y,sigma2y,O);

%%%% Gradient dJ/dtheta and Fisher %%%%

% by finite difference %
pobj_fd=zeros(D,1);
eps=1e-3;
for i=1:D
    theta_p=theta_truth;theta_m=theta_truth;
    theta_p(i)=theta_p(i)+eps;theta_m(i)=theta_m(i)-eps;
    [~,obju_p]=ePDEsol_adj(theta_p,PDE,obj);
    [~,obju_m]=ePDEsol_adj(theta_m,PDE,obj);
    pobj_fd(i)=(obju_p-obju_m)/(2*eps);
end

tic;
% by sensitivity approach %
[u,pu,p2u]=ePDEsol_sen(theta_truth,PDE,[0,1],PLOT);
[~,f]=f_adj([],[],u,[],obj);
pobj_sen=pu'*f;
Opu=bsxfun(@times,O(:),pu);
F_sen=Opu'*Opu./sigma2y;
time_sen=toc;

tic;
% by adjoint method %
[u,obju,pobj_adj,~,F_adj]=ePDEsol_adj(theta_truth,PDE,obj,[0,1,2],PLOT);
time_adj=toc;

%%% Comparison %%%

% compare
fprintf('\nInf-norm difference of gradients between sensitivity and FD: %.8e \n',max(abs(pobj_sen-pobj_fd)));
fprintf('Inf-norm difference of gradients between adjoint and FD: %.8e \n\n',max(abs(pobj_adj-pobj_fd)));

fprintf('Scaled 2-norm difference of gradients between sensitivity and FD: %.8e \n',norm(pobj_sen-pobj_fd)/meshsz);
fprintf('Scaled 2-norm difference of gradients between adjoint and FD: %.8e \n\n',norm(pobj_adj-pobj_fd)/meshsz);


fprintf('\nInf-norm difference of Fisher between sensitivity and adjoint: %.8e \n',max(abs(F_sen(:)-F_adj(:))));

fprintf('Scaled 2-norm difference of Fisher between sensitivity and adjoint: %.8e \n\n',norm(F_sen-F_adj)/meshsz);

fprintf('\nTime is %.4f seconds for sensitivity and %.4f seconds for adjoint, %.2f faster \n\n',time_sen,time_adj,time_sen/time_adj);

% plot
if PLOT
    fig10=figure(10); set(fig10,'pos',[0 800 1000 400]); clf;
    % gradient by FD
    subplot(1,3,1,'position',[.06,.12,.26,.8]);
    imagesc(reshape(pobj_fd,ncmp,ncmp));
    title('Finte Difference','FontSize',20);
    xlabel('d_1','fontsize',19);ylabel('d_2','fontsize',19,'rot',0);
    set(gca,'Ydir','normal');
    set(gca,'FontSize',15);
    % gradient by sensitivity
    subplot(1,3,2,'position',[.38,.12,.26,.8]);
    imagesc(reshape(pobj_sen,ncmp,ncmp));
    title('Sensitivity Approach','FontSize',20);
    xlabel('d_1','fontsize',19);ylabel('d_2','fontsize',19,'rot',0);
    set(gca,'Ydir','normal');
    set(gca,'FontSize',15);
    % gradient by adjoint
    subplot(1,3,3,'position',[.70,.12,.26,.8]);
    imagesc(reshape(pobj_adj,ncmp,ncmp));
    title('Adjoint Solver','FontSize',20);
    xlabel('d_1','fontsize',19);ylabel('d_2','fontsize',19,'rot',0);
    set(gca,'Ydir','normal');
    set(gca,'FontSize',15);
end