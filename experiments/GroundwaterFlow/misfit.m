%%%% objective of inverse problem %%%%
%%%% misfit of PDE model to data %%%%
classdef misfit
    % data of obj
    properties
        x; % positions of observations (N,D)
        O=[]; % observation operator (meshsz,meshsz)
        y; % observations (N,1)
        sigma2y; % variance of noise
    end
    % evaluation
    methods
        % constructor
        function obj=misfit(x,y,sigma2y,O)
            obj.x=x;
            if nargin==4
                obj.O=O;
            end
            obj.y=y;
            if size(x,1)~=length(y)
                error('Size of x (1st dim) must match the number of observations y!');
            end
            obj.sigma2y=sigma2y;
        end
        % misfit function
        function J=eval(obj,u_x)
            if numel(u_x)==numel(obj.O)
%                 [I,J] = find(obj.O);
%                 idx = sub2ind(size(obj.O),I,J);
%                 u_x = u_x(idx);
                u_x = u_x(logical(obj.O(:)));
            end
            if length(u_x)==length(obj.y)
                J=sum((obj.y-u_x).^2)/(2*obj.sigma2y);
            else
                error('Length of interpolations must match length of observations!');
            end
            
        end
    end
end