function [ESS,ACT,Times,Acpts,Nsamp] = CalculateStatistics(filabl,resloc,PLOT)
if nargin<3
    PLOT=0;
end

styles = {'-','-.','--','-','--',':','--'};
if PLOT
    fig1=figure(1); clf;
    set(fig1,'pos',[0 800 800 400]);
end

% Get stats
Files = dir([resloc,'*' filabl '*.mat']);

for i = 1:length(Files)

    Data = open(Files(i).name);
    
    ESS.raw{i} = CalculateESS(Data.samp, size(Data.samp,1)-1);
    
    ESS.min(i) = min(ESS.raw{i});
    ESS.max(i) = max(ESS.raw{i});
    ESS.med(i) = median(ESS.raw{i});
    ESS.mean(i) = mean(ESS.raw{i});
    
    ACT.raw{i} = CalculateACT(Data.samp);
    ACT.min(i) = min(ACT.raw{i});
    ACT.max(i) = max(ACT.raw{i});
    ACT.med(i) = median(ACT.raw{i});
    ACT.mean(i) = mean(ACT.raw{i});
    
    Times(i) = Data.time;
    Acpts(i) = Data.acpt(1);
    
    if PLOT
        subplot(1,2,1);
        plot(ESS.raw{i},styles{i},'linewidth',1.5); hold on;
        set(gca,'FontSize',15);
        xlabel('parameters','FontSize',19); ylabel('ESS','FontSize',19); 
        subplot(1,2,2);
        plot(ACT.raw{i},styles{i},'linewidth',1.5); hold on;
        set(gca,'FontSize',15);
        xlabel('parameters','FontSize',19); ylabel('ACT','FontSize',19);
    end
    
end
if PLOT
    hold off;
end

Nsamp=size(Data.samp,1);

disp(['Time:   ' num2str(mean(Times)) ' +/- ' num2str(std(Times)/sqrt(length(Times)))])
disp(' ')

disp(['ESS for ' filabl ' dataset.'])
disp(' ')

disp(['Min:    ' num2str(mean(ESS.min)) ' +/- ' num2str(std(ESS.min)/sqrt(length(ESS.min)))])
disp(['Median: ' num2str(mean(ESS.med)) ' +/- ' num2str(std(ESS.med)/sqrt(length(ESS.med)))])
disp(['Mean:   ' num2str(mean(ESS.mean)) ' +/- ' num2str(std(ESS.mean)/sqrt(length(ESS.mean)))])
disp(['Max:    ' num2str(mean(ESS.max)) ' +/- ' num2str(std(ESS.max)/sqrt(length(ESS.max)))])

disp('')
disp(['Min ESS per seconds: ' num2str(mean(ESS.min)/mean(Times))])
disp(' ')


disp(['ACT for ' filabl ' dataset.'])
disp(' ')

disp(['Min:    ' num2str(mean(ACT.min)) ' +/- ' num2str(std(ACT.min)/sqrt(length(ACT.min)))])
disp(['Median: ' num2str(mean(ACT.med)) ' +/- ' num2str(std(ACT.med)/sqrt(length(ACT.med)))])
disp(['Mean:   ' num2str(mean(ACT.mean)) ' +/- ' num2str(std(ACT.mean)/sqrt(length(ACT.mean)))])
disp(['Max:    ' num2str(mean(ACT.max)) ' +/- ' num2str(std(ACT.max)/sqrt(length(ACT.max)))])

disp('')
disp(['Max ACT multiply time per iteration: ' num2str(mean(ACT.max)*mean(Times)/size(Data.samp,1))])

end
