% compare covariance functions in K-L expansion specified by Fourier bases
% vs discreteGP-then-PCA, noting to do with observations

clear;
addpath('~/Documents/MATLAB/tight_subplot/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

% basis tpye
basis_types={'Fourier','GP-PCA'};
L_type=length(basis_types);

% mesh setting
Nmesh=20;
ncmp = 10; D = ncmp^2;
% Fouerier bases parameters
s=1.1;
% discreteGP-then-PCA parameters
sigma2=1; l=.1; ex=2;

% covariances %
% PDE solver by basis type 1
PDE4=ePDEsetup(Nmesh,D,basis_types{1},s);
eigenV=PDE4.eigv(:);
COV{1}=(2.*PDE4.eigf(:,:))*diag(eigenV.^2)*(2.*PDE4.eigf(:,:))';
% PDE solver by basis type 2
PDE4=ePDEsetup(Nmesh,D,basis_types{2},sigma2,l,ex);
eigenV=[eigenV,PDE4.eigv];
COV{2}=PDE4.eigf*diag(PDE4.eigv.^2)*PDE4.eigf';

% show on the geometry plot
fig1=figure(1); clf;
set(fig1,'pos',[0 800 800 350]);
h_t=tight_subplot(1,L_type+1,[.1,.05],[.12,.12],[.05,.1]);
subplot(h_t(1));
plot(eigenV); ylim([-.2,max(eigenV(:))+.2]);
refline(0,0);
legend(basis_types);
title('Root Eigen-Values');
for i=1:L_type
    h_sub=subplot(h_t(i+1));
    imagesc(COV{i});
    title(['Covariance of ',basis_types{i}]);
    set(gca,'Ydir','normal');
end
colorbar('position',[sum(h_sub.Position([1,3]))+.02 h_sub.Position(2) 0.02 h_sub.Position(4)]);
caxis([0,1]);

% save to file
% print(fig2,'covariances','-dpng');