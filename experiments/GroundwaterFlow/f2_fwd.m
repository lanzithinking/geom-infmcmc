%%%% right hand side f in 2nd order forward equation (?) %%%%

function [ft,fn]=f2_fwd(p,t,u,time,cddeltau,v)

ft=[];fn=[];

%%% du %%%
du=sparse(size(t,2),2);
[du(:,1),du(:,2)]=pdegrad(p,t,u);

ft=sum(-cddeltau(v).*du,2); % f at triangle midpoints
if nargout==2
    fn=pdeprtni(p,t,ft); % f at nodes
end

end