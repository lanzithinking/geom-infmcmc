%%% This is to plot acf of misfits in groundwater problem. %%%

clear;
addpath('~/Documents/MATLAB/tight_subplot/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

% algorithms
alg={'pCN','infMALA','infHMC','infmMALA','infmHMC','splitinfmMALA','splitinfmHMC'};
alg_name={'pCN','$$\infty$$-MALA','$$\infty$$-HMC','$$\infty$$-mMALA','$$\infty$$-mHMC','split$$\infty$$-mMALA','split$$\infty$$-mHMC'};
Nalg=length(alg);

D=100;

% plot specifications
% colors = distinguishable_colors(Nalg);
styles = {'-','-.','--','-','--',':','--'};

% set plot
fig=figure(1); clf;
set(fig,'pos',[0 800 900 450]);
% fig=figure('visible','off');
ha=tight_subplot(1,2,[0 0.07],[.1,.11],[.07,.05]);


% gather misfits by different algorithms
spiter=zeros(Nalg,1); % time per iteration
% load data
files = dir('./summary');
nfiles = length(files) - 2;

for i=1:Nalg
    found=0;
    for j=1:nfiles
        if ~isempty(strfind(files(j+2).name,['_',alg{i},'_D',num2str(D),'_']))
            load(strcat('./summary/', files(j+2).name));
            found=1;
        end
    end
    if found
        % create misfit
        if ~exist('misfit','var')
            misfit=zeros(length(loglik),Nalg);
        end
        misfit(:,i)=-loglik;
        spiter(i)=time/(setting.Nsamp-setting.NBurnIn);
    end
end

% plot misfits
h1_sub=subplot(ha(1));
% modify misfits to discern their traceplots
for i=1:Nalg
    misfit(:,i) = misfit(:,i) - (i-1)*10;
end
% set(groot,'DefaultAxesLineStyleOrder',strjoin(styles,'|'));
% plot(misfit(setting.NBurnIn+1:end,:),'linewidth',1.5);
% colors = get(gca,'ColorOrder');
for i=1:Nalg
    plot(misfit(setting.NBurnIn+1:end,i), styles{i},'linewidth',1.5); hold on;
end
drawnow;
set(gca,'FontSize',14);
xlabel('Iterations','FontSize',15); ylabel('Data  Misfit (offset)','FontSize',15);
ylabh=get(gca,'YLabel');set(ylabh,'Position',get(ylabh,'Position') - [0.02 0 0]);

% plot acf's of misifits
h2_sub=subplot(ha(2));
for i=1:Nalg
    [acf,lags,bounds] = autocorr(misfit(setting.NBurnIn+1:end,i),50);
    plot(lags,acf,styles{i},'linewidth',1.5); hold on;
end
drawnow;
% set(gca,'XTick',1:max_time);
set(gca,'FontSize',14);
xlabel('Lags','FontSize',15); ylabel('Auto-correlation','FontSize',15);
ylabh=get(gca,'YLabel');set(ylabh,'Position',get(ylabh,'Position') - [0.02 0 0]);

lgnd=legend(alg_name,'FontSize',14,'interpreter','latex','location','NorthOutside','Orientation','horizontal');
h1_pos=h1_sub.Position;
set(lgnd,'position',[h1_pos(1)-.4 sum(h1_pos([2,4]))+.01 h1_pos(3)*4 0.1]);

% save to file
fig.PaperPositionMode = 'auto';
print(fig,'./summary/misfit_acf','-dpng','-r0');