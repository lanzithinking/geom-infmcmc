% This is to solve a classic example of elliptic equation given mesh and
% boundary conditions and get the derivatives of solution wrt some design
% parameter theta using sensitivity approach.
% solution: u; derivative: pu, p2u satisfying
% d(cdu)=0;       % -d(cdpu)=d(pcdu);   % -d(cdp2u)=d(p2cdu)+d(pcdpu)+d(dpupc);
% u(x,0)=x;       % pu(x,0)=0;          % p2u(x,0)=0;
% u(x,1)=1-x;     % pu(x,1)=0;          % p2u(x,1)=0;
% d_x u(0,y)=0;   % d_x pu(0,y)=0;      % d_x p2u(0,y)=0;
% d_x u(1,y)=0;   % d_x pu(1,y)=0;      % d_x p2u(1,y)=0;
% output: [u,p_theta u,p_ij u];

function [u,pu,p2u]=ePDEsol_sen(theta,PDE,opt,PLOT,PRINT)
if nargin<3
    opt=0; PLOT=0; PRINT=0;
elseif nargin<4
    PLOT=0; PRINT=0;
elseif nargin<5
    PRINT=0;
end
u=[]; pu=[]; p2u=[];
% prepare to solve PDE
p=PDE.p;e=PDE.e;t=PDE.t; % mesh
b=PDE.b; pb=PDE.db; % problems (with boudary conditions)
eigv=PDE.eigv; eigf=PDE.eigf;

% dimensions
Np=size(p,2); D=length(theta);

% specify coefficients
if all(opt==0)
    c=coeff([],[],[],[],theta,eigv,eigf);
elseif all(ismember(opt,[0,1]))
    [c,pc]=coeff([],[],[],[],theta,eigv,eigf);
elseif all(ismember(opt,[0,1,2]))
    [c,pc,p2c]=coeff([],[],[],[],theta,eigv,eigf);
end

if any(ismember([0,1,2],opt))
    % solution of u
    [K,M,F,Q,G,H,R] = assempde(b,p,e,t,-c',0,0);
    u = assempde(K,M,F,Q,G,H,R);
    % plot the solution
    if PLOT
        fig1=figure(1); clf;
        pdesurf(p,t,u);
    end
    % print the solution
    if PRINT
        print(fig1,'-dpng','u');
    end
end
if any(ismember([1,2],opt))
    [Q,G,H,R] = assemb(pb,p,e); % assemble boundary condition for pu, p2u
end
if any(ismember([1,2],opt))
    pu=zeros(Np,D);
    if PLOT
        fig2=figure(2); clf;
        Ncol=min([ceil(sqrt(D)),5]);
        Nrow=min([ceil(D/Ncol),5]);
    end
    K1=cell(1,D);F1=cell(1,D);
    for i=1:D
        % assembling matrices on RHS
        [K1{i},F1{i}] = assempde(pb,p,e,t,-pc(:,i)',0,0);
        % solution of p_iu
        p_iu = assempde(K,0,-K1{i}*u+F1{i},Q,G,H,R);
        pu(:,i) = p_iu;
        % plot the solutions
        if PLOT&&i<=Nrow*Ncol
            set(0,'CurrentFigure',fig2);
            subplot(Nrow,Ncol,i);
            pdesurf(p,t,p_iu);
        end
    end
   % print the solution
    if PRINT
        print(fig2,'-dpng','pu');
    end
end
if any(opt==2)
    p2u=zeros(Np,D,D);
    if PLOT
        fig3=figure(3); clf;
        plotdim=min([D,5]);
    end
    for i=1:D
        % solve p_iju
        for j=1:i
            % assembling matrices on RHS
            [K_ij,F_ij] = assempde(pb,p,e,t,-p2c(:,i,j)',0,0);
            p_iju = assempde(K,0,-K_ij*u+F_ij-K1{i}*pu(:,j)+F1{i}-K1{j}*pu(:,i)+F1{j},Q,G,H,R);
            p2u(:,i,j) = p_iju; p2u(:,j,i) = p_iju;
            if PLOT&&i<=plotdim
                set(0,'CurrentFigure',fig3);
                subplot(plotdim,plotdim,sub2ind([plotdim,plotdim],j,i));
                pdesurf(p,t,p_iju);
            end
        end
    end
    % print the solution
    if PRINT
        print(fig3,'-dpng','p2u');
    end
end

end