%%%% coefficient function of Elliptic PDE %%%%

function [c,pc,p2c]=coeff(p,t,u,time,theta,varargin)
narginchk(5,9);
if isempty(varargin)
    varargin={1,'Fourier',1,1.1};
elseif length(varargin)==2
    eigv=varargin{1};eigf=varargin{2};
else
    error('Please specify prior!');
end
c=[]; pc=[]; p2c=[];

% use dct2: more error introduced due to interpolation
% A=meshsz/2.*eigv.*reshape(theta,size(eigv)); A(:,1)=2.*A(:,1); A(1,2:end)=2.*A(1,2:end);
% logc=dct2(A,meshsz,meshsz);
% logc=pdeintrp(p,t,logc(:));

% dimension
D = length(theta);

if length(varargin)>2
    % prepare for the PDE coefficients
    Nt = size(t,2);
    % Triangle point indices
    it1 = t(1,:);
    it2 = t(2,:);
    it3 = t(3,:);
    % Find centroids of triangles
    centroid = (p(:,it1)+p(:,it2)+p(:,it3))/3;
    
    sigma=varargin{1}; basis_type=varargin{2};
    % eigen-pairs of covariance kernel
    switch basis_type
        case 'Fourier'
            ncmp = sqrt(D); alpha=varargin{3};s=varargin{4};
            eigv = sigma.*(alpha+pi^2.*bsxfun(@(x,y)(x.^2+y.^2),(.5:ncmp-.5)',.5:ncmp-.5)).^(-s/2);
            cosix = cos(pi.*centroid(1,:)'*(.5:ncmp-.5)); cosiy = cos(pi.*centroid(2,:)'*(.5:ncmp-.5));
            eigf = bsxfun(@times,cosix,reshape(cosiy,Nt,1,ncmp));
        case 'PCA'
            l=varargin{3};ex=varargin{4};
            C = sigma.*exp(-pdist2(centroid',centroid').^ex/(2*l^ex));
            % number of components in PCA
            K=min([D,Nt]);
            [evc,ev] = eigs(C,K,'LA'); % there are some approximations behind it, I guess, check C*evc./evc! but much faster
            eigv = sqrt(diag(ev)); eigf = evc; % ok under fixed random seed
        otherwise
            error('Wrong option!');
    end
end

plogc = bsxfun(@times,eigv(:)',eigf(:,:)); % (Nt,D)
logc = plogc*theta;
c = exp(logc); % (Nt,1)

if nargout>1
    pc = bsxfun(@times,c,plogc); % (Nt,D)
end
if nargout>2
    p2c = bsxfun(@times,pc,reshape(plogc,[],1,D)); % (Nt,D,D)
end

end