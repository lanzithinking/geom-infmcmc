% log posterior of inverse elliptic PDE model %
function logpost = logPosterior(theta,PDE,obj)

sz=size(theta);
nll=zeros(sz(1),1);
% forward model to solve elliptic PDE given theta
for n=1:sz(1)
    [~,nll(n)]=ePDEsol_adj(theta(n,:)',PDE,obj);
end

loglik=-nll;
logpri=-sum(theta.^2,2)./2;

logpost=loglik+logpri;

end