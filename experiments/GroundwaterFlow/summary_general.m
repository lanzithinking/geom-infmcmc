% this is to summarize the numerical results (e.g. sampling efficiency) of
% different algorithms.

addpath('./result/');
addpath('./summary/');
% algorithms
alg={'pCN','infMALA','HHMC','infMMALA','splitinfMMALA','splitinfmHMC'};
alg_name={'pCN','$\infty$-MALA','H-HMC','$\infty$-MMALA','split $\infty$-MMALA','split $\infty$-mHMC'}';
Nalg=length(alg);
dim=100;
Ndim=length(dim);
dim_name=repmat({'NA'},Nalg*Ndim,1);
for i=1:Ndim
    dim_name{ceil(Nalg/2)+(i-1).*Nalg}=['D=',num2str(dim(i))];
end
% store measuring statistics
AP=zeros(Nalg,Ndim);
spiter=zeros(Nalg,Ndim);
Eff=zeros(Nalg,Ndim,3);
MinEffs=zeros(Nalg,Ndim);
spdup=zeros(Nalg,Ndim);

% compute satstistics
for i=1:Nalg
    for j=1:Ndim
        try
            [ESS,~,Times,Acpts,Nsamp] = CalculateStatistics(['_',alg{i},'_D',num2str(dim(j)),'_'],'./summary/');
            Eff(i,j,:) = mean([ESS.min',ESS.med',ESS.max'],1);
        catch
            Eff(i,j,:)=zeros; Times=zeros; Acpts=zeros;
        end
        AP(i,j) = mean(Acpts);
        spiter(i,j) = mean(Times);
    end
    MinEffs(i,:) = squeeze(Eff(i,:,1))./spiter(i,:);
    if i==1
        spdup(i,:) = 1;
    else
        spdup(i,:) = MinEffs(i,:)./MinEffs(1,:);
    end
end
spiter=spiter./Nsamp;

T = table(dim_name,repmat(alg_name,Ndim,1),AP(:),spiter(:),reshape(Eff,[],3),MinEffs(:),spdup(:));
writetable(T,'./summary/summary.txt','WriteRowNames',true);

