%%% This is to plot some posteriors of conductivity (in physical space). %%%

clear;
addpath('../','~/Documents/MATLAB/tight_subplot/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

% algorithms
alg={'pCN','infMALA','HHMC','infMMALA','splitinfMMALA','splitinfmHMC'};
alg_name={'pCN','$$\infty$$-MALA','H-HMC','$$\infty$$-MMALA','split$$\infty$$-MMALA','split$$\infty$$-mHMC'};
Nalg=length(alg);

% dimension setting
ncmp = 10; D = ncmp^2;

% set plot
fig1=figure(1); clf;
% set(fig1,'windowstyle','docked');
% set(fig1,'pos',[0 800 700 450]);
% fig=figure('visible','off');
% ha1=tight_subplot(2,2,[.16 0.11],[.1,.09],[.1,.05]);

% load data
files = dir('./summary');
nfiles = length(files) - 2;

analyzeNO=6;
dim=[1,2,floor(D/4),floor(D/2),D-1,D];

for i=analyzeNO
    for j=1:nfiles
        if ~isempty(strfind(files(j+2).name,['_',alg{i},'_D',num2str(D),'_']))
            load(strcat('./summary/', files(j+2).name));
        end
    end
    contourmatrix(samp,dim,10);
end

% save to file
% print(fig,'-dpng','./figure/somePosteriors');