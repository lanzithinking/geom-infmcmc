clear;

% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

% mesh setting
Nmesh=40; % for data generating
ncmp = 10; D = ncmp^2;

% basis setting
basis_types={'Fourier','PCA'};
basis_type=basis_types{1};
% scale parameter for prior
sigma=1;
% Fouerier basis parameters
alpha=0; s=1.1;
% PCA parameters
l=.2; ex=2;
% observation setting
obs_types={'uniform','circle'};
obs_type=obs_types{2}; Ncirc=1;
% standaard deviation of noise in observation
sigma_y=1e-2;

% generate data
% theta_truth = randn(D,1);
theta_truth = sin(bsxfun(@(x,y)x.^2+y.^2,(0.5:ncmp-.5)',0.5:ncmp-.5));
% prepare PDE solver for observation
switch basis_type
    case basis_types{1}
        PDE4obs=ePDEsetup(Nmesh,D,basis_type,sigma,alpha,s);
    case basis_types{2}
        PDE4obs=ePDEsetup(Nmesh,D,basis_type,sigma,l,ex);
    otherwise
        error('wrong choice!');
end
u=ePDEsol_adj(theta_truth(:),PDE4obs);
% observations
switch obs_type
    case 'uniform'
        obs_sz=5;
        obs_idx=ceil(linspace(1,Nmesh+1,obs_sz+1));
        O=logical(sparse(Nmesh+1,Nmesh+1)); O(obs_idx,obs_idx)=1; % observation operator
        x=PDE4obs.p(:,O(:))';
    case 'circle'
        p=PDE4obs.p;
        obs_ind=false(1,size(p,2));
        for i=1:Ncirc
            obs_ind=obs_ind|abs(sum((p-.5).^2)-(2^(-i))^2)<1e-2; % on circle(s)
        end
        x=[p(:,obs_ind)';.5,.5];
        x=unique(x,'rows');
    otherwise
        error('Wrong option!');
end
% show on the geometry plot
fig1=figure(1); clf;
set(fig1,'pos',[0 800 800 350]);
subplot(1,2,1);
pdeplot(PDE4obs.p,PDE4obs.e,PDE4obs.t);hold on;plot(x(:,1),x(:,2),'ko','markersize',8);hold off;
% observation operator
O=sparse(pdist2(x,PDE4obs.p','chebychev','smallest',1)<1e-7)';
u=u(O);
y=u(:)+sigma_y.*randn(length(u),1);
% set up objective
obj=misfit(x,y,sigma_y^2,O);

% prepare PDE solver for inference
meshsz=20; % mesh size for inference
switch basis_type
    case basis_types{1}
        PDE4inf=ePDEsetup(meshsz,D,basis_type,sigma,alpha,s);
    case basis_types{2}
        PDE4inf=ePDEsetup(meshsz,D,basis_type,sigma,l,ex);
    otherwise
        error('wrong choice!');
end
% update objective is the mesh size changes
if meshsz~=Nmesh
    obj.O=sparse(pdist2(obj.x,PDE4inf.p','chebychev','smallest',1)<1e-7)';
    x=PDE4inf.p(:,obj.O)';
    subplot(1,2,2);
    pdeplot(PDE4inf.p,PDE4inf.e,PDE4inf.t);hold on;plot(x(:,1),x(:,2),'ko','markersize',8);hold off;
    obj.y=obj.y(ismember(obj.x,x,'rows'));
    obj.x=x;
end

% plot pairwise posterior density
cmp=[1,1;2,1];
dim=[sub2ind([ncmp,ncmp],cmp(1,1),cmp(1,2)),sub2ind([ncmp,ncmp],cmp(2,1),cmp(2,2))];
fig2=figure(2); clf;
set(fig2,'pos',[0 150 800 350]);
subplot(1,2,1);
ezsurf(@(x,y)logPosterior(bsxfun(@plus,x*(dim(1)==(1:D))+y*(dim(2)==(1:D)),theta_truth(:)'.*(dim(1)~=(1:D)&dim(2)~=(1:D))),PDE4inf,obj),[-5,5,-5,5]);
xlabel(['d_{',num2str(dim(1)),'}']); ylabel(['d_{',num2str(dim(2)),'}']);
title('log Posterior Surface');
subplot(1,2,2);
ezcontour(@(x,y)logPosterior(bsxfun(@plus,x*(dim(1)==(1:D))+y*(dim(2)==(1:D)),theta_truth(:)'.*(dim(1)~=(1:D)&dim(2)~=(1:D))),PDE4inf,obj),[-5,5,-5,5]);
xlabel(['d_{',num2str(dim(1)),'}']); ylabel(['d_{',num2str(dim(2)),'}']);
title('log Posterior Contour');
colormap(jet);
% save to file
% print(fig2,['logPosteriorDensity_',obs_type],'-dpng');