%%%% This is the main script to sample from posterior of parameters in Elliptic PDE to compare algorithms %%%%
clear;
addpath('../','../../sampler/');
% Random Numbers
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

% MCMC algorithms
MCMC = {'pCN','infMALA','infHMC','infmMALA','infmHMC','splitinfmMALA','splitinfmHMC'};
L_MCMC = length(MCMC);

%% settings

% mesh setting
Nmesh=40; % for data generating
ncmp = 10; D = ncmp^2;

% basis setting
basis_types={'Fourier','PCA'};
basis_type=basis_types{1};
% scale parameter for prior
sigma=1;
% Fouerier basis parameters
alpha=0; s=1.1;
% PCA parameters
l=.1; ex=2;
% observation setting
obs_types={'uniform','circle'};
obs_type=obs_types{2};
% standaard deviation of noise in observation
sigma_y=1e-2;


%% load data
try
    load(['ellipticPDE_Nmesh',num2str(Nmesh),'_D',num2str(D),'_',basis_type,'_priorsd',num2str(sigma),'_',obs_type,'_noisesd',num2str(sigma_y),'.mat']);
catch
    % generate data
    switch basis_type
        case basis_types{1}
            obj=generateData(Nmesh,D,basis_type,sigma,obs_type,sigma_y,0,1,alpha,s);
        case basis_types{2}
            obj=generateData(Nmesh,D,basis_type,sigma,obs_type,sigma_y,0,1,l,ex);
        otherwise
            error('wrong choice!');
    end
    load(['ellipticPDE_Nmesh',num2str(Nmesh),'_D',num2str(D),'_',basis_type,'_priorsd',num2str(sigma),'_',obs_type,'_noisesd',num2str(sigma_y),'.mat']);
end

% prepare PDE solver for inference
meshsz=20; % mesh size for inference
switch basis_type
    case basis_types{1}
        PDE4inf=ePDEsetup(meshsz,D,basis_type,sigma,alpha,s);
    case basis_types{2}
        PDE4inf=ePDEsetup(meshsz,D,basis_type,sigma,l,ex);
    otherwise
        error('wrong choice!');
end
% update objective is the mesh size changes
if meshsz~=Nmesh
    obj.O=sparse(pdist2(obj.x,PDE4inf.p','chebychev','smallest',1)<1e-7)';
    x=PDE4inf.p(:,obj.O)';
%     pdeplot(PDE4inf.p,PDE4inf.e,PDE4inf.t);hold on;plot(x(:,1),x(:,2),'ko','markersize',8);hold off;
    obj.y=obj.y(ismember(obj.x,x,'rows'));
    obj.x=x;
end

% refine the mesh
meshsz=40; % mesh size for inference
switch basis_type
    case basis_types{1}
        PDE4inf=ePDEsetup(meshsz,D,basis_type,sigma,alpha,s);
    case basis_types{2}
        PDE4inf=ePDEsetup(meshsz,D,basis_type,sigma,l,ex);
    otherwise
        error('wrong choice!');
end
% update objective is the mesh size changes
% if meshsz~=Nmesh
    obj.O=sparse(pdist2(x,PDE4inf.p','chebychev','smallest',1)<1e-7)';
%     x=PDE4inf.p(:,obj.O)';
%     pdeplot(PDE4inf.p,PDE4inf.e,PDE4inf.t);hold on;plot(x(:,1),x(:,2),'ko','markersize',8);hold off;
    obj.y=obj.y(ismember(obj.x,x,'rows'));
    obj.x=x;
% end

% count times of solving PDE
global pde_sol_cnt

% choose algorithm
for alg_id=4:5

% initialize the count of PDE solving
pde_sol_cnt=zeros(4,1);

% options of geometric quantities required
geom_opt=[0,1*(alg_id>1),2*(alg_id>3)];

% options of truncation
ind_trct=[];trct_opt=zeros(1,3);
if strfind(MCMC{alg_id},'split')
    % truncation number
    rtrct=5; ntrct=rtrct^2;
    % index of truncation: indicator of being kept
    ind_trct=logical(sparse(ncmp,ncmp)); ind_trct(1:rtrct,1:rtrct)=1;
    % truncation option
    trct_opt=[0,0,1];
end

% save into data
data.obj=obj; data.PDE4inf=PDE4inf;
data.geom_opt=geom_opt;
data.ind_trct=ind_trct;data.trct_opt=trct_opt;

%% setting of sampling
stepsz=[.007,.03,.03,1,.9,.7,.6]; Nleap=ones(1,L_MCMC);
if strfind(MCMC{alg_id},'HMC')
    Nleap(alg_id)=4;
end
setting.stepsz=stepsz(alg_id); setting.Nleap=Nleap(alg_id);
Nsamp = 11000; NBurnIn = 1000;
setting.Nsamp=Nsamp; setting.NBurnIn=NBurnIn;
%% initialization
% theta = randn(D,1); % theta = theta_truth(:);
theta = zeros(D,1);
[nll,g,FI,cholG] = GEOM4inf(theta,PDE4inf,ind_trct,obj,geom_opt,trct_opt);

init.theta=theta; init.nll=nll; init.g=g;
init.FI=FI; init.cholG=cholG;

%% setting of saving
SAVE=1; PRINT=1; PLOT=0;
savepath=[pwd,'/result/'];
filename=['EllipticPDE_',char(MCMC(alg_id)),'_D',num2str(D),'_Nleap',num2str(Nleap(alg_id))];
if strfind(MCMC{alg_id},'split')
    filename=[filename,'_ntrct',num2str(ntrct)];
end

%% sampling
[samp,loglik,acpt,time]=sampling(data,char(MCMC(alg_id)),setting,init,PRINT);

%% save results
addpath('./result/');
curTime = regexprep(num2str(fix(clock)),'    ','_');
curfile=[filename,'__',curTime];
if SAVE
    stepsz=setting.stepsz; Nleap=setting.Nleap;
    save([savepath,curfile,'.mat'],'meshsz','data','setting','samp','loglik','acpt','time','pde_sol_cnt');
end
if PRINT
    CalculateStatistics(curfile,savepath);
end
if PLOT
    % some plots
    idx=floor(linspace(1,size(samp,1),min([1e4,size(samp,1)])));
    dim=[1,2,floor(D/2),D];
    fig=figure(1); set(fig,'pos',[0 800 900 300]);
    subplot(1,3,1);
    plot(samp(idx,dim));
    subplot(1,3,2);
    plotmatrix(samp(idx,dim));
    subplot(1,3,3);
    plot(loglik);
    % save to file
%     print(fig,['./figure/',curfile],'-dpng');
end

end
