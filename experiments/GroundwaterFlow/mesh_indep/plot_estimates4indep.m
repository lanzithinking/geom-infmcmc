%%% This is to plot estimated permeability of groundwater problem on different meshes. %%%

clear;
addpath('~/Documents/MATLAB/tight_subplot/');
addpath('../');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

% mode
alg='infmHMC';%'infmMALA'
mode=[3^2,4^2,5^2,100];
Nmode=length(mode);

% mesh setting
Nmesh=40; % for data generating
ncmp = 10; D = ncmp^2;
basis_type='Fourier';
sigma=1; %s=1.1;
obs_type='circle';
sigma_y=1e-2;


 % set plot for estimated permeability
fig=figure(1); clf;
set(fig,'pos',[0 800 1000 500]);
% fig=figure('visible','off');
nCOL=Nmode;
ha=tight_subplot(2,nCOL,[.135 .05],[.09,.06],[.05,.07]);

%-------- 20 x 20 --------%
files = dir('../summary');
nfiles = length(files) - 2;

for j=1:nfiles
    if ~isempty(strfind(files(j+2).name,['_',alg,'_D',num2str(D),'_']))
        load(strcat('../summary/', files(j+2).name));
        found=1;
    end
end
if found
    % mesh
    PDE4inf=data.PDE4inf;
    p=PDE4inf.p;e=PDE4inf.e;t=PDE4inf.t;
    eigv=PDE4inf.eigv;eigf=PDE4inf.eigf;
    % estimate permeability
%         theta_est=mean(samp)';
%         c=coeff(p,t,[],[],theta_est,s);
    Nsamp=size(samp,1); Nt = size(t,2);
    for i=1:Nmode
        u=zeros(Nsamp,Nt);
        for n=1:Nsamp
            samp_modei=zeros(D,1); samp_modei(1:mode(i))=samp(n,1:mode(i))';
            u(n,:)=coeff(p,t,[],[],samp_modei,eigv,eigf);
        end
        u_mean=mean(u);
        % plot
        h_sub=subplot(ha(i));
        pdeplot(p,e,t,'xydata',u_mean,'contour','on','colorbar','off');
        title([num2str(mode(i)),' modes'],'fontsize',15);
        xlabel('x_1','fontsize',15);ylabel('x_2','fontsize',15,'rot',0);
        ylabh=get(gca,'YLabel');set(ylabh,'Position',get(ylabh,'Position') - [0.08 0 0]);
        set(gca,'FontSize',14);
        caxis([.95,1.51]);
    end
end

%-------- 40 x 40 --------%
files = dir('./result');
nfiles = length(files) - 2;

for j=1:nfiles
    if ~isempty(strfind(files(j+2).name,['_',alg,'_D',num2str(D),'_']))
        load(strcat('./result/', files(j+2).name));
        found=1;
    end
end
if found
    % mesh
    PDE4inf=data.PDE4inf;
    p=PDE4inf.p;e=PDE4inf.e;t=PDE4inf.t;
    eigv=PDE4inf.eigv;eigf=PDE4inf.eigf;
    % estimate permeability
%         theta_est=mean(samp)';
%         c=coeff(p,t,[],[],theta_est,s);
    Nsamp=size(samp,1); Nt = size(t,2);
    for i=1:Nmode
        u=zeros(Nsamp,Nt);
        for n=1:Nsamp
            samp_modei=zeros(D,1); samp_modei(1:mode(i))=samp(n,1:mode(i))';
            u(n,:)=coeff(p,t,[],[],samp_modei,eigv,eigf);
        end
        u_mean=mean(u);
        % plot
        h_sub=subplot(ha(Nmode+i));
        pdeplot(p,e,t,'xydata',u_mean,'contour','on','colorbar','off');
        title([num2str(mode(i)),' modes'],'fontsize',15);
        xlabel('x_1','fontsize',15);ylabel('x_2','fontsize',15,'rot',0);
        ylabh=get(gca,'YLabel');set(ylabh,'Position',get(ylabh,'Position') - [0.08 0 0]);
        set(gca,'FontSize',14);
        caxis([.95,1.51]);
    end
end

% set same color range
h_pos=h_sub.Position;
colorbar('position',[sum(h_pos([1,3]))+.02 h_pos(2) 0.02 h_pos(4)*2.39],'linewidth',.1);
% caxis([0,8.5]);
% h = suptitle('Estimate of Mean');
% set(h,'FontSize',18,'FontWeight','normal');

% save to file
fig.PaperPositionMode = 'auto';
print(fig,'./result/est4indep','-dpng','-r0');