%%% This is to plot acf of samples in groundwater problem on different meshes. %%%

clear;
addpath('~/Documents/MATLAB/tight_subplot/');
addpath('../');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

% algorithm
alg={'infmMALA','infmHMC'};
alg_name={'$$\infty$$-mMALA','$$\infty$$-mHMC'};
Nalg=length(alg);
% chosen samples
samp_idx=[1,10^2];
Nidx=length(samp_idx);

D=100;

% plot specifications
% colors = distinguishable_colors(Nalg);
styles = {'-','-.',':','-','-.',':'};

% set plot
fig=figure(1); clf;
set(fig,'pos',[0 800 1000 400]);
% fig=figure('visible','off');
ha=tight_subplot(1,Nalg,[0 0.07],[.12,.08],[.07,.15]);


%-------- 20 x 20 --------%
files = dir('../summary');
nfiles = length(files) - 2;

for i=1:Nalg
    found=0;
    for j=1:nfiles
        if ~isempty(strfind(files(j+2).name,['_',alg{i},'_D',num2str(D),'_']))
            load(strcat('../summary/', files(j+2).name));
            found=1;
        end
    end
    if found
        % create list of samples
        if ~exist('samp2plot','var')
            samp2plot=zeros(size(samp,1),Nidx,Nalg);
        end
        samp2plot(:,:,i)=samp(:,samp_idx);
    end
end

% plot acf's of misifits
for i=1:Nalg
    h_sub=subplot(ha(i));
    for j=1:Nidx
        [acf,lags,bounds] = autocorr(samp2plot(:,j,i),20);
        plot(lags,acf,styles{j},'linewidth',2); hold on;
    end
    set(gca,'FontSize',15);
    title(alg_name(i),'fontsize',18,'interpreter','latex');
    xlabel('Lags','FontSize',18); ylabel('Auto-correlation','FontSize',18);
    ylabh=get(gca,'YLabel');set(ylabh,'Position',get(ylabh,'Position') - [0.02 0 0]);
end
% drawnow;

%-------- 40 x 40 --------%
files = dir('./result');
nfiles = length(files) - 2;

for i=1:Nalg
    found=0;
    for j=1:nfiles
        if ~isempty(strfind(files(j+2).name,['_',alg{i},'_D',num2str(D),'_']))
            load(strcat('./result/', files(j+2).name));
            found=1;
        end
    end
    if found
        % create list of samples
        if ~exist('samp2plot','var')
            samp2plot=zeros(size(samp,1),Nidx,Nalg);
        end
        samp2plot(:,:,i)=samp(:,samp_idx);
    end
end

% plot acf's of misifits
for i=1:Nalg
    h_sub=subplot(ha(i));
    for j=1:Nidx
        [acf,lags,bounds] = autocorr(samp2plot(:,j,i),20);
        plot(lags,acf,styles{Nidx+j},'linewidth',2); hold on;
    end
    set(gca,'FontSize',15);
%     title(alg_name(i),'fontsize',15,'interpreter','latex');
%     xlabel('Lags','FontSize',15); ylabel('Auto-correlation','FontSize',15);
%     ylabh=get(gca,'YLabel');set(ylabh,'Position',get(ylabh,'Position') - [0.02 0 0]);
end
drawnow;

lgd=strcat(repmat(strcat(repmat({'u_{'},Nidx,1),strtrim(cellstr(num2str(samp_idx(:))))),Nalg,1),[repmat({'} (20x20)'},Nidx,1);repmat({'} (40x40)'},Nidx,1)]);
lgnd=legend(lgd,'FontSize',14,'Location','EastOutside');
h_pos=h_sub.Position;
set(lgnd,'position',[sum(h_pos([1,3]))+.02 h_pos(2) 0.1 h_pos(4)]);


% save to file
fig.PaperPositionMode = 'auto';
print(fig,'./result/acf4indep','-dpng','-r0');