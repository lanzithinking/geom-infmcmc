% This is to solve a classic example of elliptic equation given mesh and
% boundary conditions and get the derivatives of objective wrt some design
% parameter theta using ADJOINT method with truncation on parameters.
% solution: u; satisfying
% d(cdu)=0;
% u(x,0)=x;
% u(x,1)=1-x;
% d_x u(0,y)=0;
% d_x u(1,y)=0;
% objective: obj(u)
% output: [u, obj, p_theta obj, p_ij obj * v];

function [u,obju,pobj,p2objv]=ePDEsol_adj_trct(theta,PDE,s,ntrct,obj,opt,PLOT,PRINT)
if nargin<3
    s=2; ntrct=0; obj=[]; opt=0; PLOT=0; PRINT=0;
elseif nargin<4
    ntrct=0; obj=[]; opt=0; PLOT=0; PRINT=0;
elseif nargin<5
    obj=[]; opt=0; PLOT=0; PRINT=0;
elseif nargin<6
    opt=0; PLOT=0; PRINT=0;
elseif nargin<7
    PLOT=0; PRINT=0;
elseif nargin<8
    PRINT=0;
end
u=[]; obju=[]; pobj=[]; p2objv=[];
% prepare to solve PDE
p=PDE.p;e=PDE.e;t=PDE.t; % mesh
b=PDE.b; b_adj=PDE.db; % problems (with boudary conditions)
% b_blk=PDE.b_blk;

% dimensions and stepsize
Np=size(p,2); meshsz=sqrt(Np); h=1/(meshsz-1);
D=length(theta); ncmp=sqrt(D);
if ntrct==0
    ntrct=D;
end

% coefficients
if all(opt==0)
    c=coeff(p,t,[],[],s,theta);
elseif all(ismember(opt,[0,1,2]))
    [c,pc]=coeff(p,t,[],[],s,theta);
    if ntrct<D
        % index of truncation
        ind_trct=sparse(ncmp,ncmp); ind_trct(1:sqrt(ntrct),1:sqrt(ntrct))=1; ind_trct=logical(ind_trct);
        pc = pc(:,ind_trct(:));
    end
end

if any(ismember([0,1,2],opt))
    % forward solution of u
    [K,F,B,ud] = assempde(b,p,e,t,-c',0,0);
    u1 = K\F; % solution without Dirichlet boundary
    u = B*u1+ud; % true solution
    % construct interpolator
    if nargout>1
        obju = obj.eval(u);
    end
    % plot the forward solution
    if PLOT
        fig1=figure(1); clf;
        pdesurf(p,t,u);
        title('Forward Solution','fontsize',18);
    end
    % print the forward solution
    if PRINT
        print(fig1,'-dpng','u_fwd');
    end
end
if any(ismember([1,2],opt))
    % solve adjoint equation for pobj
    [~,Fa]=f_adj(p,t,u,[],obj); % calculate rhs at nodes
    Fa=-Fa.*h^2; % times element
    Fa=reshape(Fa,meshsz,meshsz); Fa=Fa(:,2:meshsz-1); % remove Dirichlet boundary
    lambda = B*(K\Fa(:));
    % plot the adjoint solution
    if PLOT
        fig2=figure(2); clf;
        pdesurf(p,t,lambda);
        title('Adjoint Solution','fontsize',18);
    end
    % print the adjoint solution
    if PRINT
        print(fig2,'-dpng','u_adj');
    end
end
if any(ismember([1,2],opt))
    pobj=zeros(ntrct,1);
    if any(opt==2)
        dK = cell(ntrct,1);
    end
    for i=1:ntrct
        % obtain stiffness matrix with pc_i
        [K_i,~] = assempde(b_adj,p,e,t,-pc(:,i)',0,0);
        if any(opt==2)
            dK{i} = K_i;
        end
        % solution of p_i obj
        pobj(i) = (lambda'*K_i*u)/h^2;
    end
end
if any(opt==2)
    % metric-vector as a function
    p2objv=@(v)Fv(v);
end

function p2objv=Fv(v)
    % solve 2nd order forward equation
    [K2f,F2f]=assempde(b_adj,p,e,t,-(pc*v)',0,0);
    F2=K2f*u-F2f;
    F2=reshape(F2,meshsz,meshsz); F2=F2(:,2:meshsz-1); % remove Dirichlet boundary
    deltay = B*(K\F2(:));
    % plot the 2nd order forward solution
    if PLOT
        fig3=figure(3); clf;
        pdesurf(p,t,deltay);
        title('2nd order Forward Solution','fontsize',18);
    end
    % print the 2nd order forward solution
    if PRINT
        print(fig3,'-dpng','u2_fwd');
    end
    
    % solve 2nd order adjoint equation
    [~,F2a]=f2_adj(p,t,deltay,[],obj); % calculate rhs at nodes
    F2a=-F2a.*h^2; % times element
    F2a=reshape(F2a,meshsz,meshsz); F2a=F2a(:,2:meshsz-1); % remove Dirichlet boundary
    deltalambda = B*(K\F2a(:));
    % plot the adjoint solution
    if PLOT
        fig4=figure(4); clf;
        pdesurf(p,t,deltalambda);
        title('2nd order Adjoint Solution','fontsize',18);
    end
    % print the adjoint solution
    if PRINT
        print(fig4,'-dpng','u2_adj');
    end
    
    % calculate metric-vector
    p2objv=zeros(ntrct,1);
    for j=1:ntrct
        % solution of p_i obj
        p2objv(j) = -(deltalambda'*dK{j}*u)/h^2;
    end
end

end