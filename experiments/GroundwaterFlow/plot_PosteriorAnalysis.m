%%% This is to plot some posteriors of conductivity (in physical space). %%%

clear;
addpath('~/Documents/MATLAB/tight_subplot/');
% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed);

% algorithms
alg={'pCN','infMALA','HHMC','infMMALA','splitinfMMALA','splitinfmHMC'};
alg_name={'pCN','$$\infty$$-MALA','H-HMC','$$\infty$$-MMALA','split$$\infty$$-MMALA','split$$\infty$$-mHMC'};
Nalg=length(alg);

% dimension setting
ncmp = 10; D = ncmp^2;
dev=zeros(D,4);
dev_name={'Deviation 2 Prior','Deviation 2 Gaussian'};
crit_name={'(CDF)','(K-L divergence)'};

% estimate the deviation measure
% load data
files = dir('./summary');
nfiles = length(files) - 2;

analyzeNO=6;

for i=analyzeNO
    for j=1:nfiles
        if ~isempty(strfind(files(j+2).name,['_',alg{i},'_D',num2str(D),'_']))
            load(strcat('./summary/', files(j+2).name));
        end
    end
    % estimate sample mean and sample standard deviation
    m=mean(samp); s=std(samp);
    % criterion 1: difference in CDF
    tic;
    for d=1:D
        [F,X]=ecdf(samp(:,d));
        Phi=normcdf(X);
        dev(d,1)=max(abs(F-Phi));
%         dev(d,1)=norm(F-Phi);
        Phi=normcdf(X,m(d),s(d));
        dev(d,3)=max(abs(F-Phi));
    end
    toc
    % criterion 2: K-L divergence
    tic;
    for d=1:D
        f=ksdensity(samp(:,d),samp(:,d));
        phi=normpdf(samp(:,d));
%         [f,xi]=ksdensity(samp(:,d));
%         phi=normpdf(xi);
        dev(d,2)=mean(log(f)-log(phi));
        phi=normpdf(samp(:,d),m(d),s(d));
%         phi=normpdf(xi,m(d),s(d));
        dev(d,4)=mean(log(f)-log(phi));
    end
    toc
end

%%%% plot deviations %%%%
 % set plot
fig1=figure(1); clf;
% set(fig1,'windowstyle','docked');
set(fig1,'pos',[0 800 700 450]);
% fig=figure('visible','off');
ha1=tight_subplot(2,2,[.16 0.11],[.1,.09],[.1,.05]);

% plot
for i=1:2
    for j=1:2
        subplot(ha1((i-1)*2+j));
        devinmat=reshape(dev(:,(i-1)*2+j),ncmp,ncmp);
        imagesc(devinmat);
        title([dev_name{i},crit_name{j}],'fontsize',18,'interpreter','latex');
        xlabel('component','fontsize',15);ylabel('deviation','fontsize',15);
        ylabh=get(gca,'YLabel');set(ylabh,'Position',get(ylabh,'Position') - [.05 0 0]);
        set(gca,'Ydir','normal');
        set(gca,'FontSize',15);
        colorbar;
    end
end


% find the largest 3 deviations
[Y,I]=sort(dev,1,'descend');
% max3=I(1:3,[1,3]); % using CDF
max3=I(1:3,[2,4]); % using KL divergence

%%%% plot some pairwise posterior contours %%%%

fig2=figure(2); clf;
set(fig2,'pos',[0 800 900 400])
% ha2=tight_subplot(1,2,[.16 0.1],[.12,.08],[.07,.05]);
subplot(1,2,1);
plotmatrix(samp(:,max3(:,1)));
subplot(1,2,2);
plotmatrix(samp(:,max3(:,2)));


%%%% plot 3 marginal posteriors that deviate from prior/Gaussian to the most extent %%%%

% set plot
fig3=figure(3); clf;
% set(fig1,'windowstyle','docked');
set(fig3,'pos',[0 800 900 500]);
% fig=figure('visible','off');
ha3=tight_subplot(2,3,[.12 0.1],[.1,.1],[.07,.05]);

% plot
for i=1:2
    if i==1
        mu=0;sigma=1;
    end
    for j=1:3
        subplot(ha3((i-1)*3+j));
%         histfit(condct(:,max3(j,i)),20,'kernel');
        histogram(samp(:,max3(j,i)),'normalization','pdf'); hold on;
        if i==2
            mu=m(max3(j,i));sigma=s(max3(j,i));
            RANGE=[min(samp(:,max3(j,i))),max(samp(:,max3(j,i)))];
        else
            RANGE=[min([-3,min(samp(:,max3(j,i)))]),max([3,max(samp(:,max3(j,i)))])];
        end
        [f,xi]=ksdensity(samp(:,max3(j,i))); plot(xi,f,'--r','linewidth',2); hold on;
        fplot(@(x)normpdf(x,mu,sigma),[RANGE(1),RANGE(2)],'b-'); hold off;
        set(gca,'FontSize',15);
        xlim([RANGE(1),RANGE(2)]);
        xlabel(['u_{',num2str(max3(j,i)),'}'],'fontsize',15);
    end
end
h = suptitle('3 posteriors most deviated from prior/Gaussian');
set(h,'FontSize',18,'FontWeight','normal');

% save to file
% print(fig,'-dpng','./figure/somePosteriors');