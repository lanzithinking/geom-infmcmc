% This is to set up to jointly solve a classic example of elliptic equation
% and its derivatives wrt some design parameter theta.
% solution: u; satisfying
% d(cdu)=0;
% u(x,0)=x;
% u(x,1)=1-x;
% d_x u(0,y)=0;
% d_x u(1,y)=0;

function PDE=ePDEsetup(meshsz,D,basis_type,sigma,varargin)
narginchk(0,6);
if nargin<1
    meshsz=30; D=0; basis_type='Fourier'; sigma=1;
elseif nargin<2
    D=0; basis_type='Fourier'; sigma=1;
elseif nargin<3
    basis_type='Fourier'; sigma=1;
elseif nargin<4
    sigma=1;
end

% Random Numbers...
seed = RandStream('mt19937ar','Seed',2015);
RandStream.setGlobalStream(seed); % force the same random seed

% define solution domain
gdm = [3 4 0 1 1 0 0 0 1 1]';
g = decsg(gdm, 'S1', ('S1')');
% figure;
% pdegplot(g, 'edgeLabels', 'on');
% axis([-.1 1.1 -.1 1.1]);
% title 'Geometry With Edge Labels Displayed';

% creat triangular mesh
% hmax = .1; % element size
% [p,e,t] = initmesh(g, 'Hmax', hmax);
% try regular mesh
[p,e,t] = poimesh(g,meshsz(1),meshsz(end)); % there is some randomness in the mesh algorithm - t might be different (matlab2015a).
% figure;
% pdeplot(p,e,t);
% axis equal
% title 'Plate With Mesh'
% xlabel 'X-coordinate, meters'
% ylabel 'Y-coordinate, meters'

% Create a pde entity for a PDE of u
NPDE = 1;
b = pde(NPDE);
% Create a geometry entity
pg = pdeGeometryFromEdges(g);
% specify boundary conditions
bc1 = pdeBoundaryConditions(pg.Edges(1),'u',@(problem,region,state)region.x);
bc3 = pdeBoundaryConditions(pg.Edges(3),'u',@(problem,region,state)1-region.x);
bc24 = pdeBoundaryConditions(pg.Edges([2,4]),'g',0);
b.BoundaryConditions = [bc1,bc3,bc24]; % all boundary conditions

% Create a pde entity for the adjoint PDE of lambda
db = pde(NPDE);
% Create a geometry entity
pg = pdeGeometryFromEdges(g);
% specify boundary conditions
bc13 = pdeBoundaryConditions(pg.Edges([1,3]),'u',0);
% bc24 = pdeBoundaryConditions(pg.Edges([2,4]),'q',0,'g',0);
db.BoundaryConditions = [bc13,bc24]; % all boundary conditions

% % Create a pde entity for the stiffness matrices in bulk
% b_blk = pde(ncmp^2);

% PDE definition
PDE.p=p;PDE.e=e;PDE.t=t;
PDE.b=b;PDE.db=db;
% PDE.b_blk=b_blk;

if D~=0
    % prepare for the PDE coefficients
    PDE.parD=D; % dimension of parameters
    Nt = size(t,2);
    % Triangle point indices
    it1 = t(1,:);
    it2 = t(2,:);
    it3 = t(3,:);
    % Find centroids of triangles
    centroid = (p(:,it1)+p(:,it2)+p(:,it3))/3;
    
    PDE.basis_type=basis_type; PDE.sigma=sigma;
    % eigen-pairs of covariance kernel
    switch basis_type
        case 'Fourier'
            % 2d fourier basis
            if isempty(varargin)
                varargin{1}=1; % scale parameter
                varargin{2}=1.1; % smoothness parameter
            elseif length(varargin)==1
                varargin{2}=1.1;
            end
            ncmp = sqrt(D); alpha=varargin{1};s=varargin{2}; PDE.alpha=alpha;PDE.s=s;
            eigv = sigma.*(alpha+pi^2.*bsxfun(@(x,y)(x.^2+y.^2),(.5:ncmp-.5)',.5:ncmp-.5)).^(-s/2);
            cosix = cos(pi.*centroid(1,:)'*(.5:ncmp-.5)); cosiy = cos(pi.*centroid(2,:)'*(.5:ncmp-.5));
            eigf = bsxfun(@times,cosix,reshape(cosiy,Nt,1,ncmp));
        case 'PCA'
            if isempty(varargin)
                varargin{1}=.1; % correlation length of GP
                varargin{2}=2; % exponential in GP prior
            elseif length(varargin)==1
                varargin{2}=2;
            end
            l=varargin{1};ex=varargin{2}; PDE.l=l;PDE.ex=ex;
            C = exp(-pdist2(centroid',centroid').^ex/(2*l^ex));
            % number of components in PCA
            K=min([D,Nt]);
            [evc,ev] = eigs(C,K,'LA'); % there are some approximations behind it, I guess, check C*evc./evc! but much faster
            eigv = sigma.*sqrt(diag(ev)); eigf = evc; % ok under fixed random seed
        otherwise
            error('Wrong option!');
    end
    
    % PDE coefficient decomposition
    PDE.eigv=eigv; PDE.eigf=eigf;
end

end
